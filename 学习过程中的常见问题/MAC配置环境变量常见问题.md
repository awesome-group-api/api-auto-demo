### MAC系统中有多个环境可用来配置脚本

#### 1、查看MAC系统中配置环境有哪些

在终端输入命令：cat /etc/shells

#### 2、查看本地使用的是哪个配置环境

在终端输入命令：echo $SHELL

#### 3、以zsh为例，环境变量配置

1）如果是第一次配置环境变量，使用命令 `**touch ~/.bash_profile**` 创建一个名为**.bash_profile** 隐藏配置文件;

2） 如果不是第一次配置环境变量，使用命令 `**open ~/.bash_profile**` 打开配置文件。

按下I，进入文件编辑模式

```
export 自定义名字（MAVEN_HOME） = 路径名称`
export PATH=$PATH:$+自定义名字（MAVEN_HOME）（有多个环境变量可以在后面拼接）
```

例如：

export MAVEN_HOME=/usr/local/apache-maven-3.6.0 export PATH=$PATH:$MAVEN_HOME

按下esc退出，使用`:wq`退出。

在终端执行命令source ./.bash_profile

#### 4、zsh兼容bash，bash兼容大多数的sh
