> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b5879e220ea017a37e140af15dc)

> 目标：GitLab 容器化搭建，Pycharm 推送代码到 Gitlab 实现 Jenkins+Pytest+Allure 自动化测试，发送邮件

* * *

**目标：**GitLab 容器化搭建，Pycharm 推送代码到 Gitlab 实现 Jenkins+Pytest+Allure 自动化测试，发送邮件

**内容：**

*   1、项目 CI 流程实战
    
*   2、项目脚本设计思路回顾
    
*   3、常见问题梳理
    
*   4、持续集成流程汇总
    

* * *

1、项目 CI 流程实战
------------

```
#!/bin/bash 
rm -rf allure-reports
cd /var/jenkins_home/Delivery_System/test_case
pytest -sq --alluredir=${WORKSPACE}/allure-reports
exit 0


#1- webHooks---就是一个api---勾子
#gitlab--触发--jenkins
#当我们gitlab有动作事件发生，机会自动触发jenkins指定的工程就行构建！


#2-jenkins每一次构建的时候，怎么可以获取gitlab最新提交的代码--执行构建

#jenkins---从远程仓库--获取最新的--gitlab代码
每一次构建的时候，一开始就去指定的gitlab 仓库里获取最新的代码---http://192.168.32.140:9001/root/test.git

#3- jenkins从远程仓库获取的最新的代码--放哪里  工作目录worksopce

#4- 修改构建操作--shell
```

2、项目脚本设计思路回顾
------------

```
     不关心是什么系统
   
          浏览器不一样
          系统不一样
          selenium有无头模式（不会出现浏览器定位窗口）--测试环境在linux
          分布式grid测试：提高执行效率
   
              系统不一样
          多设备
          厂家型号















     win---run.bat
     linux/mac---run.sh
     run.py----  python  run.py
     ci环境--- jenkins--触发器做
```

```
#问题1：接口自动化测试什么时候需要使用数据库？
#回答：
 1- 如果这个接口的有对应的增加/删除/列出--不提供数据库权限
 2- 如果这接口需要操作，没有权限--使用数据库
  1- Mysql--用户/系统相关的数据
  2- mongodb--业务层数据：店铺数据 订单数据 食品数据
```

3、常见问题梳理
--------

*   **1、脚本层** --- 掌握抓包与代码调试的技能
    

```
#1、请求方法的区别---根据项目来
#2、数据类型转换 字典-json格式  typeError--josncodeerror
 - resp.json()-----resp.text---看下可能是格式数据html
 json.loads()---里面的需要转化的数据不是json
#3、请求编码处理
#4、请求头区分
 cookies: token  -seesionId  sign---掌握自己会封装特定请求头！
 
 resp= requests.post(cookies=userCookies)
 cookies:  headers封装；cookies = user_cookie
 
 headers1 = {'cookies':userCookie}
 resp= requests.post(headers=headers1)
 
#5、请求体参数的区分
---data  表单
---json json 格式
---params 参数会放到 url  /sq?name=tom&age=10
---files  文件--传递的是文件对象  open(name,'rb')
#6、响应的返回：text与 xx.json()区分
text--- 字符串类型
xx.json()---字典类型
```

2、**框架层问题**

```
#1、pytest数据驱动问题  0
- ("a,b",[(10,20),(30,40)])

#2、allure定制化问题
- epic  story title等--可以自行验证

#3、不产生tmp报告数据问题
pytest -sq --alluredir=../report/tmp  写了还是不行
原因是：你的运行方法有问题--选择run xx.py  
解决： file--setting--修改下运行模式--选择unittest

#4、打不开报告
如果在本机运行代码--   allure  serve ../report/tmp自动打开浏览器--
注意事项：不使用ie、360浏览器，使用火狐或者谷歌，设置默认浏览器

#5、文件路径找不到
在运行代码时候，出现fileNotFoundError----你的相对路径是否有问题
../res.xls找不到等问题

#6、log日志打印失败
注意配置，日志的等级
```

**3、持续集成环境问题**

```
#1、docker搭建问题
1-docker加速器修改了没有效果，你得自己去阿里云申请一个新，怎么申请，我有笔记
2-docker启动失败
原因：最好支持是centos7以上，还有对linux内核有要求

#-------------------------------------------
#镜像怎么打出来的
	1- vi Dockerfile_tomcat
	2- 在这个文件里写dockerfile语法
		                 FROM tomcat  #从官方下载纯净版的tomcat镜像
		     COPY /路径/waimai.war  /usr/local/webapps
	3- 执行docker  build -f Dockerfile_tomcat -t 镜像名:版本
#--------------------------------------------



#2、Jenkins插件下载失败问题
去修改一个下载源---xml文件里
实在在线安装不了--可以使用离线安装！
#3、Jenkins首页访问慢问题
去修改一个下载源---xml文件里

#4、Jenkins邮件发送问题
-邮件配置里有需要发送邮箱的账号与授权码  @163.com
-模板--提供一个邮件模板.txt文档--在11次课的笔记下
阿里云部署---开放邮箱端口是  465  但一定要钩上  ssl

#5、GItLab报错： 502  442
502---从容器里的配置文件启动--增大内存  >4G
442-- 缓存，你可以换一个浏览器使用看看
dokcer容器报错---使用docker logs查看日志

#6、GItLab与Jenkins联动失败
一定完成整个操作
```

4、持续集成流程汇总
----------

**Jenkins 实现构建**

```
#!/bin/bash 
rm -rf allure-reports
cd /Delivery_System/test_case
pytest -sq --alluredir=${WORKSPACE}/allure-reports
exit 0
```

**docker 面试题：**

```
 #1- 如果容器报错了，怎么查看
 回答：看日志，docker logs -f  容器id 
 
 #2- 怎么看已启动的容器的目录挂载情况：
 docker inspect -A 20  
 
 #3- 容器怎么跟宿主机时间同步！
 回答：在创建容器的时候，-v  /etc/localtome
 
 #4-是否有使用过容器管理工具
 
 #5- dockerfile  ---自己了解下
 
 #6- docker 怎么查看容器资源情况
 docker stats
```

**年纪**

**学历**

**婚姻**

**最核心：技术 + 能力 + 身体**

**实战项目简介 - 外卖系统（卖家端 web）**

**项目描述：**本项目基于 spring boot 和 vue 的前后端分离技术架构。功能完善，包含：后端 API、用户 H5 手机端、管理员 WEB 端、商户 WEB 端。 **主要功能包括：** 我的商铺：，卖家商铺信息管理 食品管理：商铺食品的常规操作 订单管理：订单信息管理

**软件框架**

> 前端框架：Vue.js 后端框架：Spring Boot 数据库层：mysql+mongodb 数据库连接池：Druid 缓存：Ehcache

1、熟悉软件测试理论、软件测试流程和测试方法，根据需求进行测试用例的编写、执行和缺陷的跟踪。

2、熟悉缺陷管理工具 Jira、禅道、TAPD，能使用 xMind 思维导图提取测试点

3、熟悉 MySQL 数据库常用操作和构建测试数据，具备 Linux 环境下常用操作指令；

4、熟悉 Docker 技术，可以完成自动化测试环境的搭建工作

5、掌握 Charles、Fiddler，测试规定模块是否进行加密，设置断点，测试服务器是否经过校验，能够进行弱网测试、远程映射，辅助定位 Bug、构建模拟测试场景

6、熟悉使用 Postman、Swagger 工具进行 Api 接口测试

7、熟练使用 Python 语言结合 Pytest+Allure 方案实现接口自动化、UI 自动化测试

8、熟悉 mock 技术，在接口自动化测试中使用 mock 技术实现部分接口的测试工作

9、熟悉配置管理工具 Svn、Git，快速提取测试资料及项目可执行代码

10、掌握 Jmeter 接口测试工具，能够独立编写脚本，进行接口压测，监控指标分析结果，编写性能测试报告。

**项目名称：**外卖系统（卖家端 web）

**项目架构：**Vue.js+Spring Boot+mysql+mongodb+Druid+Ehcache

**项目描述：**本项目基于 spring boot 和 vue 的前后端分离技术架构。功能完善，包含：后端 API、用户 H5 手机端、管理员 WEB 端、商户 WEB 端

**我的职责：**

1.  参与需求评审，根据接口文档编写测试用例，并参与测试用例评审
    
2.  使用 Python 语言（主要是 requests 库）编写自动化脚本
    
3.  新增业务模块，使用 Mock 技术实现自动化测试脚本开发调试工作
    
4.  结合 Pytest 框架和数据驱动自动化执行了一部分接口测试 Excel 用例
    
5.  Allure 导出定制化测试报告，可以详细查看通过的用例和失败的用例数量并显示测试不通过的原因
    
6.  使用 Docker 技术搭建基于 Jenkins+GitLab 自动化测试环境
    
7.  使用 JIRA 提交 bug 安排给负责相应模块的开修改，对 bug 状态进行跟踪和监控
    

**常见问题：**

*   1、你项目里负责自动化测试吗 ---- 连续问题
    

*   1、是哪一个项目做的？  最近的工作项目
    
*   2、你能讲下你项目里是怎么做的吗
    
*   **问答：**
    

*   1- 结合项目来描述
    
*   2- 项目做了哪几个模块 -- 之间的关系
    
*   3 - 什么方案做 pytest+allure
    
*   4 - 遇到了什么问题。--- 情节化