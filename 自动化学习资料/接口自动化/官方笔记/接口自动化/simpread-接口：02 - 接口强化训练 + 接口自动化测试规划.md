> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b5879e220ea017a37bd01e61561)

> requests 库请求参数类型

```
#cookies工作流程（主要里面就是jsessionID）
 1- 客户端发送一个请求到服务器
    2- 服务器返回响应数据--响应头--set- COOkies---给我们客户端
    3- 这个时候这个cookies不一定是有效！---需 要登录的网站--临时cookies
    4- 需要登录---服务器判断这个账号密码正确之 后，之前的cookies变成可以使用的！
```

**requests** **库请求参数类型**

<table><thead data-darkreader-inline-bgcolor=""><tr cid="n6" mdtype="table_row" data-darkreader-inline-border-top=""><th data-darkreader-inline-border-bottom="" data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-left="">请求参数</th><th data-darkreader-inline-border-bottom="" data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-left="">请求头中数据默认类型</th><th data-darkreader-inline-border-bottom="" data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-left="">描述</th></tr></thead><tbody><tr cid="n10" mdtype="table_row" data-darkreader-inline-border-top=""><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">data</td><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">Content-Type:application/x-www-form-urlencoded</td><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">请求体是表单格式如：a=1&amp;b=2</td></tr><tr cid="n14" mdtype="table_row" data-darkreader-inline-border-top="" data-darkreader-inline-bgcolor=""><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">json</td><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">Content-Type:application/json</td><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">请求体是 json 格式如：{"name":"tom"}</td></tr><tr cid="n18" mdtype="table_row" data-darkreader-inline-border-top=""><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">params</td><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">暂时不考虑</td><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">参数是放到 url 上如：<a href="http://192.168.32.100:8080/" data-darkreader-inline-color="">http://192.168.32.100:8080/</a> 路径? a=1&amp;b=2</td></tr><tr cid="n22" mdtype="table_row" data-darkreader-inline-border-top="" data-darkreader-inline-bgcolor=""><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">files</td><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">Content-Type:multipart/form-data</td><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">文件上传接口中使用</td></tr></tbody></table>

**请求相关信息**

```
 resp = requests.post(url,data=payload)
```

**对比详情**

<table><thead data-darkreader-inline-bgcolor=""><tr cid="n30" mdtype="table_row" data-darkreader-inline-border-top=""><th data-darkreader-inline-border-bottom="" data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-left="">请求相关信息 resp.request.xxxx</th><th data-darkreader-inline-border-bottom="" data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-left="">响应相关信息 resp.xxxx</th></tr></thead><tbody><tr cid="n33" mdtype="table_row" data-darkreader-inline-border-top=""><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left=""><strong>resp.request.url 请求的 url</strong></td><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">\</td></tr><tr cid="n36" mdtype="table_row" data-darkreader-inline-border-top="" data-darkreader-inline-bgcolor=""><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left=""><strong>resp.request.headers 请求头信息</strong></td><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left=""><strong>resp.headers 响应头信息</strong></td></tr><tr cid="n39" mdtype="table_row" data-darkreader-inline-border-top=""><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left=""><strong>resp.request.body 请求体信息</strong></td><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left=""><strong>resp.text 响应体信息</strong></td></tr></tbody></table>

**requests 库响应消息体四种格式**

<table><thead data-darkreader-inline-bgcolor=""><tr cid="n346" mdtype="table_row" data-darkreader-inline-border-top=""><th data-darkreader-inline-border-bottom="" data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-left="">四种返回格式</th><th data-darkreader-inline-border-bottom="" data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-left="">说明</th><th data-darkreader-inline-border-bottom="" data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-left="">用处</th></tr></thead><tbody><tr cid="n350" mdtype="table_row" data-darkreader-inline-border-top=""><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">r.text：文本相应内容</td><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">返回字符串类型</td><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">获取网页 html 时用</td></tr><tr cid="n354" mdtype="table_row" data-darkreader-inline-border-top="" data-darkreader-inline-bgcolor=""><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">r.content：字节相应内容</td><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">返回字节类型</td><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">下载图片或文件时用</td></tr><tr cid="n358" mdtype="table_row" data-darkreader-inline-border-top=""><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">r.json()：Json 解码响应内容</td><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">返回字典格式</td><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">明确服务器返回 json 数据才能用</td></tr><tr cid="n362" mdtype="table_row" data-darkreader-inline-border-top="" data-darkreader-inline-bgcolor=""><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">r.raw：原始响应内容</td><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">返回原始格式</td><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left=""><br></td></tr></tbody></table>

```
知识点1：字典与json区别
    1- 数据类型区别
        1- 字典是一种存储数据类型  type()
        2- json:是一个键值对形式的格式字符串
            1- json 不是python专属，
            2- 键是双引号的字符串
            3- json数据里怎么表示一个空对象:null，python的空None
            4- json数据里怎么表示一个真:true，python的空True
        3- 两者相互转化 import json
            1- 字典转json    json.dumps()
            2- json转字典    json.loads()
    2- 代码里区别
        在控制台里输出这个数据
            1- 如果是单引号--是字典类型
            2- 如果是双引号--json字符串
演示代码：
#1- 字典类型
    dict1 = {'name':'songqin','data':None,'data2':True}
    print('字典>>> ',dict1)
    print(json.dumps(dict1))#字典转json
```

```
知识点2：
场景描述：有时候，某一个接口你的Postman可以测试成功，但是你写的代码不能成功！
分析：
    1- 项目的接口没有问题，不要去怼开发
    2- 肯定是你接口本身写有问题：数据类型。一些参数  url等一些原因
环境限制：这个测试环境不能搭建任何抓包工具
解决方案：
    1- 你拿到你自己代码发出去请求与postman对比
    2- 自己打印你的请求详情

知识点3：
cookies在项目里使用场景：
    1- 后续接口使用原生态的cookie
    2- 后续接口会使用到登录的cookies(只有sessionid),但是这个cookies他是二次封装的
        cookies:session+token+uuid
        解决方案：
            1- 先通过登录的方式获取第三方接口的token
            2- 再使用本测试项目的登录去获取cookies
            3- 先从本项目的cookes提取session
            4- 二次封装 cookie=sessionid+token
```

### 1.1、token 实战

```
HOST = 'http://121.41.14.39:8082'#便于维护--测试环境变更--测试环境-预生产环境-生产环境
'''
目标：完成外卖系统的店铺端的登录接口操作
注意事项：密码需要md5加密！
'''
import requests
import pprint
#需求：输入一个字符串的密码，输出的一个md5加密结果
import hashlib
def get_md5(password):
    #1-实例化加密对象
    md5 = hashlib.md5()
    #2- 进行加密操作
    md5.update(password.encode('utf-8'))
    #3- 返回加密后的值
    return md5.hexdigest()

print(get_md5('xintian'))


def login(inData,getToken=False):
    '''
    :param inData:账号+密码---字典
    :param getToken  为True 获取token;   为False  返回接口响应数据
    :return:
    '''
    #1-url
    url = f'{HOST}/account/sLogin'
    #2- 参数
    #字典的修改值操作   字典[键] = 新的值
    inData['password'] = get_md5(inData['password'])
    payload = inData
    #3-请求方法
    resp = requests.post(url,params=payload)
    if getToken == False:
        return resp.json()#响应数据
    else:
        return  resp.json()['data']['token']#token

if __name__ == '__main__':
    res = login({'username':'xxxxxx','password':'xxxxxx'})
    # pprint.pprint(res)
    print(res)
```

### 1.2、cookie 实战

#### 1、cookie 关联操作

前端访问路径：[http://124.223.33.41:7081/mgr/login/login.html](http://124.223.33.41:7081/mgr/login/login.html)[](http://120.55.190.222:7080/mgr/login/login.html)

**1、登录接口**

*   url:
    

*   [http://124.223.33.41:7081/api/mgr/loginReq](http://124.223.33.41:7081/api/mgr/loginReq "http://124.223.33.41:7081/api/mgr/loginReq")   http 协议
    

*   请求体：
    
    <table><thead data-darkreader-inline-bgcolor=""><tr cid="n60" mdtype="table_row" data-darkreader-inline-border-top=""><th data-darkreader-inline-border-bottom="" data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-left="">username</th><th data-darkreader-inline-border-bottom="" data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-left="">auto</th><th data-darkreader-inline-border-bottom="" data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-left="">必填</th></tr></thead><tbody><tr cid="n64" mdtype="table_row" data-darkreader-inline-border-top=""><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">password</td><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">sdfsdfsdf</td><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">必填</td></tr></tbody></table>
*   响应头
    

*   比如：sessionid=89emkau5vhyg8vcwfwvq2pvr7ul2t5sc
    
*   Content-Type 必填 该字段值为 application/json，表示返回 JSON 格式的文本信息。
    
*   Set-Cookie 必填 该字段保存本次登录的 sessionid，
    

*   响应体
    

*   如果请求成功，返回 json 格式的消息体，如下所示，retcode 值为 0 表示登录认证成功
    

```
{
 "retcode": 0
}
```

*   如果输入的用户名或者密码错误，则返回结果为
    
    ```
    {
        "retcode": 1,
        "reason": "用户或者密码错误"
    }
    ```
    

**2、课程接口**

*   请求语法
    

```
GET /api/mgr/sq_mgr/?action=list_course&pagenum=1&pagesize=20 HTTP/1.1
```

*   url 请求参数
    
    <table><thead data-darkreader-inline-bgcolor=""><tr cid="n98" mdtype="table_row" data-darkreader-inline-border-top=""><th data-darkreader-inline-border-bottom="" data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-left="">参数名</th><th data-darkreader-inline-border-bottom="" data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-left="">描述</th></tr></thead><tbody><tr cid="n101" mdtype="table_row" data-darkreader-inline-border-top=""><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">action</td><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">填写 list_course，表明是要列出所有课程信息</td></tr><tr cid="n104" mdtype="table_row" data-darkreader-inline-border-top="" data-darkreader-inline-bgcolor=""><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">pagenum</td><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">表示当前要显示的是第几页，目前固定填写 1</td></tr><tr cid="n107" mdtype="table_row" data-darkreader-inline-border-top=""><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">pagesize</td><td data-darkreader-inline-border-top="" data-darkreader-inline-border-right="" data-darkreader-inline-border-bottom="" data-darkreader-inline-border-left="">表示一页最多显示多少条课程信息，目前固定填写 20</td></tr></tbody></table>
*   **请求体内容**
    

该请求无需指定请求内容。

*   **响应内容**
    

如果请求成功，返回 json 格式的消息体，如下所示

```
{
  "retlist": [
  {
         "desc": "初中语文",
          "id": 418,
         "display_idx": 1,
         "name": "初中语文"
    }
  ],
  "total": 3,
  "retcode": 0
}
```

retcode 值为 0 表示查询成功。

total 值表示总共有多少门课程信息

retlist 的内容是一个数组，其中每个元素对应一门课程信息。

*   **示例**
    
    ```
    testData2 = { "action":"list_course", "pagenum":1,"pagesize":20 }
    ```
    

**cookie 实战描述**

> ```
> #登录接口
> fiddler_proxies = {
>  'http':'http://127.0.0.1:8888',
>  'https':'http://127.0.0.1:8888'
> }
> esp = requests.post(url,params=payload)
> HOST = 'http://120.55.190.222:7080'
> import requests
> def login():
>  #1- url
>  url = f'{HOST}/api/mgr/loginReq'
>  #2- 请求体
>  payload = {'username':'auto','password':'sdfsdfsdf'}
>  #3- 请求
>  resp = requests.post(url,data=payload)
>  #获取cookie
>  #方案一：响应里就有这个cookie
>  # print(resp.cookies)
>  return resp.cookies#返回原生态的cookie
> 
> #需要关联的接口
> def add_lesson(userCookie):
>  # 1- url
>  url = f'{HOST}/api/mgr/sq_mgr/'
>  # 3- 请求
>  resp = requests.post(url, cookies =userCookie)
>  print(resp.request.headers)#查看请求头的cookies
> if __name__ == '__main__':
>  userCookie= login()
>  add_lesson(userCookie)    
> ```

#### 2、自己封装 cookie 关联后续接口实战

**使用场景描述：**举例说明如下

*   1、一个电商整个项目安装包给学员
    
*   2、电商项目使用了 cookie 机制 --sessionid
    
*   3、为了避免非 VIP 使用该项目，在使用这个项目的使用，会弹出一个 VIP 认证的页面，需要输入 VIP 账号
    
    ![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)
    
*   4、这个时候，代码层做接口自动化测试时：需要自己获取 sessionID+token
    
    ![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)
    

**代码实战**

```
HOST = 'http://120.55.190.222:7080'
import requests
def login():
    #1- url
    url = f'{HOST}/api/mgr/loginReq'
    #2- 请求体
    payload = {'username':'auto','password':'sdfsdfsdf'}
    #3- 请求
    resp = requests.post(url,data=payload)
    #获取cookie
   #方案二：需要额外增加一些参数到cookie---定制化cookie--提取sessionID
    print(resp.cookies['sessionid'])
    return resp.cookies['sessionid']#返回原生态的cookie里的sessionID

#需要关联的接口
def add_lesson(userCookie):
    # 1- url
    url = f'{HOST}/api/mgr/sq_mgr/'
    # 3- 请求
    resp = requests.post(url, cookies =userCookie)
    print(resp.request.headers)#查看请求头的cookies
if __name__ == '__main__':
     #自己封装cookie
     userCookie = {'sessionid':userSessionId,'token':'123456'}
     add_lesson(userCookie)  
```

### 1.3、https 协议代码

```
#https协议
HOST = 'https://120.55.190.222'
import requests
#处理https警告
requests.packages.urllib3.disable_warnings()
def login():
    #1- url
    url = f'{HOST}/api/mgr/loginReq'
    #2- 请求体
    payload = {'username':'auto','password':'sdfsdfsdf'}
    #3- 请求
    resp = requests.post(url,data=payload,verify = False)#不使用SSL
    return resp.text
if __name__ == '__main__':
    res = login()
    print(res)
```

* * *

2、接口自动化测试规划
-----------

### 2.1、接口自动化测试架构规划

**1、编程语言的选型**

**2、编程工具的选型**

**3、自动化测试框架的选型**

概念：一个架子  （数据驱动），有现成的一些代码 --- 提高效率

*   unittest---python 解释器自带
    
*   **unittest 升级版 - pytest--**
    
*   unittest 升级版 - nose
    
*   httprunner 框架
    
*   rf 框架 --- 关键字
    

**4、报告可视化方案的选型**

*   htmltestrunner--- 简单的 html 报告
    
*   beautifulreport
    
*   **allure**
    

**5、持续方案的选型**

*   ci 持续集成 ---jenkins
    

**6、仓库服务器的选型**

*   github
    
*   **gitlab**
    
*   gitee  码云
    

**7、测试管理工具的选型**

*   禅道
    
*   **jira**
    

* * *

### 2.2、项目代码工程创建

**包：代码需要 import  导入**

**文件夹：可以使用路径获取 的**

*   libs：基本代码库包、(common)
    
*   configs：配置包
    
*   data：数据 / 用例**文件夹 -**    （yaml 格式文件）
    
*   logs：日志文件夹
    
*   test_case：测试用例代码包
    
*   report：报告文件夹
    
*   docs：项目相关文档文件夹
    
*   utils：常规方法包