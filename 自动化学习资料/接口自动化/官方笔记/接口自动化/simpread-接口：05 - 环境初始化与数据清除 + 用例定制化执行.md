> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b5879e220ea017a37c34fd41573)

> fixture 源码详解

1、环境初始化与数据清除
------------

**fixture 源码详解**

fixture（scope='function'，params=None，autouse=False，ids=None，name=None）：

*   **scope：**有四个级别参数 "function"（默认），"class"，"module"，"session"
    
*   params：一个可选的参数列表，它将导致多个参数调用 fixture 功能和所有测试使用它。
    
*   autouse：如果 True，则为所有测试激活 fixture func 可以看到它。如果为 False 则显示需要参考来激活 fixture
    
*   ids：每个字符串 id 的列表，每个字符串对应于 params 这样他们就是测试 ID 的一部分。如果没有提供 ID 它们将从 params 自动生成
    
*   name：fixture 的名称。这默认为装饰函数的名称。如果 fixture 在定义它的统一模块中使用，夹具的功能名称将被请求夹具的功能 arg 遮蔽，解决这个问题的一种方法时将装饰函数命
    

**fixture 的作用范围**

fixture 里面有个 scope 参数可以控制 fixture 的作用范围：session>module>class>function

*   -function：每一个函数或方法都会调用
    
*   -class：每一个类调用一次，一个类中可以有多个方法
    
*   -module：每一个. py 文件调用一次，该文件内又有多个 function 和 class
    
*   -session：是多个文件调用一次，可以跨. py 文件调用，每个. py 文件就是 module
    

**调用 fixture 的三种方法**

**1、函数或类里面方法直接传 fixture 的函数参数名称**

```
@pytest.fixture()
def test1():
    print('\n开始执行function')

def test_a(test1):
    print('---用例a执行---')
```

**2、使用装饰器 @pytest.mark.usefixtures() 修饰需要运行的用例**

```
import pytest

@pytest.fixture()
def test1():
    print('\n开始执行function')

@pytest.mark.usefixtures('test1')
def test_a():
    print('---用例a执行---')
```

**3、叠加 usefixtures**

如果一个方法或者一个 class 用例想要同时调用多个 fixture，可以使用 @pytest.mark.usefixture() 进行叠加。注意叠加顺序，先执行的放底层，后执行的放上层。

```
@pytest.fixture()
def test1():
    print('\n开始执行function1')

@pytest.fixture()
def test2():
    print('\n开始执行function2')

@pytest.mark.usefixtures('test1')
@pytest.mark.usefixtures('test2')
def test_a():
    print('---用例a执行---')
```

**usefixtures 与传 fixture 区别**

如果 fixture 有返回值，那么 usefixture 就无法获取到返回值，这个是装饰器 usefixture 与用例直接传 fixture 参数的区别。

当 fixture 需要用到 return 出来的参数时，只能讲参数名称直接当参数传入，不需要用到 return 出来的参数时，两种方式都可以。

**fixture 自动使用 autouse=True**

当用例很多的时候，每次都传这个参数，会很麻烦。fixture 里面有个参数 autouse，默认是 False 没开启的，可以设置为 True 开启自动使用 fixture 功能，这样用例就不用每次都去传参了

autouse 设置为 True，自动调用 fixture 功能

* * *

**目标：**

**内容：**

*   **1、什么是环境初始化与数据清除**
    
*   **2、如何实现环境初始化与数据清除**
    

*   **登录模块**
    
*   **店铺模块**