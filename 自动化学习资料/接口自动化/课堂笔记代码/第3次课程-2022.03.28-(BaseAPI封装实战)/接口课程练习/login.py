#-*- coding: utf-8 -*-
#@File    : login.py
#@Time    : 2022/3/25 21:48
#@Author  : xintian
#@Email   : 1730588479@qq.com
#@Software: PyCharm
#Date:2022/3/25
print()
"""
需求：登录接口--Md5版本
方案:python+requests #pip install requests -i https://douban.com/simple
流程：
    1- 请求方法
    2- url
    3- 请求体
    4- 请求头
    5- 响应
争论：请求头什么时候需要写，什么时候可以不写？
写的场景：这个请求头里有其他接口(登录接口获取)给的数据  token,cookies
不写的场景：这个接口没有什么特殊的头数据，或者只要求写content type,可以不写

加密的在线工具(32位小写)：https://tool.chinaz.com/tools/md5.aspx
在线解密工具：https://www.cmd5.com/
MD5解密：
    原理：没有真正的解密函数！解密操作是去库里面匹配的
    实际情况：密码明文复杂的不能解密
    
md5使用场景：
    1- 简单md5加密   一般不在密码加密使用，再辅助加密信息 sign使用
    2- md5加盐 salt  不好破译  123456hello
    3- md5加双层盐  客户端加盐，服务也会加一次盐
        md5加双层盐举例：登录流程：
            1-前端对用户输入的密码进行md5加密（固定的salt）
            2-将加密后的密码传递到后端
            3-后端使用用户id取出用户信息
            4-后端对加密后的密码在进行md5加密（取出盐），然后与数据库中存储的密码进行对比，
            5-ok登录成功，否则登录失败
json获取数据  
    1- 先转化成字典   json.loads()----使用键去获取值
    2- 直接使用第三库   jsonpath   
            
"""
import requests
HOST = 'http://121.41.14.39:8082'#环境变迁
def login(data):#函数定义的参数---形参
    #1-url
    url = f'{HOST}/account/sLogin'
    #2-请求体数据
    payload = data
    #3-请求头不一定需要
    #4- 发请求
    resp = requests.post(url,data=payload)
    #5-打印响应体--字符串数据  html xml
    print(resp.text)#\t
    #6-打印响应体--字典类型返回---前提这个响应数据一定得是json格式
    # print(resp.json())

    #-----------扩展-----------------
    #使用场景：一个接口，使用postman可以同，你使用python不通！
    #解决：获取python请求发出去的东西！
    #具体操作：1、使用fiddler抓包python请求  2、print打印请求内容
    #7-请求url
    print('请求url--->',resp.request.url)

    #8-请求body
    print('请求体--->',resp.request.body)

    #8-请求头
    print('请求头--->',resp.request.headers)






if __name__ == '__main__':#一般适合本模块的调试代码写里面
    test_data = {
        'username':'th0198','password':'c607d58e3618832f937d80d500a6046c'}
    login(test_data)
    #TODO:2022.03.27---md5加密操作---xintian
    print({"a":"xintian"})

