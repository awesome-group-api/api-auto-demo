#-*- coding: utf-8 -*-
#@File    : cookies实战-原生态.py
#@Time    : 2022/3/27 9:44
#@Author  : xintian
#@Email   : 1730588479@qq.com
#@Software: PyCharm
#Date:2022/3/27
#目标：学习cookies封装
import requests
HOST = 'http://124.223.33.41:7081'
def login(data):
    #1- url
    url = f'{HOST}/api/mgr/loginReq'
    #2-请求body
    payload = data
    resp = requests.post(url,data=payload)
    print(resp.text)
    #获取响应头
    #print('响应头--->',resp.headers)
    # #1- 使用键取值
    #print('响应头的cookies--->', resp.headers['Set-Cookie'])
    #2- 是直接获取cookies对象
    print('cookies对象--->',resp.cookies)
    return resp.cookies#返回值返回

#-------业务层接口------------
class Lesson:
    def __init__(self,in_cookies):
        self.cookies = in_cookies
    def list_lesson(self,data):#列出课程
        url = f'{HOST}/api/mgr/sq_mgr/'
        payload = data
        resp = requests.get(url,params=payload,cookies=self.cookies)
        resp.encoding = 'unicode_escape'#设置响应数据编码！
        return resp.text


def get_vip_token():

    return 'token'


if __name__ == '__main__':
    #1- 登录接口
    cookies = login({'username':'auto','password':'sdfsdfsdf'})
    #---------------直接update----------------------推荐：简单点
    #字典['新键'] = 新值 === update
    # cookies.update({'token':get_vip_token()})

    #方案二：手动去增加
    user_cookies = {'sessionid':cookies['sessionid'],'token':get_vip_token()}
    print(user_cookies)
    #2- 列出课程接口
    res = Lesson(user_cookies).list_lesson({'action':'list_course','pagenum':1,'pagesize':20})
    print(res)