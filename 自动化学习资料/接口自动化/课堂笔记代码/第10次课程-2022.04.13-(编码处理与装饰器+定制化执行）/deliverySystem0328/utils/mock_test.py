#-*- coding: utf-8 -*-
#@File    : mock_test.py
#@Time    : 2022/4/11 20:36
#@Author  : xintian
#@Email   : 1730588479@qq.com
#@Software: PyCharm
#Date:2022/4/11 
HOST = 'http://127.0.0.1:9999'
import requests
import time
# def test():
#     url = f'{HOST}/xt'
#     payload  = {"key1":"abc"}
#     resp = requests.post(url,data=payload)
#     print(resp.json())

#--------1、提交申请接口--------
def commit_order(data):
    url = f'{HOST}/api/order/create/'
    payload = data
    resp = requests.post(url,json=payload)
    return resp.json()

#--------2、查询接口--------
"""
场景特性：
    - 不是你想查就里面有结果的，可能需要一段时间
方案：
    -主动通知
    - 定时轮询去查
        - 使用id
        - 频率/间隔
        - 超时机制

"""

def get_order_result(order_id,interval=5,time_out=30):
    """
    :param order_id: 申请的id
    :param interval: 间隔时间-s
    :param time_out: 超时时间-s
    :return:
    """
    start_time = time.time()#开始时间
    end_time = start_time + time_out#结束查询时间
    url = f'{HOST}/api/order/get_result_xt/'
    payload = {"order_id": order_id}
    cnt = 1#查询的次数
    while time.time()<end_time:#时间没有到！
        resp = requests.get(url,params=payload)
        if resp.text:#如果查询到了---退出
            print(f'第{cnt}查询,已返回结果，查询结束！-{resp.text}')
            return
        else:
            print(f'第{cnt}查询,没有返回结果，请稍后查询!')
        time.sleep(interval)
        cnt += 1#查询次数+1
    #提示---只有失败的时候
    print('查询超时，请联系平台管理员！')


import threading#自带的线程模块
if __name__ == '__main__':
    #主线程计时：
    start_time = time.time()
    order_data = {
        "user_id": "sq123456",
        "goods_id": "20200815",
        "num": 1,
        "amount": 200.6
    }
    #提交申请接口
    order_id = commit_order(order_data)["order_id"]
    print(order_id)
    #-------------------------创建子线程--查询接口-------
    #1-创建子线程
    #target=你需要做成子线程的函数名，
    #args:这个函数 参数--元组类型
    t1 = threading.Thread(target=get_order_result,args=(order_id,))
    #2- 设置线程模式：如果主线程结束，或者异常退出，子线程直接退出
    t1.setDaemon(True)#守护模式
    t1.start()#执行线程！
    # get_order_result(order_id)
    #--------------------------------------------------

    #----模拟一个主线程用例执行！----
    for one in range(20):
        print(f'{one}---我正在执行主线程的自动化测试用例---')
        time.sleep(1)#模拟---一条用例执行时间


    #主线程运行结果时间
    end_time = time.time()
    print('自动化执行耗时---> ',end_time-start_time)
""""
当前版本：
    - 功能实现了
    问题：执行效率问题--资源浪费---如果这个接口30s内都没有结果，自动化执行会等待这个30s
沟通：
    新需求：提升自动化执行效率
测试人员：分析新需求
    现象：效率低
    本质：while+time.sleep(5)
解决本质问题：
    发散：
        - 是否可以减少等待时间
        - 一旦sleep(5) cpu不干活
    根本：
        - cpu资源使用
        - 了解cpu
            cpu IO阻塞---*
                - sleep()
                - request---同步
            cpu 密集型
技术方案：
    技术瓶颈：cpu IO阻塞
    实施方案：
        - 多进程---使用cpu多核优势！
        - 多线程--充分使用一个核
        - 协程 - 多线程优秀点
        - 比较搞效率：进程+协程
    选定方案：
        多线程技术
        - 主线程：自动化测试运行
        - 子线程：查询结果的接口
扩展：
    pytest框架有自己的分布式运行
    pip intall  pytest-xdist
"""