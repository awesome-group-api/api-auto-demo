#-*- coding: utf-8 -*-
#@File    : shop.py
#@Time    : 2022/4/6 20:15
#@Author  : xintian
#@Email   : 1730588479@qq.com
#@Software: PyCharm
#Date:2022/4/6
from common.baseAPI import BaseAPI
from libs.login import Login
from pprint import pprint
#定义店铺类----命名：不使用下划线的类名  要使用驼峰法
class Shop(BaseAPI):
    #重写编辑接口
    """
    1-需要动态获取店铺的有效id
    2-需要获取动态图片信息
    """
    def update(self,data,shop_id,image_info):
        if data['id'] != '0000':#如果正向用例里需要更新id
            data['id'] = shop_id#更新id
        #2-更新图片信息
        data['image_path'] = image_info
        data['image'] = f'/file/getImgStream?fileName={image_info}'
        #3-调用父类的update发送
        return super(Shop, self).update(data)





if __name__ == '__main__':
    #1-登录操作
    login_data = {'username':'th0198','password':'xintian'}
    token = Login().login(login_data,get_token=True)
    #2-创建店铺实例
    shop = Shop(token)
    #3-列出店铺
    test_data = {'page':1,'limit':20}
    shop_res = shop.query(test_data)
    #获取店铺id
    shop_id = shop_res['data']['records'][0]['id']
    print('店铺id--->',shop_id)
    #4-文件上传接口
    image_info = shop.file_upload('../data/456.png')
    pprint(image_info)
    image_infos = image_info['data']['realFileName']

    #5- 店铺更新
    update_data = {
            "name": "星巴克新建店",
            "address": "上海市静安区秣陵路303号",
            "id": "id",
            "Phone": "13176876632",
            "rating": "5.0",
            "recent_order_num":110,
            "category": "快餐便当/简餐",
            "description": "满30减5，满60减8",
            "image_path": "b8be9abc-a85f-4b5b-ab13-52f48538f96c.png",
            "image": "http://121.41.14.39:8082/file/getImgStream?fileName=b8be9abc-a85f-4b5b-ab13-52f48538f96c.png"
        }
    res = shop.update(update_data,shop_id,image_infos)
    print(res)
    """
    关于继承原理：
    1- 如果子类有自己的__init__ ,子类会调用的自己的__init__
    2- 如果子类没有自己的__init__ ,子类会调用的父类的__init__

    """
