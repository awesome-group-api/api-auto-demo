#-*- coding: utf-8 -*-
#@File    : login.py
#@Time    : 2022/3/28 21:33
#@Author  : xintian
#@Email   : 1730588479@qq.com
#@Software: PyCharm
#Date:2022/3/28
from common.baseAPI import BaseAPI
from utils.handle_data import get_md5_data
"""
登录接口功能：
    1- 本身需要做自动化测试---ok
    2- 需要给后续接口提供一个返回的token
"""
class Login(BaseAPI):
    def login(self,data,get_token=False):
        """
        :param data: 请求body
        :param get_token: 是否获取token
        :return: 返回对应的值
        """
        data['password'] = get_md5_data(data['password'])
        resp = self.request_send(data)#调用发送方法
        if get_token:#为真
            return resp['data']['token']
        return resp#返回响应数据

    def login_2(self):
        pass

"""
验证： resp.request,headers
data: data=请求数据(字典类型-{'a':1,'b'=2})
    1- 请求数据是表单 a=1&b=2
    2- 表单有json  a=1&b={"name":"xintian"}
json  json=请求数据(字典类型-{'a':1,'b'=2})
    - i请求体是json



"""


if __name__ == '__main__':
    test_data = {'username':'th0198','password':'xintian'}
    res = Login().login(test_data,get_token=True)
    print(res)