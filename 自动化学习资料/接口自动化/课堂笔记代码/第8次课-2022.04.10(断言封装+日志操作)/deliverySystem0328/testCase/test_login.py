#-*- coding: utf-8 -*-
#@File    : test_login.py
#@Time    : 2022/4/1 20:59
#@Author  : xintian
#@Email   : 1730588479@qq.com
#@Software: PyCharm
#Date:2022/4/1 
import pytest
from libs.login import Login
from utils.handle_excel import get_excel_data
from utils.handle_path import report_path
import allure
import os
from common.apiAssert import ApiAssert
#1-登录类
@allure.epic('外卖系统')#工程级别--项目级别
@allure.feature('登录模块')#业务级别
class TestLogin:
    #2-登录的方法
    @pytest.mark.parametrize('title,req_body,exp_resp',get_excel_data('登录模块','Login'))
    @allure.story('登录接口')  # 接口级别
    @allure.title('{title}')  # 用例标题
    def test_login(self,title,req_body,exp_resp):
        res = Login().login(req_body)#3-调用登录的接口
        #4-断言结果是否符合预期
        ApiAssert.api_assert(res,'==',exp_resp,assert_info='msg0',msg='登录接口断言')
        #assert res['msg'] == exp_resp['msg']


if __name__ == '__main__':#ctrl+j
    #--clean-alluredir #清除之前的报告数据
    pytest.main([__file__,'-s','--alluredir',report_path,'--clean-alluredir'])
    #os.system(f'allure serve {report_path}')
    """
    - allure 跨平台性
    - 运行原理：
        1- 报告需要数据源  pytest执行后生成的xxx.json文件
        2- 使用allure serve 运行这些json文件
    """

