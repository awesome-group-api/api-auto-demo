#-*- coding: utf-8 -*-
#@File    : baseAPI.py
#@Time    : 2022/3/28 21:31
#@Author  : xintian
#@Email   : 1730588479@qq.com
#@Software: PyCharm
#Date:2022/3/28
#这个基类后续可能因为业务模块的增加可以维护
print()
"""
封装思路：
    1- 为所有的业务模块提供的基本接口操作：增删改查+发送接口
    2- 日志  截图都可以在基类里封装
    3- 断言方法
@log装饰器
def login():
    try:
        xxxxx
    except：
        log.error()

"""
#---------------封装的思路启发--------------
"""
发送公共请求方法：
    def request_send(self,method,url):
        - 实际调用发送方法必须传递2个参数，
        - 每一个接口的数据还不一样
        - 反馈：很麻烦
    优化：
        思路：代码与配置分离
        实施：method url 可以放到一个配置文件  apiPathConfig.yml
        难点：代码怎么可以识别到对应的模块、对应的接口、对应的参数
        场景分类：
            - 常规风格的接口格式
                - 方法举例： 增加数据接口： post  ；修改也是 post
                - url举例：增加接口 /sq    修改 /sq_xintian/{id}
                
            - restful接口风格
                - 方法规则：get  post  delete  put
                - url规范： 一样的
"""
import requests
from utils.handle_yaml import get_yaml_data
import inspect
from configs.config import HOST
class BaseAPI:
    def __init__(self):
        #获取对应模块的接口信息
        self.data = get_yaml_data('../configs/apiPathConfig.yml')[self.__class__.__name__]#根据类名去获取
        print('类名是--->', self.__class__.__name__)
        print('类接口数据--->', self.data)

    #---------发送的公共方法-每一个接口都会调用他----------
    def request_send(self,data):
        try:
            #api_data == {'path': '/account/sLogin', 'method': 'POST'}
            api_data = self.data[inspect.stack()[1][3]]
            resp = requests.request(
                method=api_data['method'],#方法
                url=f'{HOST}{api_data["path"]}',#url
                data=data)
            return resp.json()
        except:
            pass


#-------------------演示案例-----------------------
# import inspect
# def send():
#     #inspect.stack()[1][3]  获取调用者的函数名
#     print(f'---{inspect.stack()[1][3]}调用send方法---')
#
# def login():
#     print('---函数login开始执行了-----')
#     send()
#
# login()