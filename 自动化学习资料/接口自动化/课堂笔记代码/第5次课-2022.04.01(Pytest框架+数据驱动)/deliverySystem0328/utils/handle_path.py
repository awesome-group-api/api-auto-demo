#-*- coding: utf-8 -*-
#@File    : handle_path.py
#@Time    : 2022/4/1 21:28
#@Author  : xintian
#@Email   : 1730588479@qq.com
#@Software: PyCharm
#Date:2022/4/1 
import os
print(__file__)#当前文件路径
print(os.path.dirname(__file__))#上一层目录
#1-工程路径
project_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
#2-配置路径
config_path = os.path.join(project_path,'configs')
#3-配置路径
data_path = os.path.join(project_path,'data')
# print(config_path)
#4-report
report_path = os.path.join(project_path,r'outFiles\report\tmp')
# print(config_path)