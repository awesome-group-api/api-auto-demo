> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b5879e220ea017a2d351fce7b8b)

> 面向对象的 API 库，将 API 操作过程中的公共方法封装到 BaseAPI 中，子类基于业务的 API 集成即可。减少重复了的编码量，提高 API 库编写效率。

什么是 AO（APIObject）设计模式
---------------------

面向对象的 API 库，将 API 操作过程中的公共方法封装到 BaseAPI 中，子类基于业务的 API 集成即可。减少重复了的编码量，提高 API 库编写效率。

![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)![](http://vip.ytesting.com//upload/ueditor/upload/image/20220113/1642040012407073843.png)

AO 库提升优化
--------

当子类有特殊需求时，只需在子类中更新对应方法即可

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220113/1642040029210039718.png)![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)

基类优化：

```
import requests
from configs.env import HOST
from utils.handle_path import config_path
from utils.handle_yaml import get_yaml_data
from utils.handle_log import log
import os
import traceback

class BaseAPI:
    def __init__(self,cookies=None):
        #如果传cookies了 就不是登录接口了
        if cookies:
            self.cookies = cookies #把传入的cookies赋值给一个属性
            self.spaceid = self.cookies['X-Space-Id']#每个业务里都需要spaceid
        else:
            self.cookies = None#是登录接口
        self.data = get_yaml_data(os.path.join(config_path,'apiConfig.yaml'))[self.__class__.__name__]

    #1封装一个通用请求方法
    def request_send(self,method,_id="",json={},params={},login=False):
        '''
        resful请求响应体数据都是json
        '''
        try:
            resp = requests.request(method,url=f'{HOST}{self.data["path"]}/{_id}',json=json,params=params,cookies=self.cookies)
            if login:
                return resp#响应对象
            return resp.json()#响应体
        except Exception as error:
            #打日志
            log.error(traceback.format_exc())
            raise error

    #TODO 后面其他业务增删改查方法

    #增加接口
    def add(self,**kwargs):#name = xxx,parent=xxx
        payload = self.data['add']
        #更新spaceid
        kwargs['space'] = self.spaceid
        payload.update(kwargs)#更新字典
        #发送请求
        resp = self.request_send('POST',json=payload)#resp是响应体数据  字典类型
        return resp['value'][0]#返回通用有用的数据

    #删除接口
    def delete(self,_id):
        return self.request_send('DELETE',_id=_id)
    #更新接口
    def update(self,_id,**kwargs):
        payload = self.data['update']
        # 更新spaceid
        kwargs['space'] = self.spaceid
        payload['$set'].update(kwargs)  # 更新字典
        # 发送请求
        return self.request_send('PUT',_id=_id,json=payload)  # resp是响应体数据  字典类型


    #查询接口
    def query(self,**kwargs):
        resp = self.request_send('GET',params=kwargs)
        return resp['value']#取出对应的value列表

    def delete_all(self):
        #1列出所有数据
        items = self.query()
        #2挨个删除
        for item in items:
            self.delete(item['_id'])

```

子类特殊定制化，如：OrganizAPI(BaseAPI)

```
class OrganizAPI(BaseAPI):
    def __init__(self,cookie):
        super(OrganizAPI, self).__init__(cookie)#调用父类的init操作拿到self.data的值
        #添加部门需要关联上级id 需要查询到正确的上级id 去封装查询方法
        #获取公司的总id
        orgList = self.query()
        self.top_parentID = orgList[0]["_id"]

    #添加部门
    def add(self,**kwargs):
        #如果给个空的环境 默认取parentid的时候 是总部门id
        if 'parent' not  in kwargs:# kwargs里没传parent = xxxxx
            kwargs['parent'] = self.top_parentID
        return super(OrganizAPI, self).add(**kwargs)#调用基类——父类的add方法
    #修改部门
    def update(self,_id,**kwargs):
        #如果给个空的环境 默认取parentid的时候 是总部门id
        if 'parent' not  in kwargs:# kwargs里没传parent = xxxxx
            kwargs['parent'] = self.top_parentID
        return super(OrganizAPI, self).update(_id=_id,**kwargs)#调用基类——父类的add方法

    def delete_all(self):
        #1列出所有数据  要过滤总公司
        items = self.query()[1:]
        #2挨个删除
        for item in items:
            self.delete(item['_id'])

```