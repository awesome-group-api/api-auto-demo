> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b5879e220ea017a3d2d270223ff)

> 工厂：实体产品的生产环节，如：富士康手机生产流水线

流水线概念
-----

工厂：实体产品的生产环节，如：富士康手机生产流水线

IT 企业：软件生产环节，如：需求调研，需求设计，概要设计，详细设计，编码，单元测试，集成测试，系统测试，用户验收测试，交付。。。

市场需求调研—可行性研究—产品项目立项—需求调研开发—设计开发测试—发布运行维护

Jenkins pipeline
----------------

Jenkins 2.0 开始加入的功能

通过代码定义一切软件生产过程， 构建 - 单元测试 - 部署 - 自动化测试 - 性能测试 - 安全 - 交付

两种语法结构：

声明式：阅读性相对好，缺点是比较繁琐

脚本式：语法简洁，但是对代码能力要求比较高 ---groovy (Java 的脚本式语言) 有点类似于 python 是一

种解释型语言

两种语法 2 选 1

脚本式语法
-----

**node 标签的含义**

node 定义脚本任务执行在那台机器 -- 必须要有

```
node('机器的标签') #如果不写执行任务的机器，默认在master上面运行
{
 待执行的任务
}

```

```
node('sqtest'){
echo '执行pipeline测试'
}

```

stage 表示某个环节，对应的是视图中的小方块，可以自由定义环节名称和其中要执行的代码（可以没有，但是建议有）

```
stage('环节名称'){
待执行的任务
}

```

```
node('sqtest'){
    stage('阶段1'){
    echo '执行pipeline测试'
    }
    stage('阶段2'){
    echo '执行pipeline测试'
    }
    stage('阶段3'){
    echo '执行pipeline测试'
    }
    stage('阶段4'){
    echo '执行pipeline测试'
    }
}

```

不同测试环境脚本任务的区别，win/linux/mac

linux/mac

```
sh 待执行的任务脚本

```

```
node(){
stage('阶段1'){
 bat "echo '执行阶段1任务'"
}
stage('阶段2'){
 bat "echo '执行阶段1任务'"
}
stage('阶段3'){
 bat "echo '执行阶段1任务'"
}
}

```

win

```
bat 待执行的任务脚本

```

如：

```
node('sqtest'){
stage('阶段1'){
 bat "echo '执行阶段1任务'"
}
stage('阶段2'){
 bat "echo '执行阶段1任务'"
}
stage('阶段3'){
 bat "echo '执行阶段1任务'"
}
}

```

stage 也可以嵌套 node，表示当前阶段可以用多个环境来执行任务

```
stage('阶段1'){
 node('win环境'){ 
  bat "echo '执行阶段1任务'"
 }
    node('linux环境'){
        sh "echo '执行阶段1任务'"
    }
} 
stage('阶段2'){
 node('win环境'){
  bat "echo '执行阶段1任务'"
 }
 node('linux环境')
  { sh "echo '执行阶段1任务'"
 }
}
stage('阶段3'){
 node('win环境'){
  bat "echo '执行阶段1任务'"
 }
 node('linux环境')
  { sh "echo '执行阶段1任务'"
 }
}

```

```
stage('阶段1'){
 node('sqtest'){
  bat "echo '执行阶段1任务'"
 }
    node(){
     sh "echo '执行阶段1任务'"
    }
}
stage('阶段2'){
 node('xiaoze'){
  bat "echo '执行阶段1任务'"
 }
    node(){
     sh "echo '执行阶段1任务'"
    }
}
stage('阶段3'){
 node('xiaoze'){
  bat "echo '执行阶段1任务'"
 }
    node(){
     sh "echo '执行阶段1任务'"
    }
}

```

创建流水线任务
-------

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625637593477066917.png)

Jenkinsfile 管理流水线脚本
-------------------

任务形式

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625637623803001873.png)

#api 测试 UI 测试 测试报告 邮件发送

```
node('sqtest'){
stage('api测试'){
}
stage('UI测试'){ 
}
stage('测试报告'){ 
}
stage('邮件发送'){ 
}
}

```

Jenkins pipeline 语法提示器
----------------------

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625637652606083097.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625637687851096301.png)

生成脚本后复制

![](http://vip.ytesting.com//upload/ueditor/upload/image/20210707/1625637710154083720.png)

脚本示例
----

```
node("sqtest"){
    stage('0-拉取下载项目代码'){
       echo "拉取下载项目代码"
       checkout scm //自动下载关联的代码
    }

    stage('1-执行ui自动化'){

    echo "执行ui自动化"
    try{
     bat "pytest testCase/webui -s --alluredir=outFiles/report/tmp"
    }catch(Exception e){

     echo "错误信息"+e

    }


    }
    stage('2-执行接口自动化'){

    echo "执行接口自动化"
    bat "pytest testCase/api -s --alluredir=outFiles/report/tmp"

    }
    stage('3-生成报告'){

    echo "生成报告"
    allure results: [[path: 'outFiles/report/tmp']]

    }
    stage('4-发送邮件'){

    echo "发送邮件"
    //读取 邮件模板信息
    def email_content=readFile encoding: 'utf8', file: 'email_template.html'
    //是否读取到email 变量
    echo "${email_content}"

    emailext body: '${email_content}', subject: '构建通知:${BUILD_STATUS} - ${PROJECT_NAME} - Build # ${BUILD_NUMBER} !', to: 'xxxxxxx@qq.com'
    }

}

```