> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b5879e220ea017a2d3827dc7b93)

> 项目描述：商业合同管理系统 (web) 主要功能：合同管理：包括合同与合同相关业务的增删改查收 / 付款业务：与合同关联的企业收 / 付款业务流程审批：自定义企业审批流程

**项目描述**：商业合同管理系统 (web) **主要功能**：合同管理：包括合同与合同相关业务的增删改查收 / 付款业务：与合同关联的企业收 / 付款业务流程审批：自定义企业审批流程

技术架构
----

**后端开发技术**：node，express, mongoose **前端开发技术**：vue2.0, vuex, vueRouter **数据库**：mongo—nosql 数据库**系统架构**：前后端分离

![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)![](http://vip.ytesting.com//upload/ueditor/upload/image/20220113/1642056831151059969.png)

业务流程
----

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220113/1642056839494002396.png)![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)

项目环境
----

OA 环境：[http://120.27.146.185:6007/](http://120.27.146.185:6007/)

账号自行注册
------

代码工程
----

需求：接口 + webui 自动化

*   common—公共部分  包
    

*   baseApi.py   接口的基类
    
*   basePage.py UI 的基类
    

*   libs 业务代码  包  继承对应 common 的基类
    

*   api       接口的库
    
*   webui ui 的库
    

*   configs 配置包
    
*   testCase 测试用例库  调用对应的 libs 下的类
    

*   apiCase
    
*   uiCase
    

*   data 数据文件夹
    

*   yaml
    
*   excel
    
*   测试用例文件
    

*   utils 工具包 高可用的一些代码
    

*   处理 excel 表格
    
*   处理 yaml 数据
    
*   处理日志
    
*   处理路径
    

*   outFiles
    

*   report 报告
    
*   logs 日志
    
*   screenshot 截图
    

*   docs 文档
    

接口和 UI 的侧重点
-----------

UI：偏向流程验证，测试对象从前端到后端适用场景：1. 业务操作涉及多个页面的流程，2. 一步操作涉及多个接口 3. 非前后端分离的系统 --(服务端返回的是 html 页面)

API：偏向数据验证，绕过前端只测试后端适用场景：1. 单个业务只需少量接口即能实现 2. 测试数据的准备（利用接口准备测试数据)3. 系统只有后端没有前端页面（如金融中台系统，只对外开放 API）

接口 restful 规范
-------------

1. 接口认证机制 ---cookies 存储在请求头中

2.  接口风格 ---restful    数据格式：json  请求方法：查询：GET  增加：POST 修改：PUT 删除：DELETE
    
3.  同一个 API 的 url 相同
    

APIObject 设计模式
--------------

BaseAPI 的设计‘

统一增删改查 API

接口参数模板化

接口数据与测试脚本分离

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220113/1642056885379084641.png)