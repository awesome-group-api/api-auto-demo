> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b587eddaee2017f19f7a82c3e1d)

> 本文的 grid 是以 4.0 为例的，如果你要学习 3.0 系列的 grid 请移步

*   **本文的 grid 是以 4.0 为例的，如果你要学习 3.0 系列的 grid 请移步**
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220221/1645408011904034144.png)

下载
--

*   **[https://github.com/SeleniumHQ/selenium/releases](https://github.com/SeleniumHQ/selenium/releases)**
    
*   **本文以下面的 jar 为例**
    

**![](http://vip.ytesting.com//upload/ueditor/upload/image/20220221/1645408044177059037.png)**

目的和主要功能
-------

*   为所有的测试提供统一的入口
    
*   管理和控制运行着浏览器的节点 / 环境
    
*   扩展
    
*   并行测试
    
*   跨平台 (操作系统) 测试
    
*   负载测试
    

**通常来说，有 2 个原因你需要使用 Grid。**

*   在多种浏览器，多种版本的浏览器，不同操作系统里的浏览器里执行你的测试
    
*   缩短完成测试的时间
    

组件
--

**![](http://vip.ytesting.com//upload/ueditor/upload/image/20220221/1645408063662031273.png)**

### 路由

**路由负责将请求转发到正确的组件.**

它是 Grid 的入口点, 所有外部请求都将由其接收. 路由的行为因请求而异. 当请求一个新的会话时, 路由将把它添加到新的会话队列中. 分发器定期检查是否有空闲槽. 若有, 则从新会话队列中删除第一个匹配请求. 如果请求属于存量会话, 这个路由将会话 id 发送到会话表, 会话表将返回正在运行会话的节点. 在此之后, 路由将 将请求转发到节点.

为了更好地发挥效力, 路由通过将请求发送到组件的方式, 来平衡 Grid 的负载, 从而使处理过程中不会有任何的过载组件.

### 分发器

分发器知道所有节点及其功能. 它的主要作用是**接收新的会话请求** 并找到可以在其中创建会话的适当节点. 创建会话后, 分发器在会话集合中存储会话 ID 与正在执行会话的节点之间的关系.

### 节点

一个节点可以在网格中出现多次. **每个节点负责管理其运行机器的可用浏览器的插槽**.

节点通过事件总线将其自身注册到分发服务器, 并且将其配置作为注册消息的组成部分一起发送.

默认情况下, 节点自动注册其运行机器路径上的 所有可用浏览器驱动程序. 它还为基于 Chromium 的浏览器和 Firefox 的 每个可用 CPU 创建一个插槽. 对于 Safari 和 Internet Explorer, 只创建一个插槽. 通过特定的配置, 它可以在 Docker 容器或中继命令中运行会话

节点仅执行接收到的命令, 它不进行评估、做出判断或控制任何事情. 运行节点的计算机不需要与其他组件具有相同的操作系统. 例如, Windows 节点可以具有将 Internet Explorer 作为浏览器选项的功能, 而在 Linux 或 Mac 上则无法实现.

### 会话表

会话表是一种数据存储的方式, **用于保存会话 id 和会话运行的节点的信息.** 它作为路由支持, 在向节点转发请求的过程中起作用. 路由将通过会话表获取与会话 id 关联的节点.

### 新会话队列

新会话队列以**先进先出**的顺序保存所有新会话请求. 其具有用于设置请求超时和请求重试间隔的可配置参数.

路由将新会话请求添加到新会话队列并等待响应. 新会话队列定期检查队列中的任何请求是否已超时, 若有，则请求将被拒绝并立即删除.

分发器定期检查是否有可用槽. 若有, 分发器将为第一个匹配的请求索取新会话队列. 然后分发器会尝试创建新的会话.

一旦请求的功能与任何空闲节点槽匹配, 分发器将尝试获取可用槽. 如果没有空闲槽, 分发器会要求队列将请求添加到队列前面. 如果请求在重试或添加到队列头时超时, 则该请求将被拒绝.

成功创建会话后, 分发器将会话信息发送到新会话队列. 新会话队列将响应发送回客户端.

### 事件总线

事件总线充当节点、分发服务器、新的会话队列器和会话表之间的**通信路径.** 网格通过消息进行大部分内部通信, 避免了昂贵的 HTTP 调用. 当以完全分布式模式启动网格时, 事件总线是应该启动的第一个组件.

配置
--

### Standalone 单机模式

*   **下载好 selenium-server-<version>.jar，然后一条命令启动即可**
    
    ```
    java -jar selenium-server-<version>.jar standalone
    
    ```
    
*   **可以打开网页，输入 IP:4444/ui/index.html#/ 看到如下界面**
    

**![](http://vip.ytesting.com//upload/ueditor/upload/image/20220221/1645408089265003525.png)**

*   **可以看到默认是提供了 2 个 firefox 和 2 个 chrome 实例（注意需要你有对应的 webdriver，没有的话，也不会检测到，个数跟你操作系统的 CPU 逻辑处理器的个数有关）**
    
*   **测试代码**
    
    ```
    from selenium import webdriver
    
    option = webdriver.ChromeOptions()
    driver = webdriver.Remote(command_executor='http://192.168.10.173:4444', #此处的ip地址是运行standalone的节点IP地址,4444是默认端口，可以更改
                              options=option)
    driver.get('https://www.baidu.com')
    from time import sleep
    sleep(5)
    driver.quit()
    
    ```
    

*   **如果最后不退出，那么就等待会话超时（亦可通过参数 --session-timeout 来调整时间）后方可使用该实例**
    

### HUB-NODE(S) 模式

*   **在该模式下，hub 包括了以下所有组件的功能**
    
    ```
    Router
    Distributor
    Session Map
    New Session Queue
    Event Bus
    
    ```
    
*   **你只需两条命令分别启动 HUB 和 NODE 即可（如果 node 有多个，则对应多条 node 的启动命令）**
    
    ```
    #hub节点上运行
    java -jar selenium-server-<version>.jar hub
    
    #node节点（非本机）
    java -jar selenium-server-<version>.jar node   --hub 192.168.10.157  #一定要指定hub的信息，不然会注册不上，官网没有这样说
    #node节点（本机）
      java -jar selenium-server-<version>.jar node   
    
    ```
    

*   但 hub 节点上也可以运行一个 node 节点，这个节点就无需指定 --hub 参数
    
*   当然上述的启动方式会默认检测浏览器驱动（搜索 PATH 路径下的 driver.exe）和浏览器个数，配置都是自动的或默认的。
    
*   打开一个会话后的界面如下
    
    ![](http://vip.ytesting.com//upload/ueditor/upload/image/20220221/1645408109447049217.png)
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220221/1645408127766008771.png)

### Distributed 模式

*   **最大型的 Grid 模式，完全分布式**
    
*   **你需要 6-7 条命令来启动每个组件**
    
    ```
    #1  启动event-bus
    java -jar D:\selenium_grid\selenium-server-4.1.1.jar event-bus
    #2  启动sessions
    java -jar D:\selenium_grid\selenium-server-4.1.1.jar sessions #默认是5556端口
    #3  启动sessionqueue
    java -jar D:\selenium_grid\selenium-server-4.1.1.jar sessionqueue #默认是5559端口
    #4  启动distributor
    java -jar D:\selenium_grid\selenium-server-4.1.1.jar distributor --sessions http://192.168.10.157:5556 --sessionqueue http://192.168.10.157:5559 --bind-bus false
    #5  启动router
    java -jar D:\selenium_grid\selenium-server-4.1.1.jar router --sessions http://192.168.10.157:5556 --sessionqueue http://192.168.10.157:5559 --distributor http://192.168.10.157:5553  #默认是5553端口
    
    #6. 启动本机node
    ## 本机节点无需指定hub
    java -jar D:\selenium_grid\selenium-server-4.1.1.jar node #自动注册到本机
    java -jar D:\selenium_grid\selenium-server-4.1.1.jar node --detect-drivers true #检测驱动
    java -jar D:\selenium_grid\selenium-server-4.1.1.jar node --driver-implementation "firefox" --max-sessions 5  #配置指定的浏览器和多少个实例
    
    #7 启动外机node，需要指定hub地址
    java -jar selenium-server-4.1.1.jar node --hub 192.168.10.157 --driver-implementation "firefox" --max-sessions 5
    java -jar selenium-server-4.1.1.jar node --hub 192.168.10.157 --driver-implementation "chrome" --max-sessions 5
    
    ```
    

*   **启动节点的时候可以用配置文件的方式来读取，这里推荐用 toml 格式**
    
    ```
    #node节点使用的toml
    [node]
    driver-implementation = ["chrome"]  #官网此处是错的，用的是driver
    max-sessions = 3
    
    ```
    

命令帮助
----

*   **参考**
    
    ```
    java -jar selenium-server-<version>.jar hub  --help
    java -jar selenium-server-<version>.jar node --help
    
    ```
    

测试实例
----

*   **分布式测试可以结合 pytest-xdist 插件，利用 - n 参数来同时执行多个用例**
    
    ```
    @pytest.mark.parametrize('browsertype',['chrome','firefox'])
    def test_grid(browsertype):
        from selenium import webdriver
        if browsertype =='chrome':
            options = webdriver.ChromeOptions()
        if browsertype == 'firefox':
            options = webdriver.FirefoxOptions()
        driver = webdriver.Remote(command_executor='http://127.0.0.1:4444',
                                  options=options)
        driver.get('https://www.baidu.com')
        from time import sleep
        sleep(5)
        driver.quit()
    
    if __name__ == '__main__':
        pytest.main(['-sv','-n 2',__file__])
    
    ```