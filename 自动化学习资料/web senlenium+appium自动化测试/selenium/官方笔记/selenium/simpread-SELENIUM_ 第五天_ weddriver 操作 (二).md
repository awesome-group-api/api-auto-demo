> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b587eddaee2017f19ebe4fc3def)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220221/1645407251939070930.png)

第一部分：js 操作
----------

> [https://developer.mozilla.org/zh-CN/docs/Web/API/Document_Object_Model](https://developer.mozilla.org/zh-CN/docs/Web/API/Document_Object_Model)
> 
> [https://www.w3school.com.cn/xmldom/index.asp](https://www.w3school.com.cn/xmldom/index.asp)

### 常见的 JS 操作

> js 操作可以再开发者工具的控制台 console 运行，有自动补齐功能

**window**

*   window.open()                   # 打开一个空白页
    
*   window.open('[https://www.jd.com](https://www.jd.com/)')      # 打开京东
    
*   window.scrollTo(x,y)
    

*   配合 document.body.scrollHeight 和 document.body.scrollWidth 使用
    

**document**

*   document.title  
    
*   document.cookie
    
*   document.URL
    
*   document.body.scrollHeight
    
*   document.body.scrollWidth
    
*   元素定位
    

*   getElementById          # 通过 id 的值定位
    
*   getElementsByClassName  # 通过 class 的值定位，返回列表
    
*   getElementsByName       #通过元素的名字定位，返回列表
    
*   getElementsByTagName    # 通过元素的标签定位，返回列表
    
*   querySelector           #通过 css 选择器定位
    
*   querySelectorAll        # 通过 css 选择器定位，返回列表
    

*   元素操作：先定位到再操作
    

*   document.getElementById('username').value='sq1'   #输入 sq1 这个值
    
*   document.getElementById('username').value=''   #clear
    
*   document.querySelector('.pn.vm').click()    # 点击登录按钮
    
*   document.getElementById('password').type='text'  # 修改属性值
    
*   document.querySelector("a[href='admin.php']").removeAttribute('target')    # 移除属性
    

**navigator**

*   navigator.platform
    
*   navigator.userAgent
    
*   navigator.appName
    
*   navigator.appVersion
    

**location**

> [http://121.41.14.39:8088/index.html#/](http://121.41.14.39:8088/index.html#/)  以 UI 对战平台为例

*   location.host  #'121.41.14.39:8088'
    
*   location.hostname  #'121.41.14.39'
    
*   location.protocol  #'http:'
    
*   location.pathname  #'/index.html'
    

**其他**

*   alert('alert')
    
*   confirm('confirm')
    
*   prompt('请输入你的名字')
    

**jquery**

*   $('#username')    
    
*   $('#username').value='sq1'
    

### EXECUTE_SCRIPT 方法

*   初级用法
    
    ```
    driver.execute_script(js)
    
    ```
    
*   高级用法
    
    ```
    #语法
    driver.execute_script(js,arg0)
    driver.execute_script(js,arg0,arg1)
    
    ```
    
*   示例代码
    
    ```
    from selenium import webdriver   
    driver  = webdriver.Chrome()    
    driver.get("http://127.0.0.1/upload/forum.php")
    
    #示例1
    # js = 'arguments[0].value="admin"'
    # ele_username = driver.find_element('id','ls_username')
    # driver.execute_script(js,ele_username)
    
    #示例2
    js = 'arguments[0].value=arguments[1]'
    ele_username = driver.find_element('id','ls_username')
    value = 'admin'
    driver.execute_script(js,ele_username,value)
    
    ```
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220221/1645407287827048908.png)

**示例代码 1：**

```
driver = webdriver.Chrome()
driver.get('http://127.0.0.1/upload/forum.php')
test_flag = 4
if test_flag == 7:  # 封装
    js20 = "arguments[0].value='admin123'"
    ele_username = driver.find_element_by_css_selector('#ls_username')
    sleep(2)
    driver.execute_script(js20, ele_username)
    js21 = "arguments[0].value=arguments[1]"
    #     execute_script(js脚本,第2个参数=>arguments[0],第3个参数=>arguments[1]"
    driver.execute_script(js21, ele_username, 'world')

if test_flag == 6:
    pass
    # TODO  修改UI的日期框->日期选择框->转正日期的readonly属性
    # ele_date = document.getElementById('conversionTime')
    # ele_date.readOnly=false  #此处的readOnly的O是大写的，跟属性值不同
    # js= "ele_date = document.getElementById('conversionTime');ele_date.readOnly=false"
    # 其实也可以去移除这个属性
    #   ele.removeAttribute('readonly')
if test_flag == 5:
    js10 = "document.getElementById('ls_username').value='admin'"
    js11 = "document.getElementsByClassName('px')[1].value='123456'"
    js12 = "document.getElementById('ls_password').type='text'"
    driver.execute_script(js10)
    driver.execute_script(js11)
    driver.execute_script(js12)
if test_flag == 4:
    js7 = "document.getElementById('ls_username').value='admin'"
    js8 = "document.getElementsByClassName('px')[1].value='123456'"
    js9 = "document.getElementsByClassName('pn')[0].click()"
    driver.execute_script(js7)
    print(driver.find_element_by_id('ls_username').text)
    driver.execute_script(js8)
    # driver.execute_script(js9)
    # BUT  UI 和 POLLY都无法实现 ，跟前端的控制有关系
    # 可以输入，可以点击，但是就是无法登录UI和POLLY
    # querySelector     find_element_by_CSS  #document.querySelector('#ls_username')
    # querySelectorAll  find_elements_by_CSS
if test_flag == 3:  # 滚动条
    # 如果要逐步滚动  每次滚动的值不一样
    driver.set_window_size(500, 500)
    sleep(2)
    js3 = 'window.scrollTo(0,10000)'  # 滚动到底部 给y一个大值
    # document.body.scrollHeight 精准
    driver.execute_script(js3)
    sleep(2)
    js4 = 'window.scrollTo(0,0)'  # 滚动到顶
    driver.execute_script(js4)
    sleep(2)
    js5 = 'window.scrollTo(10000,0)'  # document.body.scrollWidth替代
    driver.execute_script(js5)
    sleep(2)
    driver.execute_script(js4)
    sleep(2)
    js6 = 'window.scrollTo(0,document.body.scrollHeight)'
    driver.execute_script(js6)

if test_flag == 2:
    js1 = 'return document.title'  # return不能少
    print(driver.execute_script(js1))
    js2 = 'return document.URL'
    print(driver.execute_script(js2))
if test_flag == 1:  # 打开网页
    driver.execute_script('window.open()')

```

第二部分：三种等待
---------

### 强制等待

*   语法
    
    ```
    from time import sleep
    sleep(2)
    
    ```
    
*   固定的等待
    
*   常用于演示、调试，初学观察效果等
    
*   实际项目中一般不建议在定位元素的时候使用，不可靠（网络的好坏导致等待时间不确定）
    
*   但也有特例要用到
    

*   比如界面操作完要关闭，但是最后你提交了一些操作，如果非常快的退出浏览器有时候会丢失请求，那就需要你强制等待一定时间，这个跟隐式等待还是显式等待都无关。
    

### 隐式等待

*   语法
    
    ```
    driver.implicitly_wait(最大等待时间)  #单位是秒
    
    ```
    
*   参数是最大等待时间，只要在此规定时间内整个页面全部加载完成即可操作元素。
    
*   设置一次，全局（在对应浏览器的整个生命周期内）生效，对所有元素都生效（一视同仁），所以一般都建议放在浏览器打开后立即设置
    
*   但是要注意的是，实际操作过程中你所需要定位的元素可能先加载了，但界面上的其他元素尚未完全加载，你也只能等待，无法操作该元素，而整个页面的加载完成可能会浪费你的等待时间
    
*   对 alert、窗口、元素的属性都无法识别
    

### 显式等待

*   显式等待 是 Selenium 客户可以使用的命令式过程语言。它们允许您的代码暂停程序执行，或冻结线程，直到满足通过的 条件 。只要这个条件返回一个假值，它就会以一定的频率一直被调用，直到等待超时。
    
*   由于显式等待允许您等待条件的发生，所以它们非常适合在浏览器及其 DOM 和 WebDriver 脚本之间同步状态。
    
*   针对隐式等待的浪费时间问题，显式等待就更加有针对性。它是针对某个（或某组）具体的元素或者某种行为（不仅仅是元素定位！），看它是否具备了一定的特征，就开始有所动作了。
    
*   显式等待也有最大等待时间，还提供了轮训间隔，等待某个条件成立或不成立。
    
*   **按照官方说法，不能将隐式等待和显式等待放在一起，会产生意想不到的结果。**
    
*   导入包
    
    ```
    from selenium.webdriver.support.wait import WebDriverWait
    from selenium.webdriver.support import expected_conditions as EC
    
    ```
    
*   语法
    
    ```
    #1. 直接调用
    WebDriverWait(driver, 超时时长, 调用频率(可选,有默认值), 忽略异常(可选,有默认值)).until(可执行方法, 超时时返回的信息)
    
    #2. 分开调用
    mywait = WebDriverWait(driver, timeout, poll_frequency=0.5, ignored_exceptions=None)
    
    mywait.until(method,msg)
    mywait.until_not(method,msg)
    
    ```
    
*   WebDriverWait 参数说明
    

*   driver：浏览器实例
    
*   timeout：最大超时时间，单位是秒
    
*   poll_frequency：轮询的频率，默认 0.5 秒
    
*   ignored_exceptions：超时后的异常信息
    

*   until 和 until_not 说明
    

*   直到该方法成立或不成立，如果超时会抛出后面的 msg
    

<table><thead><tr cid="n2390" mdtype="table_row"><th>方法 (3.X 中都是类)</th><th>说明</th></tr></thead><tbody><tr cid="n2393" mdtype="table_row"><td><strong>alert_is_present</strong></td><td>判断 alert 是否存在，若存在则切换到 alert，若不存在则返回 False</td></tr><tr cid="n2396" mdtype="table_row"><td>element_located_selection_state_to_be</td><td>判断某元素是否与预期相同，相同则返回 True，不同则返回 False，locator 为一个 (by, path) 元组</td></tr><tr cid="n2399" mdtype="table_row"><td>element_located_to_be_selected</td><td>判断某元素是否被选，locator 为一个 (by, path) 元组</td></tr><tr cid="n2402" mdtype="table_row"><td>element_selection_state_to_be</td><td>判断某元素的选中状态是否与预期相同，相同则返回 True，不同则返回 False</td></tr><tr cid="n2405" mdtype="table_row"><td>element_to_be_clickable</td><td>判断某元素是否可访问并且可启用，比如能够点击，若可以则返回元素本身，否则返回 False</td></tr><tr cid="n2408" mdtype="table_row"><td>element_to_be_selected</td><td>判断某元素是否被选中</td></tr><tr cid="n2411" mdtype="table_row"><td><strong>frame_to_be_available_and_switch_to_it</strong></td><td>判断某个 frame 是否可以切换过去，若可以则切换到该 frame，否则返回 False</td></tr><tr cid="n2414" mdtype="table_row"><td>invisibility_of_element</td><td>判断元素是否隐藏 [吴], 继承自 invisibility_of_element_located，入参可以是一个 locator 也可以是 webelement</td></tr><tr cid="n2417" mdtype="table_row"><td>invisibility_of_element_located</td><td>[吴] 判断元素是否隐藏, 入参是 locator</td></tr><tr cid="n2420" mdtype="table_row"><td><strong>new_window_is_opened</strong></td><td>[吴] 新窗口是否打开, 入参是当前的句柄列表</td></tr><tr cid="n2423" mdtype="table_row"><td><strong>number_of_windows_to_be</strong></td><td>[吴] 判断 window 数量是否为 N，入参 N 是数字</td></tr><tr cid="n2426" mdtype="table_row"><td>presence_of_all_elements_located</td><td>用于判断定位的元素范围内，至少有一个元素存在于页面当中，存在则以 list 形式返回元素本身，不存在则报错</td></tr><tr cid="n2429" mdtype="table_row"><td><strong>presence_of_element_located</strong></td><td>用于判断一个元素存在于页面 DOM 树中，存在则返回元素本身，不存在则报错</td></tr><tr cid="n2432" mdtype="table_row"><td>staleness_of</td><td>判断某个元素是否不再附加于于 DOM 树中，不再附加的话返回 True，依旧存在返回 False。可以用于判断页面是否刷新了</td></tr><tr cid="n2435" mdtype="table_row"><td>text_to_be_present_in_element</td><td>判断某文本是否是存在于特定元素的 value 值中，存在则返回 True，不存在则返回 False，对于查看没有 value 值的元素，也会返回 False</td></tr><tr cid="n2438" mdtype="table_row"><td>text_to_be_present_in_element_value</td><td>[吴] 文本在指定元素的 value 属性值中, 入参是 locator 和文本</td></tr><tr cid="n2441" mdtype="table_row"><td>title_contains</td><td>用于判断网页 title 是否包含特定文本（英文区分大小写），若包含则返回 True，不包含返回 False。</td></tr><tr cid="n2444" mdtype="table_row"><td>title_is</td><td>用于判断网页 title 是否是特定文本（英文区分大小写），若完全相同则返回 True，否则返回 False</td></tr><tr cid="n2447" mdtype="table_row"><td>url_changes</td><td>[吴]url 是否改变, 入参是 url</td></tr><tr cid="n2450" mdtype="table_row"><td>url_contains</td><td>[吴]url 是否包含，入参是 url，入参 in 当前的 url</td></tr><tr cid="n2453" mdtype="table_row"><td>url_matches</td><td>[吴]url 是否匹配指定模式，传入一个正则表达式模式</td></tr><tr cid="n2456" mdtype="table_row"><td>url_to_be</td><td>[吴]url 应该是，入参和当前的 url 比较</td></tr><tr cid="n2459" mdtype="table_row"><td>visibility_of</td><td>visibility_of(element) 同面 visibility_of_element_located(locator)，不过参数从 locator 的元组变为元素</td></tr><tr cid="n2462" mdtype="table_row"><td>visibility_of_all_elements_located</td><td>[吴] 所有元素都可见, 传入一个 locator 定位一组元素</td></tr><tr cid="n2465" mdtype="table_row"><td>visibility_of_any_elements_located</td><td>[吴] 只要有一个元素可见，传入一个 locator 定位一组元素</td></tr><tr cid="n2468" mdtype="table_row"><td><strong>visibility_of_element_located</strong></td><td>用于判断特定元素是否存在于 DOM 树中并且可见，可见意为元素的高和宽都大于 0，元素存在返回元素本身，否则返回 False</td></tr></tbody></table>

注意事项

1.  对于 locator 参数，其值为一个元组！如 ('id','username')，前者是 selenium.webdriver.common.by.By 类中的值（8 个属性），后者是具体元素的属性值。
    
2.  这个类和`presence_of_element_located(locator)`有点像，但后者只强调元素存在于 DOM 树中，可见不可见无所谓，而前者要求必须高和宽必须都大于 0，因此后者在性能上会稍微快一点点。
    
3.  visibility_of(element) 看似简单，实则在等待的时候 find 本身可能会有问题而跳过轮询，一般还是推荐使用 visibility_of_element_located(locator)
    

**简易代码**

```
def color_is(your_color):
    def _predicate(person):
        return person.color == your_color
    return _predicate

class China:
    color = 'yellow'

wuxianfeng = China()
print(color_is('yellow')(wuxianfeng))
print(color_is('black')(wuxianfeng))

```

**示例代码 1：不用 expected_conditions 的实例**

```
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
driver = webdriver.Chrome()
driver.get('http://127.0.0.1/upload/forum.php')

wait = WebDriverWait(driver,timeout=5,poll_frequency=0.5)
if wait.until(lambda x:x.find_element_by_id('ls_username')):
    print('找到了id=ls_username的元素')


```

**示例代码 2：until 源码解析及实例**

*   until 源码
    
    ```
        def until(self, method, message=''):
            """Calls the method provided with the driver as an argument until the \
            return value does not evaluate to ``False``.
    
            :param method: callable(WebDriver)
            :param message: optional message for :exc:`TimeoutException`
            :returns: the result of the last call to `method`
            :raises: :exc:`selenium.common.exceptions.TimeoutException` if timeout occurs
            """
            screen = None
            stacktrace = None
    
            end_time = time.time() + self._timeout  #计算轮询结束时间
            print(f'现在是{time.ctime()},进入轮询')
    
            while True:  #循环开始
                try:
                    value = method(self._driver)  #调用method，传入参数self._driver
                    if value:
                        return value
                except InvalidSelectorException as e:
                    raise e
                except self._ignored_exceptions as exc:
                    screen = getattr(exc, 'screen', None)
                    stacktrace = getattr(exc, 'stacktrace', None)
                print(f'我即将等待{self._poll}秒,现在是{time.ctime()}')
                time.sleep(self._poll)
                if time.time() > end_time:
                    print(f'现在是{time.ctime()},退出轮询')
                    break
            raise TimeoutException(message, screen, stacktrace)
    
    ```
    

*   观测 timeout 的效果
    
*   观测 poll_frequency 的效果
    
*   在过程中修改 HTML 中用户名 id 的值为 ls_usernam，抬高 timeout 的值，能看到改对后会立即停止（检测到了）
    

**示例代码 3：text_to_be_present_in_element**

*   方法的作用：检测元素的文本是否存在指定的内容
    

*   传参：第一个是 locator，第二个是包含的文本
    
*   返回：如果存在了就返回 True，否则返回 False
    

*   测试步骤
    

*   延长超时时间和轮询时间，在打开网页后，手工修改指定元素的文本
    

*   实例代码
    
    ```
    from selenium import webdriver
    from selenium.webdriver.support.wait import WebDriverWait
    from selenium.webdriver.support import expected_conditions  as EC
    driver = webdriver.Chrome()
    driver.get('http://127.0.0.1/upload/forum.php')
    dest_text = '软件下载中'
    try:
        wait = WebDriverWait(driver, 10, 1)
        print('最开始元素的文本', driver.find_element_by_css_selector('.lk_content.z p').text)
        wait.until(EC.text_to_be_present_in_element(('css selector','.lk_content.z p'), dest_text))
        print('现在元素的文本', driver.find_element_by_css_selector('.lk_content.z p').text)
    except:
        print(f'元素中没有出现文本:{dest_text}')
    finally:
        print('2s后关闭浏览器')
        driver.quit()
    
    ```
    

第三部分：三种切换
---------

### alert 切换

*   alert() 函数所使用的弹出框是浏览器系统对象而不是网页文档对象！alert() 弹出框会阻断 JavaScript 代码的执行。
    
*   注意不是所有的弹出框都是 alert，比较简单的方法，就是看弹出框的元素是否能定位，如果能定位就不是 alert，如果没有任何元素提示那基本就是 alert。如下图（某个同学的截图，他以为是 alert）。
    
    ![](http://vip.ytesting.com//upload/ueditor/upload/image/20220221/1645407337266080356.png)
    
*   弹窗的三种形式：alert、conform、prompt
    
*   方法
    
    ```
    driver.switch_to.alert.方法
     accept()    #确定，alert只能用他
        dismiss()   #取消，alert、confirm、prompt都有用到
        send_keys() #输入内容，prompt需要用到
    
    ```
    
*   alert 的另外一种处理方法
    
    ```
    from selenium.webdriver.common.alert import Alert
    Alert(driver).
     跟accept() 
        跟dismiss() 
        跟send_keys() 
    
    ```
    

**示例代码 1**

```
from selenium import webdriver
from time import sleep
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions  as EC
driver = webdriver.Chrome()
driver.get(r'D:\pythonProject\AutoSelenium\第6次课_1215_webdriver高级操作（三）和PO初识\test.html')
test_flag = 5
if  test_flag==5:
    # 稳定的切换
    # 问题:  你点击了alert之后你立即去accept等操作，有可能会无法及时弹出alert
    # 面试题：如果定位不到元素，等待
    driver.find_element('id','confirm').click()
    sleep(2)  #为了演示
    alert = WebDriverWait(driver,5,0.5).until(EC.alert_is_present())
    alert.accept()
if test_flag==4:  #另外一种实现
    driver.find_element('id','confirm').click()
    sleep(2)
    Alert(driver).accept()  #不展开
if test_flag ==3:
    driver.find_element('id','prompt').click()
    sleep(2)
    driver.switch_to.alert.send_keys('helloworld')  #输入无显示
    sleep(2)
    driver.switch_to.alert.accept()  #这里能看到helloworld
if test_flag ==2:
    driver.find_element('id','confirm').click()
    sleep(2)
    driver.switch_to.alert.dismiss()  #输出你选择了取消
if test_flag ==1:
    driver.find_element('id','alert').click()
    sleep(2)
    driver.switch_to.alert.accept()

```

### frame 切换

*   frame 的识别：devtools 种 console 界面的 top；xpath 语法定位 iframe
    
*   方法
    
    ```
    driver.switch_to.frame(frame_reference)
    
    frame_reference可以是
         1. frame元素的id属性的值(若有)
         2. frame元素的name属性的值(若有)
            3. frame元素的索引   #注: 第一个frame的索引是0
            4. frame元素本身（find_element得到的对象）
    
    driver.switch_to.parent_frame()      #父框架，相当于linux下的..
    driver.switch_to.default_content()   #默认的路径，相当于linux下的~（在html中其实是/）
    
    ```
    
*   html5 去掉了 frame 和 frameset，而 iframe 是需要切换的，而且有可能可以嵌套
    

**示例代码：**

```
from selenium import webdriver
from time import sleep
driver = webdriver.Chrome()
driver.get(r'D:\pythonProject\AutoTest\AutoSelenium4\第6次课_1215_webdriver高级操作（三）和PO初识\test.html')
# 1. 先在外层写一个用户名admin1
driver.find_element('id','uname').send_keys('admin1')
sleep(2)
# 2. 在里面写一个用户名admin2

driver.switch_to.frame('nf')   #name属性的值
driver.find_element('id','username').send_keys('admin2')
sleep(2)
# 3. 再到外层写一个用户名admin3
driver.switch_to.default_content()
driver.find_element('id','uname').clear()
driver.find_element('id','uname').send_keys('admin3')
sleep(2)
# 4. 再在里面写一个用户名admin4
driver.switch_to.frame('if')  #id属性的值
driver.find_element('id','username').clear()
driver.find_element('id','username').send_keys('admin4')
sleep(2)
# 5. 再在外面写一个用户名admin5
driver.switch_to.parent_frame()
driver.find_element('id','uname').clear()
driver.find_element('id','uname').send_keys('admin5')
sleep(2)
# 6. 再在里面写一个用户名admin6
driver.switch_to.frame(driver.find_element('id','if'))  #webelement的方式
driver.find_element('id','username').clear()
driver.find_element('id','username').send_keys('admin6')
sleep(2)
# 7. 再在外面清空
driver.switch_to.parent_frame()
driver.find_element('id','uname').clear()
sleep(2)
# 8. 再在里面写一个用户名admin8

driver.switch_to.frame(0)
driver.find_element('id','username').clear()
driver.find_element('id','username').send_keys('admin8')

```

**示例代码 2：稳定的 frame 切换**

```
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions  as EC
driver = webdriver.Chrome()
driver.get(r'D:\pythonProject\AutoTest\AutoSelenium4\第6次课_1215_webdriver高级操作（三）和PO初识\test.html')
mywait = WebDriverWait(driver,5,0.5)
test_flag = 2
if test_flag ==1:
    frame_locator = ('id', 'if')  #driver.switch_to.frame(webelement)
    mywait.until(EC.frame_to_be_available_and_switch_to_it(frame_locator))  #已经切换了
# driver.switch_to.frame('if')  这句话不对的， 已经切换了，再在if框架中去找if框架！没有了！
if test_flag == 2: #driver.switch_to.frame(id的值)
    frame_ele = driver.find_element('id','if')
    mywait.until(EC.frame_to_be_available_and_switch_to_it(frame_ele))  #已经切换了

driver.find_element('css selector','#username').send_keys('sq1')

```

### window 切换

*   方法
    
    ```
    switch_to.window(window_name)
    
    window_name可以是窗口的名字或window handle（窗口句柄）
    
    driver.current_window_handle  #当前窗口的句柄
    driver.window_handles         #当前的窗口的句柄列表
    
    ```
    
*   注意事项：
    

*   有时候点击后无法立即产生新的窗口
    
*   产生的新的窗口不在列表的最后
    
*   有的同学会有疑问，我怎样才能关闭多余的窗口呢，切换过去关闭？那自己不是没了嘛？其实是没任何问题的！不能用 quit，用 close！driver 还在就会切换！
    
    ```
    from selenium import webdriver
    from time import sleep
    driver = webdriver.Chrome()
    driver.get('http://121.41.14.39:8088/index.html')
    first_win = driver.current_window_handle  #比如要保留的是第一个窗口，那就记录下来
    sleep(2)
    driver.execute_script('window.open("https://www.baidu.com")') #打开新的窗口
    cur_wins = driver.window_handles  #当前的所有窗口
    driver.switch_to.window(cur_wins[-1])  #切换到新的窗口（假设就是2个窗口，多个要遍历，而且新开的就在最后）
    sleep(2)
    driver.close()  #切换过去后关闭掉
    driver.switch_to.window(first_win)  #切换到你要保留的那个窗口
    print(driver.current_url)
    driver.find_element('id','username').send_keys('123')
    
    ```
    

**示例代码 1：稳定的 window 切换**

```
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions  as EC
driver = webdriver.Chrome() #在此处就打开了一个窗口
old_win = driver.current_window_handle #字符串类型，CDwindow-B8FEABE1EA1CF4A90BA60F6FAA91C074
old_wins = driver.window_handles
print(driver.window_handles) #列表，里面塞的是window_handle
driver.get(r'D:\pythonProject\AutoTest\AutoSelenium4\第6次课_1215_webdriver高级操作（三）和PO初识\test.html')
driver.find_element('link text','百度一下').click()

#1. 点击后不一定立即出现，稳定切换
mywait = WebDriverWait(driver,5,0.5)
# if mywait.until(EC.new_window_is_opened(old_wins)):
# # print(driver.window_handles)  #此时是多个句柄
#     driver.switch_to.window(driver.window_handles[-1])
if mywait.until(EC.number_of_windows_to_be(2)):
    driver.switch_to.window(driver.window_handles[-1])
#     # TODO 新增的窗口不一定是最后一个元素，怎么办?

driver.find_element('css selector','#kw').send_keys('松勤') #百度搜索输入框

```

**示例代码 2：稳定的切换，处理不是最后一个元素**

```
from selenium import webdriver
from time import sleep
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions  as EC
driver = webdriver.Chrome()
old_win = driver.current_window_handle
old_wins = driver.window_handles
print(driver.window_handles)
driver.get(r'D:\pythonProject\AutoTest\AutoSelenium4\第6次课_1215_webdriver高级操作（三）和PO初识\test.html')
driver.find_element('link text','百度一下').click()
mywait = WebDriverWait(driver,5,0.5)
if mywait.until(EC.number_of_windows_to_be(2)):
    #此时已经有多个窗口了
    for win in driver.window_handles[::-1]: #遍历当前多窗口的列表，因为一般都是倒数的，所以从后面遍历
        if win not in old_wins: #如果不在老窗口列表中就表示是新的
            driver.switch_to.window(win) #切换
            break #退出循环，其他的没必要再处理了
driver.find_element('css selector','#kw').send_keys('松勤') #百度搜索输入框

```

第四部分：文件上传
---------

*   input 标签的文件上传直接 send_keys 即可
    
*   非 input 标签的解决方案
    

*   pywinauto
    
    ```
    from pywinauto.keyboard import send_keys
    time.sleep(3)
    send_keys(r'd:\1.png')
    # 回车操作
    send_keys('{VK_RETURN}')
    
    ```
    
*   pywin32
    
    ```
    import win32com.client
    sh = win32com.client.Dispatch('WScript.shell')
    time.sleep(2)  #这个不加有时候不行
    sh.Sendkeys(r'd:\1.png'+'\n')
    
    ```
    
*   pyautogui
    
    ```
    import pyautogui
    time.sleep(2)
    pyautogui.typewrite(r'"d:\1.png"  "d:\2.png"')  #有时候会与系统输入法冲突，可以改写位大写的D试试。
    pyautogui.keyDown('enter')
    pyautogui.keyUp('enter')
    
    ```
    

*   示例代码
    
    ```
    # Author:  wuxianfeng
    # Company: songqin
    # File:    第5个脚本_文件上传.py
    # Date:    21-12-15
    # Time:    下午 9:16
    print()
    '''
    知识点:  
    1. input标签就直接send_keys
    2. 非input标签:第三方库来处理
        2.1 pyautogui  ✓✓
        2.2 pywinauto  ✓
        2.3 pywin32
    '''
    from selenium import webdriver
    from time import sleep
    
    driver = webdriver.Chrome()
    driver.get('http://121.41.14.39:8088/index.html#/')
    driver.find_element('id', 'username').send_keys('sq1')
    driver.find_element('id', 'password').send_keys('123')
    driver.find_element('id', 'code').send_keys('999999')
    driver.find_element('id', 'submitButton').click()
    sleep(2)
    # 1. 点击文件上传
    driver.find_element('xpath', "//span[contains(text(),'文件上传')]").click()
    sleep(2)
    test_flag = 2
    if test_flag == 1:  # 点击单文件上传
        driver.find_element('xpath', "//li[contains(text(),'单文件上传')]").click()
        sleep(1)
        driver.find_element('css selector', '#cover').send_keys(r'd:\1.txt')
    
    if test_flag == 2:  # 点击单文件上传非input
        driver.find_element('xpath', "//li[contains(text(),'非input')]").click()
        sleep(1)
        driver.find_element('css selector', '.el-icon-upload').click()
        sleep(1)
        import win32com.client
    
        sh = win32com.client.Dispatch('WScript.shell')  # 注册组件，对话框，excel应用就不是他，
        sleep(2)
        sh.Sendkeys(r'd:\1.txt' + '\n')
        # TODO 多个文件操作
    
    if test_flag == 3:  # 点击单文件上传非input  #建议深入了解下pyautogui
        # 1. 定位到两个，处理第一个
        driver.find_element('xpath', "//li[contains(text(),'非input')]").click()
        sleep(1)
        driver.find_element('css selector', '.el-icon-upload').click()
        sleep(1)  # 此处的延迟无法用隐式、显式来处理，不在浏览器上，windows提供的对话框
        import pyautogui
    
        pyautogui.typewrite(r'D:\1.txt')  # 有时会跟输入法有关
        pyautogui.press('enter')
    if test_flag == 4:  # 点击单文件上传非input
        driver.find_element('xpath', "//li[contains(text(),'非input')]").click()
        sleep(1)
        driver.find_element('css selector', '.el-icon-upload').click()
        sleep(1)
        from pywinauto.keyboard import send_keys
    
        send_keys(r'D:\1.txt')
        send_keys('{ENTER}')
    
    if test_flag == 5:  #
        driver.find_element('xpath', '//*[contains(text(),"多文件上传")]').click()
        driver.find_element('css selector', '.el-upload__text').click()
        sleep(1)
        import pyautogui
        pyautogui.typewrite(r'"d:\1.jpg"  "d:\2.jpg" ')
        sleep(2)
        pyautogui.press('enter')
    
    sleep(5)
    driver.quit()
    
    ```