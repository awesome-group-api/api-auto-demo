> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b5879e220ea017a2c3156a577c9)

> 面向对象，即 OOP，Object Oriented Programming，是一种程序设计方法。

概念
--

*   面向对象，即 OOP，Object Oriented Programming，是一种程序设计方法。
    
*   python 中一切皆对象，通过 type 方法可以看到任何数据都是以 class 的形式存在的
    
    ```
    >>> type(1)
    <class 'int'>
    
    ```
    
*   面向对象最重要的概念就是类（Class）和实例（Instance），类是抽象的模板。实例是根据类创建出来的一个个具体的 “对象”，每个对象都拥有相同的方法，但各自的数据可能不同。
    

*   比如人类是一个类，那你就是一个实例（具体的人）
    
*   比如学生是一个类，那你就是一个实例（具体的学生）
    
*   比如三角形是个类，那么边长分别为 3-4-5 的三角形就是一个实例（具体的三角形）
    

类定义
---

```
class 类名(父类列表):
    数据和方法的定义

```

*   类名：一般建议是驼峰命名，首字母一般大写
    
*   示例
    
    ```
    class  Person(object):  #python中所有类都继承自object
    #class  Person():       #没有继承非object类的时候()可以写
    #class Person:          #也可以不写
        pass
    
    ```
    

类的实例化
-----

*   示例
    
    ```
    class Person:
        pass
    zhangsan = Person()  #实例化，()不能少
    
    ```
    
*   实际是在调用`__init__`方法，没有这个方法就是什么都不做（不需要传递任何参数）。参见下文的类的魔术方法。
    

类变量
---

*   访问类的数据，用**类名. 变量名**的
    
*   示例
    
    ```
    class Person:
        head_num = 1   #这是一个类变量
    
    print(Person.head_num)  #可以直接输出1
    
    ```
    

* * *

*   python 中没有像 java 中的 private 将类的数据和方法设置为私有，python 是通过名字的变更（加双下划线）来实现的。
    
*   示例
    
    ```
    class Person:
        __sex = 'unknow'
    print(Person.__sex)  #AttributeError: type object 'Person' has no attribute '__sex'
    
    ```
    
*   如果要访问，建议是增加个方法来访问该数据。但也可以通过下面这种方式来访问
    
    ```
    print(Person._Person__sex)
    
    ```
    

* * *

*   类属性可以自由添加
    
*   属性更改：python 是一门动态语言，类的属性可以自行添加（也有人觉得不好，那 python 也提供了方式来控制）
    
    ```
    class Person:
        head_num = 1
    zhangsan = Person()
    zhangsan.head_num=2   #更改属性值
    print(zhangsan.head_num)
    zhangsan.sex = 'male'  #添加属性值
    print(zhangsan.sex)
    
    
    ```
    
*   可以通过 setattr 内建方法来批量设置属性
    
    ```
    attr_value = {
        'color':'yellow',
        'height':'178cm',
        'weight':'75kg',
    }
    for attr,value in attr_value.items():
        setattr(zhangsan,attr,value)
    print(zhangsan.color)    #yellow
    print(zhangsan.weight)   #75kg
    
    ```
    

* * *

类的方法
----

*   类中的函数称为方法，方法有很多种
    
*   类方法的命名一般是小写的单词组合，可以用下划线连接
    
*   调用类方法的方式，就是**类名. 方法名 (参数)** 的方式
    

### 类的实例方法

*   类的方法的第一个参数是 self，这是个约定，可以不写 self，但不推荐，
    
*   示例
    
    ```
    class Person:
        def  say_hi(self):
            print('Hi')
    zhangsan = Person()
    zhangsan.say_hi()   #输出Hi
    
    ```
    

### 类的魔术方法

*   双下划线开头的方法都是私有方法
    
*   前后都是双下划线的方法是特殊的，称之为魔术方法，比如构造方法__init__
    
    ```
    class Person:
        def __init__(self,sex): #形参 sex
            self.xingbie = sex  #初始化的时候，sex具体的值赋值给实例的xingbie变量
    zhangsan = Person('男')     #传入一个参数sex，self是不要传入的
    #实例化的时候自动去调用__init__方法，将sex这个参数的值'男'传递给张三这个实例的xingbie变量
    james = Person('male')
    #实例化的时候自动去调用__init__方法，将sex这个参数的值'male'传递给james这个实例的xingbie变量
    
    print(zhangsan.xingbie)   #输出 男
    print(james.xingbie)      #输出 male
    
    ```
    

* * *

*   本质上实例化的过程是这样的
    
    ```
    class Person:
        def __init__(self,sex):
            self.xingbie = sex
    
    lisi = object.__new__(Person)
    Person.__init__(lisi,'女')
    print(lisi.xingbie)
    
    ```
    
*   常见的魔术方法和特殊属性
    
    <table><thead><tr cid="n103" mdtype="table_row"><th>魔术方法</th><th>说明</th></tr></thead><tbody><tr cid="n106" mdtype="table_row"><td>__init__()</td><td>构造方法</td></tr><tr cid="n109" mdtype="table_row"><td>__del__()</td><td>析构方法</td></tr><tr cid="n112" mdtype="table_row"><td>__call__()</td><td>实例 () 会执行的方法</td></tr><tr cid="n115" mdtype="table_row"><td>__len__()</td><td><strong>len(实例)</strong> 的时候做的事情，等价于<strong>实例.__len_()</strong></td></tr><tr cid="n118" mdtype="table_row"><td>__add__()</td><td>重载加法，类似的还有__sub__()、__div__()、__mul__()、__mod__()、__pow__()</td></tr><tr cid="n121" mdtype="table_row"><td>__iter__()</td><td>迭代器方法，该类可以用 for 循环遍历需要实现的方法</td></tr><tr cid="n124" mdtype="table_row"><td>__getitem__()</td><td>该类可以用 [] 来取索引需要实现的方法，类似的还有 setitem 和 delitem</td></tr></tbody></table><table><thead><tr cid="n128" mdtype="table_row"><th>特殊属性</th><th>说明</th></tr></thead><tbody><tr cid="n131" mdtype="table_row"><td>__doc__</td><td>说明文档（函数也有该属性）</td></tr><tr cid="n134" mdtype="table_row"><td>__file__</td><td>文档所在的绝对路径</td></tr><tr cid="n137" mdtype="table_row"><td>__module__</td><td>当前操作的对象所在的模块</td></tr><tr cid="n140" mdtype="table_row"><td>__class__</td><td>当前操作的对象所在的类</td></tr><tr cid="n143" mdtype="table_row"><td>__dict__</td><td>列出类或对象中的所有成员</td></tr><tr cid="n146" mdtype="table_row"><td>__author__</td><td>作者信息</td></tr><tr cid="n149" mdtype="table_row"><td><br></td><td><br></td></tr></tbody></table>

### 静态方法 @staticmethod

> [https://blog.csdn.net/cunchi4221/article/details/107474514](https://blog.csdn.net/cunchi4221/article/details/107474514)

*   静态方法绑定到一个类而不是该类的对象。
    
*   当我们需要某些功能而不是对象，而需要完整的类时，我们可以使方法静态化。
    
*   在静态方法中，我们不需要将`self`作为第一个参数传递
    
*   静态方法的实现方法之一：不推荐
    
    ```
    class Person:
        def sayhi():
            print('Hi')
    Person.sayhi = staticmethod(Person.sayhi)
    Person.sayhi()  #Hi
    
    zhangsan= Person()
    zhangsan.sayhi()   #Hi,可以通过实例来调用，但并不推荐
    
    ```
    
*   静态方法的实现方法之二：推荐方法
    
    ```
    class Person:
        @staticmethod
        def sayhi():
            print('Hi')
    
    Person.sayhi()  #Hi
    
    zhangsan = Person()
    zhangsan.sayhi()    #Hi,可以通过实例来调用，但并不推荐
    
    ```
    

### 类方法 @classmethod

*   类方法是用类本身调用的方法（实例也能调用）。 这些方法通常定义对象的行为并修改对象的属性或实例变量。
    
*   示例
    
    ```
    class Person:
        @classmethod
        def run(cls):
            print('run run run ',cls)
    
    Person.run()  #run run run  
    
    ```
    

<table><thead><tr cid="n182" mdtype="table_row"><th><br></th><th>实例方法</th><th>静态方法</th><th>类方法</th></tr></thead><tbody><tr cid="n187" mdtype="table_row"><td>定义</td><td><br></td><td>@staticmethod 装饰</td><td>@classmethod 装饰</td></tr><tr cid="n192" mdtype="table_row"><td>参数</td><td>第一个参数 self</td><td>参数无要求</td><td>第一个参数 cls</td></tr><tr cid="n197" mdtype="table_row"><td>调用</td><td>实例调用</td><td>类调用，不推荐实例调用（但可）</td><td>类调用，不推荐实例调用（但可）</td></tr><tr cid="n202" mdtype="table_row"><td>是否常用</td><td>Y</td><td>N</td><td>N</td></tr><tr cid="n207" mdtype="table_row"><td>应用场景</td><td>传递实例的属性和方法；也可以传递类的属性和方法</td><td>不能使用类或实例的任何属性和方法</td><td>传递类的属性和方法（不能传实例的属性和方法）</td></tr><tr cid="n212" mdtype="table_row"><td><br></td><td><br></td><td><br></td><td><br></td></tr></tbody></table>

> 类方法的一个例子

*   需求：有个班级类，有个学生类，学生类继承自班级类，要求每当有一个学生实例化的时候，班级类中学生总数就加 1
    
*   参考代码
    
    ```
    class MyClass:
        __student_num = 0
        @classmethod
        def add_student(cls):
            cls.__student_num+=1
        @classmethod
        def get_studentnum(cls):
            return cls.__student_num
        def __new__(cls, *args, **kwargs):
            MyClass.add_student()
            return super().__new__(cls)
    class Student(MyClass):
        pass
    
    a1 = Student()
    a2 = Student()
    a3 = Student()
    print(MyClass.get_studentnum())
    
    ```
    

* * *

继承和多态
-----

*   类是抽象的模板，那模板很多的时候就是用来套用的，这就是继承。
    
*   继承的好处在于公共的部分放在基类（父类、超类）中，子类关心自己的实现即可，子类拥有父类的一切；如果子类实现了父类的方法，那么优先采用自己的方法（这是多态）
    
*   示例
    
    ```
    class Person:
        def __init__(self,name,sex):
            self.mingzi = name
            self.xingbie = sex
        def eat(self):
            print('i cat eat')
        def introduce(self):
            print(f'my name is {self.mingzi},i am {self.xingbie}')
    class Chinese(Person): #中国人是人类，继承了人类的属性和方法
        def introduce(self):  #重写introduce方法，多态
            print(f'我叫{self.mingzi},我是{self.xingbie}的')
    
    kobe = Person('kobe','male')
    kobe.introduce()  #my name is kobe,i am male
    zhangsan = Chinese('张三','男')  
    zhangsan.introduce()   #我叫张三,我是男的
    zhangsan.eat()       #i cat eat；子类可以用超类的方法
    
    
    print(isinstance(kobe, Person))   #True
    print(isinstance(zhangsan, Chinese))  #True，子类是Chinese，但也是Person
    print(isinstance(zhangsan, Person))   #True
    
    ```
    

* * *

**多重继承**

*   子类可以继承多个父类，子类在调用某个方法或属性的时候，首先在自身寻找，如果没有就找第一个父类，如果没有就找第二个父类
    
*   如果父类还有父类，那遵循深度优先的原则
    
*   示例
    
    ```
    class  A_1():
        pass
        # def test(self):
        #     print('A1 testing')
    
    class A_2(A_1):    
        pass
        # def test(self):
        #     print('A2 testing')
    
    class B_1():
        # pass
        def test(self):
            print('B1 testing')
    class B_2(B_1):
        pass
        # def test(self):
        #     print('B2 testing')
    
    class C(A_2,B_2):
        pass
        # def test(self):
        #     print('C testing')
    c = C()
    c.test()
    #你需要逐一注释那些类的方法定义
    #如第一次，反注释C自己的test方法，会发现调用的是C自己的test方法
    #第二次，反注释A2的test方法，注释C的test方法，你会发现调用A2的test方法
    #第三次，注释C和A2的test方法，再反注释A1的test方法，你会发现调用A1的test方法
    #第四次，注释C/A1/A2的test方法，反注释B2的test方法，你会发现调用B2的test方法
    
    ```
    
*   综上，继承的调用顺序是类似于这样的（继承顺序从左到右，深度优先）
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20211019/1634623800935094272.png)

### 单例模式

> [https://zhuanlan.zhihu.com/p/37534850](https://zhuanlan.zhihu.com/p/37534850)

> 23 种设计模式可以分为三大类：创建型模式、行为型模式、结构型模式。单例模式属于创建型模式的一种，单例模式是最简单的设计模式之一：**单例模式只涉及一个类，确保在系统中一个类只有一个实例，并提供一个全局访问入口**。许多时候整个系统只需要拥有一个全局对象，这样有利于我们协调系统整体的行为。
> 
> 很多初学者喜欢用**全局变量**，因为这比函数的参数传来传去更容易让人理解。确实在很多场景下用全局变量很方便。不过如果代码规模增大，并且有多个文件的时候，全局变量就会变得比较混乱。你可能不知道在哪个文件中定义了相同类型甚至重名的全局变量，也不知道这个变量在程序的某个地方被做了怎样的操作。

#### 介绍

*   单例是一种**设计模式**，应用该模式的类只会生成一个实例。
    
*   单例模式的作用是：让类创建的对象，在内存中只有唯一的实例（内存地址一样）
    

#### 应用场景

**1、 日志类**

日志类通常作为单例实现，并在所有应用程序组件中提供全局日志访问点，而无需在每次执行日志操作时创建对象。

**2、 配置类**

将配置类设计为单例实现，比如在某个服务器程序中，该服务器的配置信息存放在一个文件中，这些配置数据由一个单例对象统一读取，然后服务进程中的其他对象再通过这个单例对象获取这些配置信息，这种方式简化了在复杂环境下的配置管理。

**3、工厂类**

假设我们设计了一个带有工厂的应用程序，以在多线程环境中生成带有 ID 的新对象（Acount、Customer、Site、Address 对象）。如果工厂在 2 个不同的线程中被实例化两次，那么 2 个不同的对象可能有 2 个重叠的 id。如果我们将工厂实现为单例，我们就可以避免这个问题，结合抽象工厂或工厂方法和单例设计模式是一种常见的做法。

**4、以共享模式访问资源的类**

比如网站的计数器，一般也是采用单例模式实现，如果你存在多个计数器，每一个用户的访问都刷新计数器的值，这样的话你的实计数的值是难以同步的。但是如果采用单例模式实现就不会存在这样的问题，而且还可以避免线程安全问题。

**5、在 Spring 中创建的 Bean 实例默认都是单例模式存在的。**

**6、其他情况**

还比如系统中可以存在多个打印任务，但同一时刻就一个正在运行的；一个音乐播放器可以播放很多音乐，但一次就播放一个音乐。游戏中需要有 “场景管理器” 这样一种东西，用来管理游戏场景的切换、资源载入、网络连接等等任务。这个管理器需要有多种方法和属性，在代码中很多地方会被调用，且被调用的必须是同一个管理器，否则既容易产生冲突，也会浪费资源。这种情况下，单例模式就是一个很好的实现方法。

#### 原理说明

*   python 的__new__方法
    

*   object 基类提供的内置静态方法
    
*   在内存中为对象分配空间
    
*   返回对象引用
    
*   Python 解释器获得对象引用后，将引用作为第一个出参传递给__init__方法（所以 new 在 init 之前）
    

*   关于__new__和__init__的一些区别
    

*   __init__通常用于初始化一个新实例，控制这个初始化的过程，比如添加一些属性， 做一些额外的操作，发生在类实例被创建完以后。它是实例级别的方法。
    
*   __new__通常用于控制生成一个新实例的过程。它是类级别的方法。
    
*   __new__至少要有一个参数 cls，代表要实例化的类，此参数在实例化时由 Python 解释器自动提供
    
*   __new__必须要有返回值，返回实例化出来的实例，这点在自己实现 **new** 时要特别注意，可以 return 父类 **new** 出来的实例，或者直接是 object 的 **new** 出来的实例
    
*   可以将类比作制造商，**new** 方法就是前期的原材料购买环节，**init** 方法就是在有原材料的基础上，加工，初始化商品环节
    

#### 单例的优缺点

**优点：**

*   单例对象在内存中只有一个，节省内存空间；
    
*   可以避免频繁的创建销毁对象，减轻 垃圾回收 工作，进而可以提高性能；
    
*   避免对共享资源的多重占用，简化访问；
    
*   为整个系统提供一个全局访问点。
    

**缺点：**

*   不适用于变化频繁的对象；
    
*   滥用单例将带来一些负面问题，如为了节省资源将数据库连接池对象设计为的单例类，可能会导致共享连接池对象的程序过多而出现连接池溢出；
    
*   如果实例化的对象长时间不被利用，系统会认为该对象是垃圾而被回收，这可能会导致对象状态的丢失；
    

#### 单例模式的实现

**new 关键字**

```
class A:
    __instance = None #类变量 它用来存放实例，如果 _instance 为 None，则新建实例，否则直接返回 _instance 存放的实例。
    __flag = True  #类变量
    def __new__(cls, *args, **kwargs): #固定的部分
        if cls.__instance is None: #如果类从来没被调用过，那么就是None（最初的样子）
            cls.__instance = object.__new__(cls) #如果调用过，就不是None了
            #cls.__instance = super().__new__(cls) #也可以这么写
            #cls.__instance = object.__new__(cls, *args, **kwargs)
        return cls.__instance #new方法必须要返回一个实例
    def __init__(self): #如果只做上面的，那么init方法仍然会多次调用 ， 如果你希望值做一次init，同理可以这么玩。
        if A.__flag:
            print('haha')
            A.__flag =False
a = A()
aa = A()
print(a)
print(aa)
print(a._A__flag)
print(a._A__instance)


----输出
haha
<__main__.A object at 0x0000025117E7F988>
<__main__.A object at 0x0000025117E7F988>
False
<__main__.A object at 0x0000025117E7F988>

```

*   在这里要注意一点
    
    ```
    class Singleton1(object):
        def __new__(cls, *args, **kwargs):
            if not hasattr(cls,'_instance'):
                cls._instance = super().__new__(cls)
            return cls._instance
        
    class Singleton1(object):
        def __new__(cls, *args, **kwargs):
            if not hasattr(cls,'__instance'):  #双下划线不行，单的可以，因为__是私有变量无法访问
                cls.__instance = super().__new__(cls)
            return cls.__instance
    
    ```
    

**单例模式的其他实现方式 ：装饰器实现**

```
def singleton(cls):
    _instance = {}
#★使用不可变的类地址作为键，其实例作为值，每次创造实例时，首先查看该类是否存在实例，存在的话直接返回该实例即可，否则新建一个实例并存放在字典中。
    def inner():
        if cls not in _instance:
            _instance[cls] = cls()
        return _instance[cls]
    return inner
    
@singleton
class Cls(object):
    def __init__(self):
        pass

cls1 = Cls()
cls2 = Cls()
print(id(cls1) == id(cls2))

```

**单例模式的其他实现：类装饰器**

```
class Singleton(object):
    def __init__(self, cls):
        self._cls = cls
        self._instance = {}
    def __call__(self):
        if self._cls not in self._instance:
            self._instance[self._cls] = self._cls()
        return self._instance[self._cls]

@Singleton
class Cls2(object):
    def __init__(self):
        pass

cls1 = Cls2()
cls2 = Cls2()
print(id(cls1) == id(cls2))

```

*   由于是类，还可以这么用
    
    ```
    class Cls3():
        pass
    
    Cls3 = Singleton(Cls3)
    cls3 = Cls3()
    cls4 = Cls3()
    print(id(cls3) == id(cls4))
    
    ```
    

**单例模式的其他实现：metaclass**

*   了解在 Python 中一个类和一个实例是通过哪些方法以怎样的顺序被创造的。
    
*   简单来说，**元类** (**metaclass**) 可以通过方法 **metaclass** 创造了**类 (class)**，而**类 (class)** 通过方法 **new** 创造了**实例 (instance)**。
    
    在单例模式应用中，在创造类的过程中或者创造实例的过程中稍加控制达到最后产生的实例都是一个对象的目的。
    
*   同样，我们在类的创建时进行干预，从而达到实现单例的目的。
    
    ```
    def func(self):
        print("do sth")
    
    Klass = type("Klass", (), {"func": func})
    
    c = Klass()
    c.func()  #用type创造了一个类
    
    ```
    
    ```
    #这里，我们将 metaclass 指向 Singleton 类，让 Singleton 中的 type 来创造新的 Cls4 实例。
    class Singleton(type):
        _instances = {}
        def __call__(cls, *args, **kwargs):
            if cls not in cls._instances:
                cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
            return cls._instances[cls]
    
    class Cls4(metaclass=Singleton):
        pass
    
    cls1 = Cls4()
    cls2 = Cls4()
    print(id(cls1) == id(cls2))
    
    ```
    

---

### pytest 介绍

*   简单灵活, 容易上手, 文档丰富;
    
*   支持参数化, 可以更细力度地控制需要测试的测试用例
    
*   能够支持简单的单元测试和复杂的功能测试, 还可以用来做 selenium/appnium 等自动化测试, 接口自动化测试 (pytest+requests);
    
*   pytest 具有很多第三方插件, 并且可以自定义扩展, 比较好用的如 pytest-selenium(集成 selenium),pytest-html(测试报告生成),pytest-rerunfailures(失败 case 重复执行),pytest-xdist(多 cpu 分发) 等;
    
*   测试用例的 skip 和 xfail 处理
    
*   可以很好的和 CI 工具结合, 例如 Jenkins
    

### pytest 基础

> 基于 Python 3.7.5, pytest-6.2.5 测试

*   命名规范
    

*   测试类 test 开头也不行的
    
*   带 init 的话会有报错
    
    ```
    cannot collect test class 'Test02' because it has a __init__ constructor
    
    ```
    
*   比如 testa、test01 或者 atest 都是不行的
    
*   测试文件以 test_开头 (以_test 结尾也可以)    # 这些原则是可以打破的，可以通过配置文件更改
    
*   测试类以 Test 开头, 并且不能带有__init__方法
    
*   测试函数必须以 test 开头（不需要是 test_）
    
*   断言使用基本的 assert 即可
    
*   所有的 package 必须有__init__.py 文件
    

*   查找测试的策略：默认会递归寻找当前目录下的所有（符合命名规范的）测试脚本
    
*   控制台 (console) 实用参数
    
*   控制台 (console) 实用参数
    
    <table><thead><tr cid="n1595" mdtype="table_row"><th>参数</th><th>说明</th><th>示例</th></tr></thead><tbody><tr cid="n1599" mdtype="table_row"><td>--collect-only</td><td>显示哪些用例被选到</td><td><br></td></tr><tr cid="n1603" mdtype="table_row"><td>-s</td><td>显示程序中的 print/logging 输出</td><td><br></td></tr><tr cid="n1607" mdtype="table_row"><td>-v</td><td>丰富信息模式, 输出更详细的用例执行信息</td><td><br></td></tr><tr cid="n1611" mdtype="table_row"><td>-k</td><td>keyword，运行包含某个字符串的测试用例。如：</td><td>pytest -k add XX.py 表示运行 XX.py 中包含 add 的测试用例。</td></tr><tr cid="n1615" mdtype="table_row"><td>-x</td><td>出现一条测试用例失败就退出测试。在调试阶段非常有用，当测试用例失败时，应该先调试通过，而不是继续执行测试用例。</td><td><br></td></tr><tr cid="n1619" mdtype="table_row"><td>-m</td><td>mark，运行带有标记的测试用例</td><td>pytest -m 'm1';pytest -m 'm1 and &nbsp;m2'</td></tr><tr cid="n1623" mdtype="table_row"><td>-h</td><td>查看帮助</td><td><br></td></tr><tr cid="n1627" mdtype="table_row"><td>-q</td><td>只显示整体的测试结果，相当于静默输出</td><td><br></td></tr><tr cid="n1631" mdtype="table_row"><td>--markers</td><td>显示所有 pytest 支持的 markers</td><td>不是测试文件中的 mark</td></tr><tr cid="n1635" mdtype="table_row"><td>--setup-show</td><td>回溯 fixture 的执行过程</td><td><br></td></tr><tr cid="n1639" mdtype="table_row"><td>--fixtures</td><td>列出所有可供测试使用的 fixture，包括重命名的</td><td><br></td></tr></tbody></table>

#### fixture 及 conftest

*   ##### 基本概念
    

*   函数不能以 test 开头，要和测试用例有区别
    
*   类似于 setup 和 teardown，但更加灵活
    
*   fixture 是测试函数运行前后，由 pytest 执行的外壳函数。常用于配置测试前系统的初始状态、为批量测试提供数据源、传入测试中的测试集
    
*   装饰器 @pytest.fixture() 声明一个函数是一个 fixture。那么如果测试函数的参数列表中含有这个 fixture 的名字，pytest 会在测试函数运行前执行该 fixture（自动扫描）。所以 fixture 的命名、参数的命名都非常重要。
    
*   fixture 可以放在一个测试文件中，也可以放在一个叫 conftest.py 的文件中供多个文件共享。
    
*   conftest.py 的作用域是当前目录及子目录，conftest.py 可以存在多个（肯定不是同一个目录下）
    
*   conftest.py 是特殊的模块，无法 import。conftest.py 被 pytest 视为本地插件库，可以把 conftest 看做是一个供当前目录下所有测试文件使用的 fixture 仓库。
    
*   如果 fixture 函数包含 yield，那么系统会在 yield 处停止，转去运行测试，等测试函数运行完毕再回到 fixture 执行 yield 后面的内容。在这里把 yield 前的内容视为 setup，把 yield 后的视为 teardown。但是无论测试过程中发生了什么 yield 之后的内容都会执行的。
    
*   可以用 --setup-show 回溯 setup 的运行过程！
    
*   fixture 中可以返回数据给测试函数，可以是 return（那就没有 teardown，你写会语法错误，return 不能同级还有语句），一般都用 yield。返回的数据也可以是任意类型，多种数据。
    
*   fixture 的 scope
    
    <table><thead><tr cid="n1752" mdtype="table_row"><th>scope</th><th>含义</th></tr></thead><tbody><tr cid="n1755" mdtype="table_row"><td>scope='session'</td><td>每个会话执行一次</td></tr><tr cid="n1758" mdtype="table_row"><td>scope='module'</td><td>每个模块运行一次</td></tr><tr cid="n1761" mdtype="table_row"><td>scope='class'</td><td>每个类执行一次</td></tr><tr cid="n1764" mdtype="table_row"><td>scope='function'</td><td>每个函数运行一次，默认值</td></tr></tbody></table>

*   除了在函数的参数列表中使用 fixture，或者用 autouse 来自动使用 fixture，也可以用 @pytest.mark.usefixtures('f1','f2') 来标记测试函数或测试类，使用 usefixtures 的时候需要在参数列表中指定 fixture 的名字，一般我们不用在测试函数中（但可以用），但非常适合用于测试类。
    
*   fixture 的 2 种调用方法的区别在于，用参数的方法能使用 fixture 的返回值，用 usefixtures 可不行。