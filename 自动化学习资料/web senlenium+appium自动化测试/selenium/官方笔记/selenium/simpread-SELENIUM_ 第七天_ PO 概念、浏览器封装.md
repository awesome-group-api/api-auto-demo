> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b587eddaee2017f1a0c300a3e55)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220221/1645409203588029912.png)

第一部分：PO 概念
----------

*   一个没有 PO 实现的测试用例
    
    ```
    from selenium import webdriver
    from selenium.webdriver.common.by import By
    from selenium.webdriver.support.wait import WebDriverWait
    from selenium.webdriver.support import expected_conditions as EC
    from time import sleep
    import pytest
    class TestPolly:
        pollyurl = 'http://120.55.190.222:38090/#/login'
        username = ('id','username')
        def test_login_001(self):
            driver = webdriver.Chrome()
            driver.maximize_window()
            driver.implicitly_wait(20)
            driver.get(self.pollyurl)
            driver.find_element(*self.username).send_keys('松勤老师')
            driver.find_element_by_id('password').send_keys('123456')
            driver.find_element_by_id('btnLogin').click()
            assert driver.find_element_by_css_selector(".no-redirect").text =='首页'
    
        def test_add_product_001(self):
            driver = webdriver.Chrome()
            driver.maximize_window()
            driver.implicitly_wait(20)
            driver.get(self.pollyurl)
            driver.find_element(*self.username).send_keys('松勤老师')
            driver.find_element_by_id('password').send_keys('123456')
            driver.find_element_by_id('btnLogin').click()
            driver.find_element_by_xpath("//span[contains(text(),'商品管理')]").click()
            sleep(2)
            driver.find_element_by_css_selector("a[href='#/pms/addProduct'] span").click()
            mywait = WebDriverWait(driver,5,0.5)
            ele_product = mywait.until(EC.presence_of_element_located((By.XPATH,"//span[text()='添加商品']")))
            assert ele_product.is_enabled()
    
    if __name__ == '__main__':
        pytest.main(['-sv',__file__])
    
    ```
    
*   上述代码的问题：
    

*   重用性低：登录功能重复
    
*   可维护性差：数据和代码混合
    
*   可读性差：元素定位方法杂乱（id、xpath、css 混杂）
    
*   可读性差：不易识别操作的含义（特别是 css 和 xpath 语法）
    
*   可维护性差：如果某个元素的属性改了，你要更改多次
    

*   PO 模型
    

*   Page Object Model 页面对象模型
    
*   PO 是自动化测试项目开发实践的最佳设计模式之一
    

*   **设计准则**
    
    > [https://github.com/SeleniumHQ/selenium/wiki/PageObjects](https://github.com/SeleniumHQ/selenium/wiki/PageObjects)
    

*   相同的操作（但可能是不同的数据）带来的不同的结果可以封装成不同的方法。
    

1.  The pulic methods represent the services that the page offers
    

1.  使用**公共方法**来代表**页面提供的服务**
    
2.  基类（基础服务：页面的基础的服务：点击元素，输入内容）  
    
3.  页面类：登录、添加商品
    

3.  Try not to expose the internals of the page
    

1.  不要暴露页面的内部细节（比如元素 ，元素的定位方法等），隔离了测试用例和业务和页面对象
    

5.  Generally don't make assertions
    

1.  PO 本身通常（绝）不应进行判断或断言. 判断和断言是测试的一部分, 应始终在测试的代码内, 而不是在 PO 中. PO 用来包含页面的表示形式, 以及页面通过方法提供的服务, 但是与 PO 无关的测试代码不应包含在其中
    

7.  **Methods return other PageObjects**
    

1.  方法返回其他的页面对象，进行页面的关联
    

1.  登录 -> 首页，首页 -> 添加商品，推荐正向返回
    
2.  添加商品，登出 -> 登录页，不推荐反向返回
    

9.  Need not represent an entire page
    

1.  PO 不一定需要代表整个页面，定义你所需要实现的业务的部分即可，所以在我们的测试中有的页面对象可以非常简单（用什么写什么）. PO 设计模式可用于表示页面上的组件. 如果自动化测试中的页面包含多个组件, 则每个组件都有单独的页面对象, 则可以提高可维护性.
    

11.  Different results for the same action are modelled as different methods
    

1.  相同的操作（但可能是不同的数据）带来的不同的结果可以封装成不同的方法。
    

13.  补充
    
    ```
    1. 实例化PO时, 应进行一次验证, 即验证页面以及页面上可能的关键元素是否已正确加载. 在上面的示例中, SignInPage和HomePage的构造函数均检查预期的页面是否可用并准备接受测试请求（selenium官网提到的）
    2. 上述只是一个经验总结，非强制 
    
    ```
    

*   分层模型：
    

*   **表现层**：页面中可见的元素，都属于表现层。（元素定位器的编写）
    
*   **操作层**：对页面可见元素的操作。点击、输入文本、获取文本等。（基类）
    
*   **业务层**：上面 2 层的组合，并联合到一起形成某个业务动作，在页面中对若干元素操作后所实现的功能。（页面类的方法，也可以是多个页面的组合）
    
*   **测试用例就是组合了 1 个或多个页面的方法，操作对应的元素，完成的测试。测试用例本身不在 PO 内**
    

*   换句话来说
    

*   XX 页面中的 XX 元素
    
*   XX 模块下的 XX 页面
    
*   XX 页面的 XX 元素的动作 1+.. 动作 2+.. 动作 3 形成的业务
    

*   非 PO 结构和 PO 结构对比
    
    ![](http://vip.ytesting.com//upload/ueditor/upload/image/20220221/1645409421424030495.png)
    
*   PO 架构图
    
    ![](http://vip.ytesting.com//upload/ueditor/upload/image/20220221/1645409440816011485.png)
    
    ![](http://vip.ytesting.com//upload/ueditor/upload/image/20220221/1645409449797077706.png)
    
    ![](http://vip.ytesting.com//upload/ueditor/upload/image/20220221/1645409456569024066.png)
    

第二部分：项目结构
---------

*   selenium 的实战是逐步展示这个结构的（需要什么建什么，让大家去理解为何要这么维护项目结构）
    
*   实战的时候就直接套这个目录结构了。
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220221/1645409475225044725.png)

第三部分：浏览器封装
----------

*   获取浏览器方法 get_driver()
    
*   通过参数控制浏览器的默认类型 (兼容性测试)
    
*   通过参数控制浏览器的模式，可以无头运行
    
*   通过参数控制隐式等待时间
    
*   优化: 单例实现 (参数化登录用例发现打开多个页面后改进)
    
*   示例代码
    
    ```
    from selenium import webdriver
    from configs.env import Env
    
    class Single(object):
        '''
        单例是一种设计模式
        1. 直接用
        2. 要去理解__new__ __init__ type
        3. 单例的应用场景: log、自动化测试的时候浏览器、配置器、播放器
        '''
        _instance = None #实例
        def  __new__(cls, *args, **kwargs):
            if cls._instance is None:     #此处是可以用__instance
                cls._instance = super().__new__(cls)
            return cls._instance
    
    class Single1(object):
        def  __new__(cls, *args, **kwargs):
            if not hasattr(cls,'_instance'):   #__instance  私有的
                cls._instance = super().__new__(cls)
            return cls._instance
    
    class Comm_Driver(Single):
        """
        功能:打开一个浏览器
        1. 测试多种浏览器
        2. 配置参数：浏览器类型，是否有头，隐式等待时间
        """
        driver = None
        def get_driver(self, browser_type=Env.BROWSER_TYPE,
                       headless_flag=Env.HEADLESS_FLAG):
            """
            :param browser_type: 浏览器类型，取配置文件中的值作为默认值
            :param headless_flag: 是否有头，取配置文件中的值作为默认值
            :return: 返回一个浏览器对象
            """
            if self.driver is None:  #如果没有打开过浏览器，就去打开...
                if not headless_flag:  #如果有头模式
                    if browser_type == 'chrome':
                        self.driver = webdriver.Chrome()
                    elif browser_type == 'firefox':
                        self.driver = webdriver.Firefox()
                    # TODO 其他的浏览器测试你也可以加在此处
                    else:
                        raise Exception(f'暂不支持当前的{browser_type}')
                else:
                    if browser_type == 'chrome':
                        _option = webdriver.ChromeOptions() #_option前面_是建议私有
                        _option.add_argument('--headless')  #添加一个--headless参数
                        self.driver = webdriver.Chrome(options=_option)
                    elif browser_type == 'firefox':
                        _option = webdriver.FirefoxOptions()
                        _option.add_argument('--headless')
                        self.driver = webdriver.Firefox(options=_option)
                    else:
                        raise Exception(f'暂不支持当前的{browser_type}')
                self.driver.maximize_window()  #最大化
                self.driver.implicitly_wait(Env.IMPLICITLY_WAIT_TIME)  #隐式等待
            return self.driver #如果打开过了，就直接返回
    
    ```