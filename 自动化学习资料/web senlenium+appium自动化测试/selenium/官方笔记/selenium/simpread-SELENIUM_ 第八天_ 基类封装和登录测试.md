> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b5879e220ea017a2c349bdb77d3)

> 功能

浏览器封装
-----

*   功能
    

*   打开浏览器
    
*   通过参数控制浏览器类型
    
*   通过参数控制有头模式还是无头模式
    
*   通过参数控制隐式等待时间
    

*   示例代码
    
    ```
    class CommDriver(Singe):
        """
        1. 通用的浏览器类
        """
        driver = None  #设置一个类属性
        def get_driver(self,
                       browname=def_browname,
                       headless=headless_flag,
                       implicitly_wait_time=def_impwait_time):
            """
            1. 获取浏览器对象
            :param browname: 浏览器名字，目前支持
            :param headless: 是否有头，True是无头，False是有头
            :param implicitly_wait_time: 隐式等待时间
            :return: 返回一个浏览器
            """
    
            if self.driver is None:  #如果获取过浏览器了，这个就有值了不会再做下面的事情
                if not headless:
                    if browname == 'chrome':
                        self.driver = webdriver.Chrome()
    
                    elif browname == 'firefox':
                        self.driver = webdriver.Firefox()
                    else:
                        raise Exception(f'{browname} is not supported')
                else:
                    if browname == 'chrome':
                        self.__option = webdriver.ChromeOptions()
                        self.__option.add_argument('-headless')
                        self.driver = webdriver.Chrome(options=self.__option)
                    elif browname == 'firefox':
                        self.__option = webdriver.FirefoxOptions()
                        self.__option.add_argument('-headless')
                        self.driver = webdriver.Firefox(options=self.__option)
                    else:
                        raise Exception(f'{browname} is not supported')
                self.driver.maximize_window()
                self.driver.implicitly_wait(implicitly_wait_time)
            return self.driver
    
    ```
    
*   单例优化
    

基类封装
----

*   功能
    

*   要注意，默认是追加输入，大部分是清空后输入所以要更改方法
    
*   打开浏览器
    
*   输入网址
    
*   定位元素
    
*   输入文本
    
*   点击元素
    
*   优化：等待后点击（触发：登录后要退出）
    
*   优化：等待后获取元素（触发：错误提示出现太快获取不到）
    

*   示例代码
    
    ```
    class BasePage():
        """
        基类
        1. 打开浏览器
        2. 输入网址
        3. 定位元素
        4. 输入文本
        5. 点击元素
        # 待补充
        6. 等待后点击
        7. 等待后获取
        """
        def __init__(self):
            self.driver = CommDriver().get_driver()
        def open_url(self,url):  #二次封装实现的方法，提供更多的可能
            """
            打开网址
            :param url: 待打开的网址
            :return: None
            """
            self.driver.get(url)
        def get_element(self,locator):
            """
            定位元素
            :param locator:   定位器 如 (By.ID,'username') ['id','username']
            :return: 返回元素
            """
            return  self.driver.find_element(*locator)
        def input_text_old(self, locator, text):
            """
            在元素上输入内容
            :param locator:  定位器
            :param text:  输入的内容
            :return:  None
            """
            warnings.warn('input_text_old is deprecated,please use input_text instead',DeprecationWarning)
            self.get_element(locator).send_keys(text)
        def input_text(self,locator,text,append=False):
            """
            清空原内容后再输入内容
            保留原内容追加输入内容
            :param locator: 定位器
            :param text:    输入文本
            :param append:    True是追加输入，Flase是清空后输入
            :return: None
            """
            if not append:
                self.get_element(locator).clear()
                self.get_element(locator).send_keys(text)
            else:
                self.get_element(locator).send_keys(text)
        def click_element(self,locator):
            """
            点击元素
            :param locator:  定位器
            :return:  None
            """
            self.get_element(locator).click()
        #增加一个方法，获取元素的文本
        def get_element_text(self,locator):
            """
            获取元素的文本
            :param locator: 定位器
            :return:
            """
            return self.get_element(locator).text
        def wait_click_element(self,locator):
            # TODO 你可以封装这个函数，设置超时时间和轮询时间
            WebDriverWait(self.driver,5,0.5).\
                until(EC.visibility_of_element_located(locator)).click()
        def wait_get_element_text(self,locator):
            return         WebDriverWait(self.driver,5,0.5).\
                until(EC.visibility_of_element_located(locator)).text
    
    
    ```
    

登录页封装
-----

*   功能
    

*   打开登录页
    
*   登录系统
    

*   示例代码
    
    ```
    class LoginPage(BasePage):
        """
        登录页面
        1. 打开登录页
        2. 登录系统
        """
        def open_loginpage(self):
            self.open_url(POLLY_URL)
        def login_polly(self,username,password):
            #要用到页面元素
            #元素的封装可以用py文件，也可以用其他类型的配置文件
            self.input_text(username_input,username)
            self.input_text(password_input,password)
            self.click_element(login_button)
    
    ```
    

测试用例
----

*   简单的成功登录测试用例
    
    ```
    def test_login_001():
        """
        最简单的登录测试
        :return:
        """
        test_loginpage = LoginPage()
        test_loginpage.open_loginpage()
        test_loginpage.login_polly(USERNAME1,PASSWORD1)
        #断言： 测试思想的体现
        #首页出现另一个首页元素
        #判断这个首页元素的文本是“首页”
        assert test_loginpage.get_element_text(['xpath','//*[text()="首页"]']) == '首页'
    
    
    ```
    
*   多组成功登录测试用例
    
    ```
    @pytest.mark.parametrize('username,password',
                             [('松勤老师','123456'),
                              ('PW179038','123456')])
    def test_login_success(username,password):
        """
        登录成功的测试: 正向用例
        :return:
        """
        test_loginpage = LoginPage()
        test_loginpage.open_loginpage()
        test_loginpage.login_polly(username,password)
        #断言： 测试思想的体现
        #首页出现一个首页元素
        #判断这个首页元素的文本是“首页”
        assert test_loginpage.get_element_text(['xpath','//*[text()="首页"]']) == '首页'
        #第一次登录后页面停住了，要退出后再登录
        #点击个人中心按钮
        test_loginpage.click_element(['xpath','//img'])
        #点击退出
        test_loginpage.wait_click_element(['xpath',"//span[text()='退出']"])
    
    ```
    
*   多组失败登录用例
    
    ```
    @pytest.mark.parametrize('username,password,locator,expected',
                             get_yaml_data(f'{test_datas_path}login_failed_data.yaml'))
    def test_login_failed(username,password,locator,expected):
        """
        登录失败的用例
        1. 用户名输入错误，密码正确 预期出现提示"用户名或密码错误"
        2. 密码输入错误           预期出现提示"密码不正确"
        3. 密码太短               预期出现提示"密码不能小于3位"
        输入数据不同，判断的依据不同（元素不同，内容不同）
        username,password,(元素定位器),预期结果
        YAML是非常契合这种数据的构造的
        :return:
        """
        test_loginpage = LoginPage()
        test_loginpage.open_loginpage()
        test_loginpage.login_polly(username,password)
        assert test_loginpage.wait_get_element_text(locator) == expected
    
    ```
    

yaml 基础
-------

*   yaml 数据的读取
    
    ```
    import yaml
    from pprint import pprint
    from utils.handle_path import test_datas_path
    
    def get_yaml_data(yaml_file):
        with open(yaml_file,encoding='utf-8') as f:
            return yaml.safe_load(f.read())
    
    if __name__ == '__main__':
        pprint(get_yaml_data(f'{test_datas_path}login_failed_data.yaml'))
    
    ```
    

pytest 基础
---------

*   参数
    

<table><thead><tr cid="n295" mdtype="table_row"><th>参数</th><th>说明</th><th>示例</th></tr></thead><tbody><tr cid="n299" mdtype="table_row"><td>--collect-only</td><td>显示哪些用例被选到</td><td><br></td></tr><tr cid="n303" mdtype="table_row"><td>-s</td><td>显示程序中的 print/logging 输出</td><td><br></td></tr><tr cid="n307" mdtype="table_row"><td>-v</td><td>丰富信息模式, 输出更详细的用例执行信息</td><td><br></td></tr><tr cid="n311" mdtype="table_row"><td>-k</td><td>keyword，运行包含某个字符串的测试用例。如：</td><td>pytest -k add XX.py 表示运行 XX.py 中包含 add 的测试用例。</td></tr><tr cid="n315" mdtype="table_row"><td>-x</td><td>出现一条测试用例失败就退出测试。在调试阶段非常有用，当测试用例失败时，应该先调试通过，而不是继续执行测试用例。</td><td><br></td></tr><tr cid="n319" mdtype="table_row"><td>-m</td><td>mark，运行带有标记的测试用例</td><td>pytest -m 'm1'</td></tr><tr cid="n323" mdtype="table_row"><td>-h</td><td>查看帮助</td><td><br></td></tr><tr cid="n327" mdtype="table_row"><td>-q</td><td>只显示整体的测试结果，相当于静默输出</td><td><br></td></tr><tr cid="n331" mdtype="table_row"><td>--markers</td><td>显示所有 pytest 支持的 markers</td><td>不是测试文件中的 mark</td></tr></tbody></table>

os 基础
-----

*   通过__file__获取当前文件的绝对路径
    
*   通过 os.path.dirname 获取目录路径
    
*   通过 os.path.join 来拼接路径
    
*   通过
    

```
import os
project_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
common_path = os.path.join(project_path,'common\\')
test_datas_path = os.path.join(project_path,'test_datas\\')
if __name__ == '__main__':
    print(test_datas_path)

```

plantuml 画类图
------------

*   在 pycharm 中下载插件 PlantUML integration
    
*   右键某个工程下的目录，新建 ->PlanUML FILE
    
    ```
    @startuml  #开始
    package common <<folder>>  #包，标记为目录形式
    {
        package comm_driver.py <<Frame>>  #包，标记位框架形式（类似于内联框架）
        {
        class CommDriver  #类的注释，和下面一行联合使用
        note left:浏览器类  #注释在左侧
        class CommDriver  #类 声明
            {
            +get_driver()  #方法
            }
        class Single
        note top:单例实现
        class Single
        {
        }
        }
    CommDriver --> Single :继承   #指向 语法类1 --> 类2 :注释 ;-->还有其他用法
    }
    @enduml  #结束
    
    ```