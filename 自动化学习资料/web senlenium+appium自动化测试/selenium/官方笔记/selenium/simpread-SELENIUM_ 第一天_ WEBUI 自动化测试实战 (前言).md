> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b587eddaee2017f14d286be3a4c)

> 适合用户

*   适合用户
    

*   你需要有一定的 python 基础
    
*   你要有一定的 web 基础
    
*   你要有一定的软件测试基础
    

*   **软件环境**
    
    <table><thead><tr cid="n12" mdtype="table_row"><th>软件</th><th>库 / 插件</th><th>Version</th><th>说明</th></tr></thead><tbody><tr cid="n17" mdtype="table_row"><td>python</td><td><br></td><td>3.9.6</td><td>部分库不支持 3.10</td></tr><tr cid="n22" mdtype="table_row"><td>pycharm</td><td><br></td><td>2021.2.3 社区版</td><td><br></td></tr><tr cid="n27" mdtype="table_row"><td><br></td><td>chinese(simplified) language pack / 中文语言包</td><td><br></td><td><br></td></tr><tr cid="n32" mdtype="table_row"><td><br></td><td>one dark theme</td><td><br></td><td><br></td></tr><tr cid="n37" mdtype="table_row"><td><br></td><td>plantuml integration</td><td><br></td><td><br></td></tr><tr cid="n42" mdtype="table_row"><td>JDK</td><td><br></td><td>1.8</td><td>SELENIUM GRID 需要</td></tr><tr cid="n47" mdtype="table_row"><td>selenium</td><td><br></td><td>4.1.0</td><td><br></td></tr><tr cid="n52" mdtype="table_row"><td><br></td><td>ddddocr</td><td>1.4.1</td><td>不支持 3.10 及以上版本</td></tr><tr cid="n57" mdtype="table_row"><td><br></td><td>pyyaml</td><td>6.0</td><td><br></td></tr><tr cid="n62" mdtype="table_row"><td><br></td><td>pathlib</td><td>1.0.1</td><td><br></td></tr><tr cid="n67" mdtype="table_row"><td><br></td><td>pytest</td><td>6.2.5</td><td><br></td></tr><tr cid="n72" mdtype="table_row"><td><br></td><td>allure-pytest</td><td>2.9.45</td><td><br></td></tr><tr cid="n77" mdtype="table_row"><td><br></td><td>loguru</td><td>0.6.0</td><td><br></td></tr><tr cid="n82" mdtype="table_row"><td><br></td><td>pyautogui</td><td>0.9.53</td><td><br></td></tr><tr cid="n87" mdtype="table_row"><td><br></td><td>pywinauto</td><td>0.6.8</td><td><br></td></tr><tr cid="n92" mdtype="table_row"><td><br></td><td>Pywin32</td><td>303</td><td><br></td></tr><tr cid="n97" mdtype="table_row"><td><br></td><td>pytest-base-url</td><td>1.4.2</td><td><br></td></tr><tr cid="n102" mdtype="table_row"><td><br></td><td>pytest-assume</td><td>2.4.3</td><td><br></td></tr><tr cid="n107" mdtype="table_row"><td><br></td><td>pytest-dependency</td><td>0.5.1</td><td>实战课</td></tr><tr cid="n112" mdtype="table_row"><td><br></td><td>pytest-ordering</td><td>0.6</td><td>实战课</td></tr><tr cid="n117" mdtype="table_row"><td><br></td><td>pytest-xdist</td><td>2.5.0</td><td><br></td></tr><tr cid="n122" mdtype="table_row"><td><br></td><td>pytest-repeat</td><td>0.9.1</td><td>实战课</td></tr><tr cid="n127" mdtype="table_row"><td><br></td><td>pytest-rerunfailures</td><td>10.2</td><td>实战课</td></tr></tbody></table>
*   **测试环境网址**
    
    <table><thead><tr cid="n135" mdtype="table_row"><th>网站</th><th>URL</th><th>说明</th></tr></thead><tbody><tr cid="n139" mdtype="table_row"><td>控件测试</td><td><a href="http://sahitest.com/demo/">http://sahitest.com/demo/</a></td><td>国外的一个网址用于部门 SELENIUM 方法测试</td></tr><tr cid="n143" mdtype="table_row"><td>本地论坛</td><td><a href="http://127.0.0.1/upload/forum.php">http://106.14.1.150:8090/forum.php</a></td><td>1-5 天会使用，开源论坛，需要自己部署在本地或虚拟机上（虚拟机的话需要改变 ip）</td></tr><tr cid="n151" mdtype="table_row"><td>UI 定位</td><td><a href="http://121.41.14.39:8088/index.html#/">http://121.41.14.39:8088/index.html#/</a></td><td>1-5 天会使用</td></tr><tr cid="n155" mdtype="table_row"><td><strong>宝利商城</strong></td><td><a href="http://47.99.220.37:38090/#/home">http://124.223.33.41:38090/#/login</a></td><td>项目开始使用该环境</td></tr></tbody></table>
*   第三方网址
    
    <table><thead><tr cid="n162" mdtype="table_row"><th>网站</th><th>URL</th><th>说明</th></tr></thead><tbody><tr cid="n166" mdtype="table_row"><td>selenium 官网</td><td><a href="https://www.selenium.dev/">https://www.selenium.dev/</a></td><td><br></td></tr><tr cid="n170" mdtype="table_row"><td>驱动淘宝镜像</td><td><a href="http://npm.taobao.org/mirrors">http://npm.taobao.org/mirrors</a></td><td><br></td></tr><tr cid="n174" mdtype="table_row"><td>SELENIUM IDE 命令说明</td><td><a href="https://www.seleniumhq.org/selenium-ide/docs/en/api/commands/">https://www.seleniumhq.org/selenium-ide/docs/en/api/commands/</a></td><td><br></td></tr><tr cid="n178" mdtype="table_row"><td>webdriver</td><td><a href="https://w3c.github.io/webdriver/#get-title">https://w3c.github.io/webdriver/#get-title</a></td><td><br></td></tr><tr cid="n182" mdtype="table_row"><td>Chrome DevTools Protocol</td><td><a href="https://chromedevtools.github.io/devtools-protocol/">https://chromedevtools.github.io/devtools-protocol/</a></td><td><br></td></tr><tr cid="n186" mdtype="table_row"><td>selenium changelog</td><td><a href="https://github.com/SeleniumHQ/selenium/blob/trunk/py/CHANGES">https://github.com/SeleniumHQ/selenium/blob/trunk/py/CHANGES</a></td><td><br></td></tr><tr cid="n190" mdtype="table_row"><td>GITHUB 下载地址 (GRID 下载)</td><td><a href="https://github.com/SeleniumHQ/selenium/releases/">https://github.com/SeleniumHQ/selenium/releases/</a></td><td><br></td></tr><tr cid="n194" mdtype="table_row"><td>MDN DOM API</td><td><a href="https://developer.mozilla.org/zh-CN/docs/Web/API/Document_Object_Model">https://developer.mozilla.org/zh-CN/docs/Web/API/Document_Object_Model</a></td><td><br></td></tr></tbody></table>  
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220220/1645321058986014640.png)

第一部分：自动化测试理论
------------

*   自动化测试本质还是测试，**好的测试设计**非常重要（经常能看到开发做不好测试这样的话）
    
*   自动化测试会有编码，编码就会有 bug，你的代码的健壮性如何，你的语言功底如何？
    
*   代码庞大了之后，总会考虑性能，软件架构，你是否有足够的算法基础，设计思维。
    
*   WEBUI 测试偏向于功能测试，与前端联系更加紧密，你要有一定的前端基础。
    
*   什么样的项目适合自动化测试
    

*   软件需求变动不频繁
    
*   项目周期较长
    
*   自动化测试脚本可以重复使用
    
*   敏捷模式下每日构建后的测试验证
    
*   比较频繁的回归测试
    
*   软件系统界面稳定、变动较少
    
*   **测试对象可识别、易识别**
    

*   分层的金字塔模型
    

*   敏捷大师 Mike Cohn 在 Succeeding with Agile 一书中提出测试金字塔的概念。
    
*   我们应该有更多低级别的单元测试，而不仅仅是用户界面运行的高层的端到端的测试。
    
*   分层自动化测试倡导的是从黑盒 UI 单层到黑白盒多层的自动化测试体系，从全面黑盒的自动化测试到对系统的不同阶段、不同层次进行自动化测试。
    

*   [小型测试] 是指单元测试，用于验证应用的行为，一次验证一个类。
    
*   [中型测试] 是指集成测试，用于验证模块内堆栈级别之间的互动或相关模块之间的互动。
    
*   [大型测试] 是指端到端测试，用于验证跨越了应用的多个模块的用户操作流程
    
*   沿着金字塔逐级向上，从小型测试到大型测试，各类测试的保真度逐级提高，但维护和调试工作所需的执行时间和工作量也逐级增加。因此，您编写的单元测试应多于集成测试，集成测试应多于端到端测试。虽然各类测试的比例可能会因应用的用例不同而异，但通常建议各类测试所占比例如下：**小型测试占 70%，中型测试占 20%，大型测试占 10%**
    
![](http://vip.ytesting.com//upload/ueditor/upload/image/20220220/1645321161670049037.png)

第二部分：环境说明
---------

### UI 定位平台

<table><thead><tr cid="n256" mdtype="table_row"><th>网址</th><th><a href="http://121.41.14.39:8088/index.htm">http://121.41.14.39:8088/index.htm</a></th></tr></thead><tbody><tr cid="n259" mdtype="table_row"><td>账号</td><td>sq1~sq100</td></tr><tr cid="n262" mdtype="table_row"><td>密码</td><td>123</td></tr><tr cid="n265" mdtype="table_row"><td>登录万能验证码</td><td>999999</td></tr><tr cid="n268" mdtype="table_row"><td>说明</td><td>注意!!! 该网站后续会调整（关闭或账号回收）目前只提供给当前期学员, 请及时练习</td></tr></tbody></table>

### 开源论坛

<table><thead><tr cid="n274" mdtype="table_row"><th>网址</th><th><a href="http://127.0.0.1/upload/forum.php">http://127.0.0.1/upload/forum.php</a></th></tr></thead><tbody><tr cid="n277" mdtype="table_row"><td>账号</td><td>admin</td></tr><tr cid="n280" mdtype="table_row"><td>密码</td><td>123456（安装时自行设定，文档推荐用 123456）</td></tr><tr cid="n283" mdtype="table_row"><td>说明</td><td>需要自己部署在本地或虚拟机上（虚拟机的话需要改变 ip）</td></tr></tbody></table>

*   安装包在网盘的课前准备中
    
*   请参考文档《wamp 安装指南. pdf》
    
*   在线论坛不推荐使用
    

### 自己写的网页

*   第一天写的 test.html 及据此剥离的 min.html
    

### 宝利商城

<table><thead><tr cid="n299" mdtype="table_row"><th>网址</th><th><a href="http://47.99.220.37:38090/#/login">http://124.223.33.41:38090/#/login</a></th></tr></thead><tbody><tr cid="n302" mdtype="table_row"><td>账号</td><td><br></td></tr><tr cid="n305" mdtype="table_row"><td>密码</td><td>与班主任申请</td></tr><tr cid="n308" mdtype="table_row"><td>说明</td><td>PO 实现用例的环境</td></tr></tbody></table>

### 第三方网站

*   [http://sahitest.com/demo/](http://sahitest.com/demo/)   控件练习
    

第三部分：WEB 基础
-----------

### HTML 基础

**HTML 是什么**

*   HTML 指的是超文本标记语言 (**H**yper **T**ext **M**arkup **L**anguage)
    
*   HTML 不是一种编程语言，而是一种**标记语言** (markup language)
    
*   标记语言是一套标记标签 (markup tag)
    
*   HTML 使用**标记标签**来描述网页
    

**HTML 标签 (tag) 是什么**

*   HTML 标签是由尖括号包围的关键词，比如 <html>
    
*   HTML 标签通常是成对出现的，比如 <b> 和 </b>
    
*   标签对中的第一个标签是开始标签，第二个标签是**结束标签**
    
*   开始和结束标签也被称为开放标签和**闭合标签**
    

**HTML 元素是什么**

*   HTML 元素以**开始标签**起始
    
*   HTML 元素以**结束标签**终止
    
*   元素的内容是开始标签与结束标签之间的内容
    
*   某些 HTML 元素具有 *** 空内容**（empty content）*
    
*   空元素在开始标签中进行关闭（以开始标签的结束而结束）
    
*   大多数 HTML 元素可拥有**属性**
    

**常见的 HTML 标签即属性**

<table><thead><tr cid="n356" mdtype="table_row"><th>标签</th><th>标签含义</th><th>属性</th><th>属性含义</th></tr></thead><tbody><tr cid="n361" mdtype="table_row"><td>html</td><td>定义 HTML 文档</td><td><br></td><td><br></td></tr><tr cid="n366" mdtype="table_row"><td>head</td><td>定义关于文档的信息</td><td><br></td><td><br></td></tr><tr cid="n371" mdtype="table_row"><td>title</td><td>定义文档的标题</td><td><br></td><td><br></td></tr><tr cid="n376" mdtype="table_row"><td>body</td><td>定义文档的主体</td><td><br></td><td><br></td></tr><tr cid="n381" mdtype="table_row"><td>div</td><td>定义文档中的节</td><td>id</td><td>规定元素的唯一 id。</td></tr><tr cid="n386" mdtype="table_row"><td><br></td><td><br></td><td>class</td><td>规定元素的一个或多个类名（引用样式表中的类）。</td></tr><tr cid="n391" mdtype="table_row"><td>input</td><td>定义输入控件</td><td>type</td><td>规定按钮的类型</td></tr><tr cid="n396" mdtype="table_row"><td>a</td><td>锚，anchor，超链接</td><td>href</td><td>链接的目标</td></tr><tr cid="n401" mdtype="table_row"><td><br></td><td><br></td><td>taget</td><td>规定在何处打开链接文档，_blank 就是新开一个网页</td></tr><tr cid="n406" mdtype="table_row"><td>p</td><td>定义段落</td><td><br></td><td><br></td></tr><tr cid="n411" mdtype="table_row"><td>button</td><td>定义按钮</td><td><br></td><td><br></td></tr><tr cid="n416" mdtype="table_row"><td>span</td><td>定义文档中的节</td><td><br></td><td><br></td></tr><tr cid="n421" mdtype="table_row"><td>ul</td><td>定义无序列表</td><td><br></td><td><br></td></tr><tr cid="n426" mdtype="table_row"><td>ol</td><td>定义有序列表</td><td><br></td><td><br></td></tr><tr cid="n431" mdtype="table_row"><td>li</td><td>定义列表的项目</td><td><br></td><td><br></td></tr><tr cid="n436" mdtype="table_row"><td>select</td><td>定义选择列表（下拉列表）</td><td><br></td><td><br></td></tr><tr cid="n441" mdtype="table_row"><td>option</td><td>定义选择列表中的选项</td><td><br></td><td><br></td></tr><tr cid="n446" mdtype="table_row"><td>iframe</td><td>定义内联框架</td><td>src</td><td>规定在 iframe 中显示的文档的 URL</td></tr><tr cid="n451" mdtype="table_row"><td>script</td><td>定义客户端脚本</td><td>src</td><td><br></td></tr><tr cid="n456" mdtype="table_row"><td>img</td><td>定义图像</td><td><br></td><td><br></td></tr><tr cid="n461" mdtype="table_row"><td>&lt;!--...--&gt;</td><td>注释</td><td><br></td><td><br></td></tr><tr cid="n466" mdtype="table_row"><td></td><td>文档的第一行，声明</td><td><br></td><td><br></td></tr><tr cid="n471" mdtype="table_row"><td>h1~h6</td><td>定义 HTML 几级 标题</td><td><br></td><td><br></td></tr><tr cid="n476" mdtype="table_row"><td>br</td><td>换行</td><td><br></td><td><br></td></tr></tbody></table>

### CSS 基础

*   Cascading Style Sheets  层叠样式表
    
*   是一门基于规则的语言 —— 你能定义用于你的网页中特定元素样式的一组规则
    
*   示例代码：**我希望页面中的主标题是红色的大字**
    
    ```
    h1 {
        color: red;
        font-size: 5em;
    }
    
    ```
    
*   格式
    
    ```
    css选择器{
        属性1: 属性1的值;
        属性2: 属性2的值;
    }
    
    ```
    
*   你需要知道的是 h1 是选择器，在 selenium 中选择器是一种定位器，后面要重点学
    

*   示例 html
    
    ```
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>css demotitle>
        <style>
            #p2 {
                color: blue;
                font-size: 6em;
            }
        style>
        <link href="./cssdemo.css" rel="stylesheet">
         
    head>
    <body>
      <p>P1p>
      <p>P2p>
      <p>P3p>
    body>
    html>
    
    ```
    
    ```
    #p3 {
        color: black;
        font-size: 7em;
    }
    
    ```
    

*   行内样式（内联样式）
    
    ```
     内容
    
    ```
    
*   内部样式表
    
    ```
    >
    >
    
    ```
    
*   外部样式表
    
    ```
    >
    
    ```
    
*   上面的 HTML 演示了 3 种 CSS 样式
    

### JS 基础

*   Javascript 和 Java 是完全不同的语言，不论是概念还是设计
    
*   ECMAScript 标准，ECMAScript6 是 2015 年发布的
    
*   API
    

*   浏览器 API：**DOM API** 和地理位置 API、画布等
    
*   第三方 API：新浪 API、TwitterAPI 等
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220220/1645321273335077936.png)

*   [课程主要涉及一些 DOM API](https://developer.mozilla.org/zh-CN/docs/Web/API/Document_Object_Model)
    

### URI 结构

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220220/1645321378583032651.png)

**Scheme**：URI 的起始点，并与 URI 协议相关。URI schema 对大小写不敏感，后面带有一个 “:”。尽管在实践中可以使用未注册的 schema，但 URI schema 应该向 Internet Assigned Numbers Authority (IANA) 注册。几个流行的 URI schema 的例子：HTTP、HTTPS、FTP 和 mailto。

**Authority（权限）**：Authority 字段是位于 schema 字段之后的第二个条目，以两个斜杠 (//) 开头。这个字段由多个子字段组成：

*   **authentication**（认证信息）- 可选的字段和用户名，密码，由冒号隔开，后面跟着 “@” 符号
    
*   **host**（主机名）—注册的名称或 IP 地址，出现在 “@” 符号之后
    
*   **port**（端口号）- 可选字段，后面跟着一个冒号
    

**Path（路径）**：Path 是第三个字段，由斜杠分隔的段序列来表示，用来提供资源的位置。注意，不管 authority 部分存在或不存在，path 都应该以一个斜杠开始，而不是双斜杠 (//)。

**Query（查询）**：Query 是第四个字段，是 URI 的可选字段，包含一串非结构数据并以 “?” 和 Path 隔开。

**Fragment（片段）**：Fragment 是第五个组成部分，也是一个可选字段，提供指向辅助资源的方向，并以 “#” 开始。简单来说，Fragment 字段可以用于指向 HTML 页面的特定元素(主资源)。

### 浏览器及开发者工具

#### 快捷键

<table><thead><tr cid="n554" mdtype="table_row"><th>快捷键</th><th>作用</th></tr></thead><tbody><tr cid="n557" mdtype="table_row"><td>CTRL+SHIFT+I</td><td>打开开发者工具</td></tr><tr cid="n560" mdtype="table_row"><td>F12</td><td>打开开发者工具</td></tr><tr cid="n563" mdtype="table_row"><td>右键 -&gt; 检查</td><td>打开 Elements 面板并直接定位到对应元素</td></tr><tr cid="n566" mdtype="table_row"><td>CTRL+SHIFT+C</td><td>点击箭头开始检测元素</td></tr><tr cid="n569" mdtype="table_row"><td>CTRL+SHIFT+P</td><td>打开命令输入框</td></tr><tr cid="n572" mdtype="table_row"><td>SHIFT+?</td><td>打开设置界面</td></tr><tr cid="n575" mdtype="table_row"><td>ALT+R</td><td>重载 devtool</td></tr><tr cid="n578" mdtype="table_row"><td>CTRL+`</td><td>打开控制台</td></tr><tr cid="n581" mdtype="table_row"><td><strong>CTRL+F5</strong></td><td>去缓存刷新</td></tr></tbody></table>

*   在自动化测试打开的网页中用右键 -> 检查会失效
    

#### 地址栏

*   chrome://version/
    
*   chrome://about/
    
*   chrome://inspect   #appium 会用到
    

#### 操作技巧 (以 chrome 为例)

**★★★技巧一：元素定位**

1.  打开开发者工具
    
2.  切换到 element(元素面板)
    
3.  点击左上角的箭头使其变成蓝色
    
4.  在网页上点击你要定位的元素
    
5.  在元素面板中会自动跳转到该元素所在的 HTML 代码处
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220220/1645321430056063241.png)

**★★★★技巧二：用语法定位元素**

1.  打开开发者工具
    
2.  在 elements(元素)面板任意选择一个区域 (或元素) 点击下
    
3.  按 CTRL+F，弹出搜索框
    
4.  在搜索框中输入 XPATH 表达式 / CSS 表达式 / 字符串
    
5.  凡是符合该表达式的元素会被检索到（泛黄显示），右侧会显示 1 of  N，如果是 1of1 就是唯一定位到了，如果是 1of2 表示 2 个被定位到了，按回车会自动定位到第二个匹配的元素上，依次类推，如果在 selenium 中用 find_element 定位方法，那操作的就是第一个元素。一般情况下我们要缩小范围使得能唯一定位到，特殊场景下可以定位到多个（如爬虫），具体情况具体分析。
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220220/1645321511818060462.png)

**技巧三：元素操作**

1.  双击 HTML 元素的属性值可以复制其值（规避一些看不清的内容，如 l 和 1，如 0oO 等）
    
2.  右键元素，选择 copy->copy selector|copy xpath|copy full xpath 都可以复制该元素的表达式，但要注意有时候会失效，有时候不太可用
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220220/1645321531813053106.png)

**其他技巧**

1.  firefox 对 css 支持的尤其好，提供了自动补齐功能
    
    ![](http://vip.ytesting.com//upload/ueditor/upload/image/20220220/1645321557371029856.png)
    
2.  在 console 中执行 setTimeout(function(){debugger},1000) 可以在 1s 后冻结窗口，有的时候元素无法定位到就可以通过它来定位
    
    ```
    #也可以是
    setTimeout(() => {debugger;}, 4000);
    
    ```
    
3.  开发者工具中按 shift+？打开 settings 界面，debugger 处有个 disable js 的功能，有的网页无法复制是通过 js 实现就可以去掉。
    
4.  在开发者工具界面按 ctrl+shift +p，可以输入 cdp 相关命令，如全屏截图 capture full size screenshot
    
5.  常见的辅助定位的插件：selectorhub、xpath finder、xpath helper 等
    
6.  选取界面元素的时候，浏览器提供了元素的默认定位语法就是 css 的
    
    ![](http://vip.ytesting.com//upload/ueditor/upload/image/20220220/1645321582515099130.png)
    

第四部分：SELENIUM 简介            
----------------------------

*   简介
    

*   2021.11.22 RELEASE [4.1.0](https://pypi.org/project/selenium/)
    
*   核心理念: Selenium automates browsers
    
*   Primarily it is for automating web applications for testing purposes, but is certainly not limited to just that.
    
*   Boring web-based administration tasks can (and should) also be automated as well.
    

*   特点
    

*   开源、免费
    
*   多浏览器支持：Firefox Chrome IE Opera Edge Safari
    
*   多平台支持：Linux Windows MAC
    
*   多语言支持：Java Python Ruby C# JavaScript C++
    
*   WEBUI 自动化测试事实上的标准
    
*   简单（API）、灵活（开发语言驱动）
    
*   支持分布式执行测试用例
    

### IDE

*   火狐的插件中已经无法找到 SELENIUM IDE（弃用了），你可以用 katalon ide 替代，而 chrome 的插件中尚支持 SELENIUM IDE，但你需要翻墙方可访问，或者使用老师提供的压缩包。
    

*   **第一步：创建项目**
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220220/1645321620582096058.png)

*   **第二步：项目名**
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220220/1645321646921094660.png)

*   **第三步：输入被测网址**
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220220/1645321658681083269.png)

*   **第四步：录制（操作网页）**
    
*   **第五步：根据需要裁剪裁剪**
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220220/1645321670804022306.png)

*   **第六步：回放**
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220220/1645321685794047971.png)

> 回访的时候注意有的时候网页会有记录，要保证你的步骤是闭环的（比如登录后退出），但纯写代码则不会发生这样的问题。
> 
> 有的时候回访会卡住，常见 open 方法在 set window size 之前就多发，可以调换 2 个步骤的位置

*   **第七步：导出为脚本**
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220220/1645321703291063648.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220220/1645321738294006203.png)

*   注意: selenium ide 的命令比较多，详见[官方](https://www.seleniumhq.org/selenium-ide/docs/en/api/commands/)
    

### 入门代码详解

**三行入门代码**

```
from selenium import webdriver driver = webdriver.Chrome() driver.get('https://www.baidu.com')

```

**更换导入方式**

```
from selenium.webdriver.chrome.webdriver import WebDriver driver = WebDriver()

```

**打开本地 HTML**

**一、利用 os 模块打开**

```
import os
from selenium import webdriver
driver = webdriver.Chrome()
html = os.path.abspath('./min.html')
driver.get(html)

```

**二、利用 pathlib 库打开**

```
from selenium import webdriver
from pathlib import Path
driver = webdriver.Chrome()
html = str(Path('min.html').resolve())
driver.get(html)

```

**三、利用绝对路径打开**

```
from selenium import webdriver
driver = webdriver.Chrome()
driver.get(r'min.html的绝对路径')   #在pycharm中右键复制路径/引用，选择绝对路径即可

```

*   注意苹果系统打开的时候需要在前面加 file:///(windows 也支持)
    

**优雅的打开网页**

```
from selenium import webdriver
with webdriver.Chrome() as driver:
    driver.get('https://www.baidu.com')

```

**认识 Selenium 原理**

```
from selenium import webdriver
from time import sleep
with webdriver.Chrome(service_args=['--verbose'],
                      service_log_path='seleniun.log') as driver:
    driver.get('https://www.baidu.com')
    driver.find_element('id','kw').send_keys('松勤')
    sleep(5)

```

*   原理
    
    ```
    1. 对于每一个Selenium脚本，一个http请求会被创建并且发送给浏览器的驱动(CHROMEDRIVER.EXE)
    2. 浏览器驱动中包含了一个HTTP Server，用来接收这些http请求 
    3. HTTP Server接收到请求后根据请求来具体操控对应的浏览器
    4. 浏览器执行具体的测试步骤
    5. 浏览器将步骤执行结果返回给HTTP Server
    6. HTTP Server又将结果返回给Selenium的脚本，如果是错误的http代码我们就会在控制台看到对应的报错信息
    
    HTTP协议是一个浏览器和Web服务器之间通信的标准协议，而几乎每一种编程语言都提供了丰富的http libraries，这样就可以方便的处理客户端Client和服务器Server之间的请求request及响应response，WebDriver的结构中就是典型的C/S结构，WebDriver API相当于是客户端，而小小的浏览器驱动才是服务器端。
    
    那为什么同一个浏览器驱动即可以处理Java语言的脚本，也可以处理Python语言的脚本呢？
    这就要提到WebDriver基于的协议：JSON Wire protocol。Client和Server之间，只要是基于JSON Wire Protocol来传递数据，就与具体的脚本语言无关了，这样同一个浏览器的驱动就即可以处理Java语言的脚本，也可以处理Python语言的脚本了。
    
    ```
    
*   代码 -> 驱动 -> 浏览器的关系好比乘客 -> 司机 -> 出租车
    
*   通信方式
    

*   本机：WebDriver 通过驱动程序向浏览器传递命令， 然后通过相同的路径接收信息
    
    ![](http://vip.ytesting.com//upload/ueditor/upload/image/20220220/1645321794318027932.png)
    
*   远程：与浏览器的通信也可以是通过 Selenium 服务器或 RemoteWebDriver 进行的 远程通信。RemoteWebDriver 与驱动程序和浏览器运行在同一个系统上。
    
    ![](http://vip.ytesting.com//upload/ueditor/upload/image/20220220/1645321804046070720.png)
    
*   grid：使用 Selenium Server 或 Selenium Grid 进行，这两者依次与主机系统上的驱动程序进行通信
    
    ![](http://vip.ytesting.com//upload/ueditor/upload/image/20220220/1645321811178058587.png)
    

### 示例 HTML

```
<html lang="en">
 
<head>
 
    <meta charset="UTF-8">
    <title>我的第一个HTMLtitle>
 
 <style type="text/css">
  li{ 
   color: red;
   // 表示所有的li标签都是这个颜色
  }
 style>
 <link href="test1.css" rel="stylesheet"/>
head>
<body>
 
 <h1>一级标题h1>
 <div>
  
  用户名: <input type="text"  />
  
  <br>
  密码: <input type="text"  />
  <br>
  验证码:
  <br>
  <button type="button">
   <span>登录span>
   
   
  button>
 div>
 <div>
  <p>这是一个段落p>
  
  <a href="https://www.baidu.com">百度一下,你就不知道a>
  
  <a href="https://cn.bing.com" target="_blank">有求必应a>
  
 div>
 <div>
  <input type="button"  id="alert" value='alert' onclick="alert('helloalert')" />
  <input type="button"  id="confirm" value='confirm' onclick="confirm('helloconfirm')" />
  <input type="button"  id="prompt" value='prompt' onclick="prompt('helloprompt')" />
  
 div>
<div>
  <ul>
   <li>1li>
   <li>2li>
   <li>3li>
   
  ul>
  <ol>
   <li>呵呵li>
   <li>哈哈li>
   <li>嘿嘿li>
   
  ol>
 div>
 <div>
  <select multiple="multiple">
 
   <option value="1">10K-13Koption>
   <option value="2">13K-16Koption>
   <option value="3">16K-19Koption>
   <option value="3">19K-25Koption>
   
  select>
 div> 
 <iframe id='if' >iframe>
 
body>
html>

```