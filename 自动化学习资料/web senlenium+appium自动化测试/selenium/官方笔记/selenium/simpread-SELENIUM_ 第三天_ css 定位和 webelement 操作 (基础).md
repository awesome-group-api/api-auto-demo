> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b587eddaee2017f19e0fc0d3ddc)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220221/1645406552288023765.png)

第一部分：css 定位
-----------

*   官方：[https://www.w3school.com.cn/cssref/css_selectors.asp](https://www.w3school.com.cn/cssref/css_selectors.asp)
    
*   css 简介：
    

*   CSS 指的是层叠样式表 (Cascading Style Sheets)
    
*   CSS 描述了如何在屏幕、纸张或其他媒体上显示 HTML 元素
    
*   CSS 节省了大量工作。它可以同时控制多张网页的布局
    

*   [CSS 选择器的分类](https://developer.mozilla.org/zh-CN/docs/Learn/CSS/Building_blocks/Selectors#%E7%B1%BB%E5%9E%8B%E3%80%81%E7%B1%BB%E5%92%8Cid%E9%80%89%E6%8B%A9%E5%99%A8)
    

<table><thead><tr cid="n1529" mdtype="table_row"><th>名称</th><th>表达式</th><th>示例</th><th>描述</th></tr></thead><tbody><tr cid="n1534" mdtype="table_row"><td>类型选择器 (标签)</td><td>tag_name</td><td>p</td><td>所有 p 标签</td></tr><tr cid="n1539" mdtype="table_row"><td>id 选择器</td><td>#id</td><td>#username</td><td>id 的值为 username</td></tr><tr cid="n1544" mdtype="table_row"><td>类选择器</td><td>.class1.class2</td><td>.pn.vm</td><td>class1 的值是 pn，class2 的值是 vm</td></tr><tr cid="n1549" mdtype="table_row"><td><br></td><td>tagname#id.class</td><td>input#username.pn.vm</td><td>class1 的值是 pn，class2 的值是 vm，且 id 的值是 username 的 input 标签</td></tr><tr cid="n1554" mdtype="table_row"><td>路径选择器：子代</td><td>nod1&gt;nod2</td><td>html&gt;body&gt;div</td><td>html 下 body 下的 div 标签（子代元素）</td></tr><tr cid="n1559" mdtype="table_row"><td>路径选择器：后代</td><td>nod1 &nbsp; &nbsp; node2</td><td>html &nbsp;input</td><td>html 下的任意 input 标签（后代元素）</td></tr><tr cid="n1564" mdtype="table_row"><td>路径选择器：相邻弟弟 (二弟)</td><td>node1+node2</td><td>html&gt;head+body</td><td>html 下跟 head 紧挨着的 body</td></tr><tr cid="n1569" mdtype="table_row"><td>路径选择器：弟弟</td><td>node1~node2</td><td>div~p</td><td>div 与 p 同级，p 在 div 后面不一定紧挨着</td></tr><tr cid="n1574" mdtype="table_row"><td>属性选择器</td><td>[属性名 ='属性值']</td><td>input[id='username']</td><td>id 属性值为 username 的 input 标签 (id 唯一则 input 可以不写)</td></tr><tr cid="n1579" mdtype="table_row"><td><br></td><td><br></td><td>input[class='el-input__inner fm-text itxt-error dlname']</td><td>clas 的属性必须全部填入不能是部分</td></tr><tr cid="n1584" mdtype="table_row"><td><br></td><td>[属性名 ^='属性值']</td><td>input[name^='username']</td><td>开头是</td></tr><tr cid="n1589" mdtype="table_row"><td><br></td><td>[属性名 $='属性值']</td><td>input[name$='ername']</td><td>结尾是</td></tr><tr cid="n1594" mdtype="table_row"><td><br></td><td>[属性名 *='属性值']</td><td>input[name*='erna']</td><td>包含</td></tr><tr cid="n1599" mdtype="table_row"><td>伪类选择器：第几个元素</td><td>tagname:nth-child(n)</td><td>body&gt;div:nth-child(3)</td><td>body 下的第三个标签是 div（不是第三个 div）</td></tr><tr cid="n1604" mdtype="table_row"><td>伪类选择器：第一个子元素</td><td>:first-child</td><td>body&gt;:first-child</td><td>body 下的第一个标签</td></tr><tr cid="n1609" mdtype="table_row"><td>伪类选择器：最后一个</td><td>:last-child</td><td>body&gt;:last-child</td><td>body 下的最后一个标签</td></tr><tr cid="n1614" mdtype="table_row"><td>伪类选择器：倒数第几个元素</td><td>:nth-last-child</td><td>body&gt;div:nth-last-child(4)</td><td>body 下的倒数第四个标签是 div（不是第 4 个 div），也不区分大小写</td></tr><tr cid="n1619" mdtype="table_row"><td>伪类选择器：唯一元素</td><td>:only-child</td><td>input:only-child</td><td>每个节点下的唯一一个 input 标签（父节点只有一个标签）</td></tr></tbody></table>

**注意事项：**

*   css 号称速度最快，推荐使用
    
*   css 是 firefox 和 chrome 默认显示的语法
    
*   firefox 对 css 的支持最好
    
*   css 相较 xpath 无法实现 text 的定位
    
*   **数字开头的 id 的值用 css 的 id 定位会遇到问题**
    
*   css 并不支持多属性用逻辑运算符来组合定位，如 [name='username   and  id='username'] 是无效的，但他可以这么写
    
    ```
    [id='username'][name='username']
    
    ```
    

* * *

**Xpath 和 CSS 语法的对比**

<table><thead><tr cid="n1643" mdtype="table_row"><th>示例</th><th>css</th><th>xpath</th><th>说明</th></tr></thead><tbody><tr cid="n1648" mdtype="table_row"><td>id 的值</td><td>#id_value</td><td>//*[@id='id_value']</td><td><br></td></tr><tr cid="n1653" mdtype="table_row"><td>class 的值</td><td>.class1.class2</td><td>//*[@class='all_class_value']</td><td>xpath 用属性法要全部写</td></tr><tr cid="n1658" mdtype="table_row"><td><br></td><td>[class='all_class_value']</td><td><br></td><td>css 如果用属性法也要全部写</td></tr><tr cid="n1663" mdtype="table_row"><td>标签</td><td>tag_name</td><td>//tag_name</td><td><br></td></tr><tr cid="n1668" mdtype="table_row"><td>绝对路径</td><td>html&gt;body</td><td>/html/body</td><td><br></td></tr><tr cid="n1673" mdtype="table_row"><td>相对路径</td><td>html &nbsp;div</td><td>/html//div</td><td><br></td></tr><tr cid="n1678" mdtype="table_row"><td>紧挨的下一个</td><td>div+p</td><td>不支持</td><td><br></td></tr><tr cid="n1683" mdtype="table_row"><td>开头是</td><td>[属性名 ^= 属性开头的值]</td><td>//*[starts-with(@属性名, 属性开头的值)]</td><td><br></td></tr><tr cid="n1688" mdtype="table_row"><td>包含</td><td>[属性名 *= 属性开头的值]</td><td>//*[contains(@属性名, 属性包含的值)]</td><td><br></td></tr><tr cid="n1693" mdtype="table_row"><td>结尾是</td><td>[属性名 $= 属性开头的值]</td><td>selenium 中不支持</td><td>xpath 有 ends-with</td></tr><tr cid="n1698" mdtype="table_row"><td>文本</td><td>不支持</td><td>//*[text()='文本的值']</td><td>link_text 和 partial_link_text 的底层实现就是 xpath</td></tr><tr cid="n1703" mdtype="table_row"><td>部分文本</td><td>不支持</td><td>//*[contains(text(),'部分文本的值']</td><td><br></td></tr><tr cid="n1708" mdtype="table_row"><td>下标</td><td><br></td><td>//input[1]</td><td>xpath 表示每个节点下的第一个 input 标签（input 前可以有非 input 标签）</td></tr><tr cid="n1713" mdtype="table_row"><td><br></td><td>input:nth-child(1)</td><td><br></td><td>css 表示 input 必须是父节点下的第一个元素</td></tr></tbody></table>

**动态元素**

**示例 1：这个 class='a'，只有当前页面才有**

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220221/1645406595080044833.png)

**示例 2：class='is-active'**

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220221/1645406610658035060.png)

第二部分：find_element_定位
--------------------

*   源码阅读
    
    ```
        def find_element(self, by=By.ID, value=None) -> WebElement: #返回一个webelement对象
            默认就是ID定位，value默认为空，意味着value是必须传递的参数
            """
            Find an element given a By strategy and locator.
    
            :Usage:
                ::
    
                    element = driver.find_element(By.ID, 'foo')
    
            :rtype: WebElement
            """
            if isinstance(by, RelativeBy): #如果by是一个RelativeBy对象
                return self.find_elements(by=by, value=value)[0] #调用find_elements方法（后面看相对定位的时候解析）
    
            if by == By.ID:   #如果是ID定位
                by = By.CSS_SELECTOR  #改为CSS定位
                value = '[id="%s"]' % value  #把你写的value替换为[ID='VALUE']的格式
            elif by == By.TAG_NAME: #如果是TAG定位
                by = By.CSS_SELECTOR #改为CSS定位，啥也不做，因为CSS的语法TAG_NAME就是直接写
            elif by == By.CLASS_NAME: 
                by = By.CSS_SELECTOR
                value = ".%s" % value  #如果是CLASS定位，在VALUE前加.，那么你能理解为何之前2个类的时候的报错了吗？如果你非要传多个类呢？
            elif by == By.NAME:  
                by = By.CSS_SELECTOR
                value = '[]' % value  #如果是name定位就改写VALUE为CSS的属性法，跟ID是一样的
    
            return self.execute(Command.FIND_ELEMENT, {
                'using': by,
                'value': value})['value']
    
    
    ```
    
*   实例代码
    
    ```
    from selenium import webdriver
    from selenium.webdriver.common.by import By
    
    driver = webdriver.Chrome()
    driver.get('http://121.41.14.39:8088/index.html#/')
    driver.find_element(value='username').send_keys('a')  #ID是默认值可以不传
    driver.find_element(By.CLASS_NAME,'el-input__inner.fm-text.itxt-error.dlname').send_keys('b')      #非要用classname传递多个值
    driver.find_element(By.ID,'username').send_keys('c')  #规矩的写法
    driver.find_element('id','username').send_keys('d')  #直接用str，但有些值你可能记不清楚
    username_locator = ('id','username')   #写一个定位器
    driver.find_element(*username_locator).send_keys('e')  #解包
    driver.find_element(*['id','username']).send_keys('f') 
    driver.find_element(['id','username']).send_keys('g')  #最典型的错误
    
    #selenium.common.exceptions.InvalidArgumentException: Message: invalid argument: 'using' must be a string
    
    
    ```
    

第三部分：相对定位
---------

*   在 selenium4.0 中增加了相对定位（之前叫做友好定位）
    
*   使用场景：当你用 8 个定位方法不那么容易定位到的时候，可以尝试用相对定位（前提是相对定位反而方便）
    
*   selenium 实现相对定位的根本是在于它用了 JavaScript 的 getBoundingClientRect() 方法来获取元素在页面上的大小和位置，从而实现你根据某个元素来定位周边元素的想法。
    

*   导入
    
    ```
    from selenium.webdriver.support.relative_locator import locate_with
    
    ```
    
*   语法
    
    ```
    locate_with(By,using).above(webelement或dict)  最终返回一个RelativeBy对象，可以用来定位
    #含义是: 根据above()括号内的元素或定位器定位其上面的元素，符合locate_with()括号内的表达式
    
    
    locate_with(By,using).above(webelement或dict).below(webelement或dict)  链式调用
    #含义是: 根据above()括号内的元素或定位器定位其上面的元素，同时根据below()括号内的元素或定位器定位其下面的         符合locate_with()括号内的表达式
    
    
    ```
    
*   **示例代码 1：演示语法**
    
    ```
    from selenium import webdriver
    from selenium.webdriver.common.by import By
    from selenium.webdriver.support.relative_locator import locate_with
    driver =webdriver.Chrome()
    driver.get('http://121.41.14.39:8088/index.html#/')
    password_locator = {'id': 'password'}  #字典形式，个人觉得比较生僻
    which_element1 = locate_with(By.TAG_NAME,'input').below(password_locator) #找密码元素下面的标签为input的元素，返回一个元素的定位器（其实是一个RelativeBy对象）
    driver.find_element(which_element1).send_keys('hehe')  #效果在验证码输入框输入hehe
    
    password_webelement = driver.find_element('id','password')  #webelement形式，个人觉得比较直观
    which_element2 = locate_with(By.TAG_NAME,'input').below(password_webelement)
    driver.find_element(which_element2).send_keys('haha')  #效果在验证码输入框追加输入haha
    
    
    img_locator = {'xpath':'//img'}  #验证码图片
    which_element = locate_with(By.TAG_NAME,'input').below(password_locator).to_left_of(img_locator)
    #链式调用 : 最终定位的元素是密码下方，图片左侧，满足条件定位器tag name=input
    driver.find_element(which_element).send_keys('heihei')  ##效果在验证码输入框追加输入heihei
    
    ```
    
*   **示例代码 2：演示上下左右附近 5 个方法**
    
    ```
    from selenium import webdriver
    from selenium.webdriver.common.by import By
    from selenium.webdriver.support.relative_locator import locate_with
    driver =webdriver.Chrome()
    driver.get('http://121.41.14.39:8088/index.html#/')
    
    
    ele_password = driver.find_element('id','password')
    driver.find_element(locate_with(By.TAG_NAME,'input').above(ele_password)).send_keys('密码上方')
    driver.find_element(locate_with(By.TAG_NAME,'input').below(ele_password)).send_keys('密码下方')
    
    
    ele_img  = driver.find_element('xpath','//img')
    driver.find_element(locate_with(By.TAG_NAME,'input').to_left_of(ele_img)).send_keys('图片左侧')
    
    ele_code = driver.find_element('id','code')
    print(driver.find_element(locate_with(By.TAG_NAME, 'img').to_right_of(ele_code)).tag_name)
    
    ele_username = driver.find_element('id','username')
    driver.find_element(locate_with(By.TAG_NAME,'input').near(ele_username)).send_keys('用户名附近')
    #效果如图
    
    ```
    
    ![](http://vip.ytesting.com//upload/ueditor/upload/image/20220221/1645406637848036563.png)
    

第四部分：find_elements_定位
---------------------

*   返回对象是列表
    
*   里面的每个元素都是 webelement 对象
    
*   如果要操作元素或者获取元素的属性需要遍历
    
*   应用场景
    

*   爬虫：往往要获取多组数据
    
*   批量操作：同类型元素的相同操作
    

第五部分：webelement 操作
------------------

**WebElement 对象所有属性、方法**

<table><thead><tr cid="n1786" mdtype="table_row"><th>对象属性 (方法或类)</th><th>说明</th></tr></thead><tbody><tr cid="n1789" mdtype="table_row"><td>accessible_name</td><td><br></td></tr><tr cid="n1792" mdtype="table_row"><td>aria_role</td><td><br></td></tr><tr cid="n1795" mdtype="table_row"><td>clear</td><td>清空</td></tr><tr cid="n1798" mdtype="table_row"><td>click</td><td>点击</td></tr><tr cid="n1801" mdtype="table_row"><td>find_element</td><td>元素上找元素</td></tr><tr cid="n1804" mdtype="table_row"><td>find_element_by_class_name</td><td>同上，未废弃</td></tr><tr cid="n1807" mdtype="table_row"><td>find_element_by_css_selector</td><td>同上</td></tr><tr cid="n1810" mdtype="table_row"><td>find_element_by_id</td><td>同上</td></tr><tr cid="n1813" mdtype="table_row"><td>find_element_by_link_text</td><td>同上</td></tr><tr cid="n1816" mdtype="table_row"><td>find_element_by_name</td><td>同上</td></tr><tr cid="n1819" mdtype="table_row"><td>find_element_by_partial_link_text</td><td>同上</td></tr><tr cid="n1822" mdtype="table_row"><td>find_element_by_tag_name</td><td>同上</td></tr><tr cid="n1825" mdtype="table_row"><td>find_element_by_xpath</td><td>同上</td></tr><tr cid="n1828" mdtype="table_row"><td>find_elements</td><td>同上</td></tr><tr cid="n1831" mdtype="table_row"><td>find_elements_by_class_name</td><td>同上</td></tr><tr cid="n1834" mdtype="table_row"><td>find_elements_by_css_selector</td><td>同上</td></tr><tr cid="n1837" mdtype="table_row"><td>find_elements_by_id</td><td>同上</td></tr><tr cid="n1840" mdtype="table_row"><td>find_elements_by_link_text</td><td>同上</td></tr><tr cid="n1843" mdtype="table_row"><td>find_elements_by_name</td><td>同上</td></tr><tr cid="n1846" mdtype="table_row"><td>find_elements_by_partial_link_text</td><td>同上</td></tr><tr cid="n1849" mdtype="table_row"><td>find_elements_by_tag_name</td><td>同上</td></tr><tr cid="n1852" mdtype="table_row"><td>find_elements_by_xpath</td><td>同上</td></tr><tr cid="n1855" mdtype="table_row"><td>get_attribute</td><td>获取元素属性</td></tr><tr cid="n1858" mdtype="table_row"><td>get_dom_attribute</td><td><br></td></tr><tr cid="n1861" mdtype="table_row"><td>get_property</td><td><br></td></tr><tr cid="n1864" mdtype="table_row"><td>id</td><td><br></td></tr><tr cid="n1867" mdtype="table_row"><td>is_displayed</td><td>是否显示</td></tr><tr cid="n1870" mdtype="table_row"><td>is_enabled</td><td>是否使能</td></tr><tr cid="n1873" mdtype="table_row"><td>is_selected</td><td>是否可选</td></tr><tr cid="n1876" mdtype="table_row"><td>location</td><td>元素位置</td></tr><tr cid="n1879" mdtype="table_row"><td>location_once_scrolled_into_view</td><td>滚动到可见</td></tr><tr cid="n1882" mdtype="table_row"><td>parent</td><td><br></td></tr><tr cid="n1885" mdtype="table_row"><td>rect</td><td>元素矩形</td></tr><tr cid="n1888" mdtype="table_row"><td>screenshot</td><td><br></td></tr><tr cid="n1891" mdtype="table_row"><td>screenshot_as_base64</td><td><br></td></tr><tr cid="n1894" mdtype="table_row"><td>screenshot_as_png</td><td>元素截图</td></tr><tr cid="n1897" mdtype="table_row"><td>send_keys</td><td>元素上输入内容</td></tr><tr cid="n1900" mdtype="table_row"><td>shadow_root</td><td><br></td></tr><tr cid="n1903" mdtype="table_row"><td>size</td><td>元素大小</td></tr><tr cid="n1906" mdtype="table_row"><td>submit</td><td>提交元素</td></tr><tr cid="n1909" mdtype="table_row"><td>tag_name</td><td>元素标签名</td></tr><tr cid="n1912" mdtype="table_row"><td>text</td><td>元素文本</td></tr><tr cid="n1915" mdtype="table_row"><td>value_of_css_property</td><td>元素的 css 属性值</td></tr></tbody></table>

示例代码 DAY3
---------

**验证码破解**

```
from selenium import webdriver
from time import sleep
import ddddocr
driver = webdriver.Chrome()
driver.get('http://121.41.14.39:8088/index.html')
driver.find_element('css selector','#username').send_keys('sq1')
driver.find_element('css selector','#password').send_keys('123')
ele_img = driver.find_element('css selector','img')
ocr = ddddocr.DdddOcr(show_ad=False)
text = ocr.classification(ele_img.screenshot_as_png)
# with open('code.png','wb') as f:
#     f.write(ele_img.screenshot_as_png)  #bytes类型，可以保存截图
sleep(2)
driver.find_element('css selector','#code').send_keys(text)
sleep(2)
driver.find_element('css selector','#submitButton').click()

```