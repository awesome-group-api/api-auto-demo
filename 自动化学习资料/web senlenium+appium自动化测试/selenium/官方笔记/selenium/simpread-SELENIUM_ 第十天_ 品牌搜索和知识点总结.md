> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b587acce5f6017b70b452ee3702)

> 用例步骤

*   用例步骤
    
    ```
    1. 登录宝利商城
    2. 点击品牌管理  ： 新的页面
        2.1 获取当前页的所有品牌
        2.2 搜索指定的品牌信息
            2.2.1 输入一个品牌  ： 品牌来自于当前的页面（随机取一个）
            2.2.2 点击查询结果
    3. 断言：你搜索的品牌名 在查询结果中
    
    ```
    
*   品牌管理页及方法
    
    ```
    from common.basePage import BasePage
    
    
    class  BrandManagePage(BasePage):
        def get_curpage_allbrands(self): #获取当前页所有品牌，此处设计基类的改造
            return self.get_elements_text(self.all_brand_name_txt)
        def search_brand(self,brandname): #搜索指定品牌名
            self.input_text(self.brand_search_input,brandname)
            self.wait_click_element(self.search_btn)
        #得到所有的品牌信息的，不推荐用点点点
        #你可以借助于接口，如果有数据库的操作权限也可以调用数据库接口去获取
        #如果你用点点点，你可能要加强制等待
        #每个页面都有类似的locator，1页.locator=2页.locator
        #配合前端 增加一个搜索完毕的标志，非常好判断
    
    ```
    
*   基类改造：增加获取多个元素的文本的方法
    
    ```
        def get_element_text(self,locator):
            """
            获取元素的文本
            :param locator: 定位器
            :return:
            """
            return self.get_element(locator).text
        def get_elements_text(self,locator):
            #self.get_elements(locator)这是所有的元素
            #遍历得到每个元素的text
            return [ele.text for ele in self.get_elements(locator)]
    
    ```
    
*   mainpage 改造：增加跳转到品牌管理的方法
    
    ```
        def goto_brandmanagepage(self):
            self.click_element(self.menu_productmanage)
            self.wait_click_element(self.submenu_pm_brandmanage)
            return BrandManagePage()
    
    ```
    
*   测试用例：示例代码
    
    ```
    # Author:  wuxianfeng
    # Company: songqin
    # File:    test_searchbrand.py
    # Date:    2021/10/25
    # Time:    20:34
    from pageObjects.loginPage import LoginPage
    
    print()
    '''
    知识点:测试搜索品牌
    '''
    import  pytest
    import random
    def  test_searchbrand(init_pm):
        """
        1. 登录宝利商城
        2. 点击品牌管理  ： 新的页面
            2.1 获取当前页的所有品牌
            2.2 搜索指定的品牌信息
                2.2.1 输入一个品牌  ： 品牌来自于当前的页面（随机取一个）
                2.2.2 点击查询结果
        3. 断言：你搜索的品牌名 在查询结果中
        :return:
        """
        # 1. 登录宝利
        test_mainpage = init_pm
        test_mainpage = LoginPage().open_loginpage().login_polly('松勤老师','123456')
        # 2. 跳转到品牌管理
        test_brandpage = test_mainpage.goto_brandmanagepage()
        # 3. 获取当前页的所有品牌名
        init_brands = test_brandpage.get_curpage_allbrands()
        print(init_brands)
        # 4. 随机取一个品牌
        test_brandname = random.choice(init_brands)
        print(test_brandname)
        # 5. 搜索品牌
        test_brandpage.search_brand(test_brandname)
        # 6. 搜索后的品牌信息
        from time import sleep
        sleep(1)
        result_brands = test_brandpage.get_curpage_allbrands()
        print(result_brands)
        # 7. 断言
        # 实现方法1
        # 测试反向  result_brands.append('haha')
        # for _ in result_brands:
        #     if test_brandname not in _:
        #         assert False
        # else:
        #     assert True
        # 实现方法2
        # s = 'a'
        # result1 = ['a', 'ab', 'abc']  # True
        # result2 = ['a', 'ab', 'abc', 'b']  # False
        # print(list(filter(lambda x: s in x, result2)))
        assert len(list(filter(lambda x: test_brandname in x, result_brands)))==len(result_brands)
        # python的基础非常的重要
    if __name__ == '__main__':
        pytest.main(['-sv',__file__])
    
    ```
    
*   上面的测试最后解决了一个小 python 问题
    
    ```
    Question: 判断目标字符串S是否在结果字符串列表的每个元素中出现
        s = 'a'
        result_true = ['a','aa','ab']
        result_false = ['a','b']
    
    ```
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20211025/1635133629602041962.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20211025/1635133638993011155.png)