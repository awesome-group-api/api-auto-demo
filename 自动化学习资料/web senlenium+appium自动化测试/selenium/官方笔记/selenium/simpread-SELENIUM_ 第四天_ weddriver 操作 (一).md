> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b587eddaee2017f19e832883dea)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20220221/1645407104988047367.png)

第一部分：webdriver 基础操作
-------------------

<table><thead><tr cid="n1932" mdtype="table_row"><th>对象属性、方法</th><th>说明</th></tr></thead><tbody><tr cid="n1935" mdtype="table_row"><td>add_cookie</td><td>添加 cookie</td></tr><tr cid="n1938" mdtype="table_row"><td>back</td><td>返回</td></tr><tr cid="n1941" mdtype="table_row"><td>close</td><td>关闭不退出进程</td></tr><tr cid="n1944" mdtype="table_row"><td>current_url</td><td>当前 URL</td></tr><tr cid="n1947" mdtype="table_row"><td>current_window_handle</td><td>当前窗口句柄</td></tr><tr cid="n1950" mdtype="table_row"><td>delete_all_cookies</td><td>删除所有 cookie</td></tr><tr cid="n1953" mdtype="table_row"><td>delete_cookie</td><td>删除指定 cookie</td></tr><tr cid="n1956" mdtype="table_row"><td>desired_capabilities</td><td>能力值</td></tr><tr cid="n1959" mdtype="table_row"><td>execute_cdp_cmd</td><td>执行 cdp 命令</td></tr><tr cid="n1962" mdtype="table_row"><td>execute_script</td><td>执行 js 代码</td></tr><tr cid="n1965" mdtype="table_row"><td>forward</td><td>前进</td></tr><tr cid="n1968" mdtype="table_row"><td>fullscreen_window</td><td>全屏</td></tr><tr cid="n1971" mdtype="table_row"><td>get</td><td>打开网址</td></tr><tr cid="n1974" mdtype="table_row"><td>get_cookie</td><td>获取 cookie</td></tr><tr cid="n1977" mdtype="table_row"><td>get_cookies</td><td>获取所有 cookie</td></tr><tr cid="n1980" mdtype="table_row"><td>get_window_position</td><td>获取窗口位置</td></tr><tr cid="n1983" mdtype="table_row"><td>get_window_rect</td><td>获取窗口矩形</td></tr><tr cid="n1986" mdtype="table_row"><td>get_window_size</td><td>获取窗口大小</td></tr><tr cid="n1989" mdtype="table_row"><td>implicitly_wait</td><td>隐式等待</td></tr><tr cid="n1992" mdtype="table_row"><td>maximize_window</td><td>最大化窗口</td></tr><tr cid="n1995" mdtype="table_row"><td>minimize_window</td><td>最小化窗口</td></tr><tr cid="n1998" mdtype="table_row"><td>name</td><td>浏览器名字</td></tr><tr cid="n2001" mdtype="table_row"><td>page_source</td><td>页面源码</td></tr><tr cid="n2004" mdtype="table_row"><td>quit</td><td>退出浏览器进程</td></tr><tr cid="n2007" mdtype="table_row"><td>refresh</td><td>刷新</td></tr><tr cid="n2010" mdtype="table_row"><td>save_screenshot</td><td>保存截图</td></tr><tr cid="n2013" mdtype="table_row"><td>set_page_load_timeout</td><td>设置页面加载超时时间</td></tr><tr cid="n2016" mdtype="table_row"><td>set_window_position</td><td>设置窗口位置</td></tr><tr cid="n2019" mdtype="table_row"><td>set_window_rect</td><td>设置窗口矩形</td></tr><tr cid="n2022" mdtype="table_row"><td>set_window_size</td><td>设置窗口大小</td></tr><tr cid="n2025" mdtype="table_row"><td>switch_to</td><td>切换到</td></tr><tr cid="n2028" mdtype="table_row"><td>title</td><td>浏览器标题</td></tr><tr cid="n2031" mdtype="table_row"><td>window_handles</td><td>窗口句柄</td></tr></tbody></table>

第二部分：无头浏览器及 option
------------------

### headless

**历史由来：**

*   [Phantomjs](https://phantomjs.org/)4.0 中彻底废除了
    

**应用场景：**

*   无头的场景，一般先有头测试，再无头运行
    
*   节省资源
    
*   不关注正常的操作过程
    
*   对错误的仍然可以截图
    

**chrome 示例代码**

```
from  selenium  import  webdriver
my_option = webdriver.ChromeOptions()
my_option.add_argument('-headless')
driver = webdriver.Chrome(options=my_option)

```

**firefox 示例代码**

```
from  selenium  import  webdriver
my_option = webdriver.FirefoxOptions()
my_option.add_argument('-headless')
driver = webdriver.Firefox(options=my_option)

```

**注意事项：**

*   浏览器的默认大小不太一样
    
*   一般建议打开后最大化浏览器
    
*   不会影响截图
    
*   注意 headless 的参数可以是
    
    ```
    -headless
    --headless
    但如果是headless，chrome是可以的，但firefox不可以
    
    ```
    

### 关于 option

**示例代码 1：免登录**

*   user_data_dir 的获取可以通过 chrome://version 得到，注意去掉尾部的 default
    
    ![](http://vip.ytesting.com//upload/ueditor/upload/image/20220221/1645407075714067231.png)
    

```
from  selenium import webdriver      
myoption = webdriver.ChromeOptions()
user_data_dir = r'C:\Users\songqin008\AppData\Local\Google\Chrome\User Data' #我的路径跟你 的不一样
myoption.add_argument(f'--user-data-dir={user_data_dir}')  #选项中就有用户数据了
driver = webdriver.Chrome(options=myoption)    
driver.get('https://processon.com/')  #打开一个网页，注意这个网页你应该要事先登录过

```

### 关于 CDP

> [https://chromedevtools.github.io/devtools-protocol/](https://chromedevtools.github.io/devtools-protocol/)

**示例代码 1：典型的 CDP 命令**

```
import base64
from selenium import webdriver
from time import sleep
driver = webdriver.Chrome()
test_flag = 3
if test_flag ==1: #关闭浏览器，相当于quit
    driver.execute_cdp_cmd('Browser.close',{})

if test_flag ==2: #  截图
    driver.get('https://www.baidu.com')
    res = driver.execute_cdp_cmd('Page.captureScreenshot', {})
    with open('test.png', 'wb') as f:
        img = base64.b64decode(res['data'])
        f.write(img)
if test_flag ==3: # 开网址
    cdp_cmd = 'Page.navigate'
    cdp_para = {
       "url": "https://cn.bing.com/"
    }
    driver.execute_cdp_cmd(cdp_cmd,cdp_para)

```

第三部分：select 元素操作
----------------

*   本质上也可以用点击来完成，但 selenium 提供了 API 来操作，操作更加方便
    
*   导入包
    
    ```
    from selenium.webdriver.support.select import Select
    
    ```
    
*   方法说明
    
    ```
    select_by_visible_text('男')   #可见的文本 
    select_by_value('2')      #value属性的值 
    select_by_index(0)       #下拉菜单的第一个选项
    deselect_all()          #反选所有
    
    ```
    
*   注意事项：对于 select_by_visible_text，可见的文本就是用户见到的文本，但有的同学在使用 UI 对战平台的下拉示例的时候，可见的文本以为要复制 HTML 源码，那是有空格的，如果把空格复制进去你就得不到结果，需要
    

第四部分：键盘模块
---------

*   导入包
    
    ```
    from selenium.webdriver import Keys
    
    ```
    
*   源码
    
    ```
    class Keys(object):
        BACKSPACE = u'\ue003'
        BACK_SPACE = BACKSPACE
        ENTER = u'\ue007'
        CONTROL = u'\ue009'
    
    ```
    
*   注意事项
    

*   结合 send_keys() 发送组合键
    
*   CTRL+A 这样的组合键，可以是 A 或 a
    
*   可以用乘法进行多次操作
    
*   可以组合文本（如输入信息后 + ENTER，变相实现提交功能）
    

第五部分：鼠标模块
---------

*   导入包
    
    ```
    from selenium.webdriver import ActionChains
    
    ```
    
*   语法
    
    ```
    #第一种写法:链式写法
    ActionChains(driver).动作1.动作2.perform()
    #第二种写法:分步执行
    myaciton = ActionChains(driver)
    myaciton.动作1
    myaciton.动作2
    myaciton.perform()
    
    ```
    
*   常见的方法
    
    <table><thead><tr cid="n2145" mdtype="table_row"><th>动作</th><th>说明</th></tr></thead><tbody><tr cid="n2148" mdtype="table_row"><td>context_click(self, on_element=None)</td><td>右键</td></tr><tr cid="n2151" mdtype="table_row"><td>double_click(self, on_element=None)</td><td>双击</td></tr><tr cid="n2154" mdtype="table_row"><td>move_to_element(self, to_element)</td><td>鼠标移动到某个元素</td></tr><tr cid="n2157" mdtype="table_row"><td>click(self, on_element=None)</td><td>左键</td></tr><tr cid="n2160" mdtype="table_row"><td>drag_and_drop(self, source, target)</td><td>拖拽到某个元素然后松开</td></tr><tr cid="n2163" mdtype="table_row"><td>drag_and_drop_by_offset(self, source, xoffset, yoffset)</td><td>拖拽到某个坐标然后松开</td></tr><tr cid="n2166" mdtype="table_row"><td><br></td><td><br></td></tr><tr cid="n2169" mdtype="table_row"><td>key_down(self, value, element=None)</td><td>按下某个键</td></tr><tr cid="n2172" mdtype="table_row"><td>key_up(self, value, element=None)</td><td>松开某个键</td></tr><tr cid="n2175" mdtype="table_row"><td>move_by_offset(self, xoffset, yoffset)</td><td>鼠标从当前位置移动到某个坐标</td></tr><tr cid="n2178" mdtype="table_row"><td>move_to_element_with_offset(self, to_element, xoffset, yoffset)</td><td>移动到距某个元素（左上角坐标）多少距离的位置</td></tr><tr cid="n2181" mdtype="table_row"><td>release(self, on_element=None)</td><td>在某个元素位置松开鼠标左键</td></tr><tr cid="n2184" mdtype="table_row"><td>send_keys(self, *keys_to_send)</td><td>发送某个键到当前焦点的元素</td></tr><tr cid="n2187" mdtype="table_row"><td>send_keys_to_element(self, element, *keys_to_send)</td><td>发送某个键到指定元素</td></tr><tr cid="n2190" mdtype="table_row"><td>click_and_hold(self, on_element=None)</td><td>按住左键不放</td></tr><tr cid="n2193" mdtype="table_row"><td><br></td><td><br></td></tr></tbody></table>