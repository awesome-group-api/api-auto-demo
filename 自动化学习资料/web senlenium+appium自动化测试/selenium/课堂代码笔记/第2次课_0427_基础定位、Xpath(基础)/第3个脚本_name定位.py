# Author:  wuxianfeng
# Company: songqin
# File:    第3个脚本_name定位.py
# Date:    22-4-27
# Time:    下午 8:38
from selenium.webdriver.common.by import By

print()
'''
知识点:  通过name属性的值来定位
1. 元素上的输入内容，send_keys输入的
'''
from selenium import webdriver
from time import sleep
driver = webdriver.Chrome('/usr/local/bin/chromeDriver')
driver.get(r'/Users/bytedance/Desktop/web_robot/api-auto-demo/day2.html')
print(driver.find_element(By.NAME, 'username').text)  #text没有
# 这是一个输入框，输入内容
driver.find_element(By.NAME, 'username').send_keys('username')
