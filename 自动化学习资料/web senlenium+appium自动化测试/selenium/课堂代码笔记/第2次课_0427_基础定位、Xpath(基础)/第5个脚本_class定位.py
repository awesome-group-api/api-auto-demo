# Author:  wuxianfeng
# Company: songqin
# File:    第5个脚本_class定位.py
# Date:    22-4-27
# Time:    下午 8:51
from selenium.webdriver.common.by import By

print()
'''
知识点: 
1. class="px vm"  表示的是class1的值是px，class2的值是vm
2. 如果要对多值class属性定位，只能写一个值（？？？其实可以写多个值）
3. class也会重复！
'''
from selenium import webdriver
from time import sleep
driver = webdriver.Chrome()
driver.get(r'D:\pythonProject\AutoTest\AutoSelenium0425\第2次课_0427_基础定位、Xpath(基础)\day2.html')
driver.find_element(By.CLASS_NAME,'pv').send_keys('password')

driver.find_element(By.CLASS_NAME,'vm').send_keys('username')
# selenium.common.exceptions.NoSuchElementException: Message: no such element: Unable to locate element: {"method":"css selector","selector":".px vm"}
# 无法定位到这个元素，方法是css selector（？？？），值是.px vm（？？？）
#   (Session info: chrome=100.0.4896.127)


