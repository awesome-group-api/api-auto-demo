# Author:  wuxianfeng
# Company: songqin
# File:    第8个脚本_xpath的路径法.py
# Date:    22-4-27
# Time:    下午 9:17
from selenium.webdriver.common.by import By

print()
'''
知识点:
1 .绝对路径 /
    html: /html/body/div/p  
    绝对路径很少单独使用，往往非常长，如果某个中间节点发生了变化，会导致整个表达式失效！
    
2. 相对路径 //
    //*  所有的标签
    //p
    //a
    通过下标选择，但是要注意，第一个元素是[1]，不是[0]跟列表要注意区分
    //input[2]   #全文第二个INPUT标签，×！！！！
    //a[2]       #每个父节点下的第二个a标签！！（如果没明白，去敲表达式，观察界面变化）
    
    拓展：不作要求
        last()  #最后一个
        position()  #根据所在位置
        
    当前路径.和父目录..      //body/div[1]/..  #用在儿子比较好认，父亲没有明显特征的情况

3. 浏览器的定位技巧 ★★★★★★
    3.1 F12打开开发者工具
    3.2 选择一个元素
    3.3 CTRL+F  ==>find by string,xpath,css selector
                          string是干扰项（css语法会和string语法冲突）
    3.4 输入xpath表达式（如/html/body/div/p)
        观察界面，定位到的元素会泛黄显示，右侧会有1 of N的字样，尽量要做到1of1
    
'''
from selenium import webdriver
from time import sleep
driver = webdriver.Chrome()
driver.get(r'D:\pythonProject\AutoTest\AutoSelenium0425\第2次课_0427_基础定位、Xpath(基础)\day2.html')
print(driver.find_element(By.XPATH, '/html/body/div/p').text)