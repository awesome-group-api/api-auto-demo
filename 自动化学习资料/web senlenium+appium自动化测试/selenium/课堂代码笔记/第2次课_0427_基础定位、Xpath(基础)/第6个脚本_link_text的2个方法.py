# Author:  wuxianfeng
# Company: songqin
# File:    第6个脚本_link_text的2个方法.py
# Date:    22-4-27
# Time:    下午 8:59
from selenium.webdriver.common.by import By

print()
'''
知识点: a标签 超链接， anchor 锚
1. <a href="https://www.qq.com">腾讯QQ</a>
   其link_text 是  腾讯QQ，完全匹配
   其partial_link_text是"腾" "腾讯" "QQ" 只要是部分即可，模糊查询
2. LINK_TEXT和 PARTIAL_LINK_TEXT 只针对a标签生效，其他的如p都无法操作此类元素的
'''
from selenium import webdriver
from time import sleep
driver = webdriver.Chrome()
driver.get(r'D:\pythonProject\AutoTest\AutoSelenium0425\第2次课_0427_基础定位、Xpath(基础)\day2.html')
print(driver.find_element(By.LINK_TEXT, '腾讯QQ').text)
print(driver.find_element(By.PARTIAL_LINK_TEXT, 'QQ').text)
print(driver.find_element(By.PARTIAL_LINK_TEXT, '腾讯').text)
print(driver.find_element(By.PARTIAL_LINK_TEXT, '段落').text)