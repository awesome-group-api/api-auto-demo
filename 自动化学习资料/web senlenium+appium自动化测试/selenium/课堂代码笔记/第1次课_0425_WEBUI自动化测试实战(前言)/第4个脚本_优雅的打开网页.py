# Author:  wuxianfeng
# Company: songqin
# File:    第4个脚本_优雅的打开网页.py
# Date:    22-4-25
# Time:    下午 10:03
print()
'''
知识点:
  with  上下文管理器  对象实现了 __enter__ __exit__
'''
from selenium import webdriver
from time import sleep  #只是为了演示用
with webdriver.Chrome() as driver:
    driver.get('https://www.baidu.com')
    sleep(2)