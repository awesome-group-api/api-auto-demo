import requests
from time import sleep
#登录
# 在cmd下启动chromedriver，默认端口是9515，若有更改需要同步修改下面的代码
# 1. 打开浏览器
server_url = 'http://127.0.0.1:9515/session'  #这个url不能以/结尾
session_data = {
    'desiredCapabilities':{
        "caps":{"browserName":"chrome"} #打开chrome浏览器所需的能力值
    }
}
session_req = requests.post(url=server_url,json=session_data)  #此处不能是data
# 2. 输入网址
session_id = session_req.json().get('sessionId')
dest_url = 'http://106.14.1.150:8090/forum.php'  #这是我在阿里云部署的一个服务器
get_url_data = {'url':dest_url}
requests.post(server_url+'/'+session_id+'/url',json=get_url_data)