from selenium import webdriver
from time import sleep
driver = webdriver.Chrome()
driver.get('https://www.baidu.com')
ele_tag_a = driver.find_element('tag name','a')
print('元素的对象类型是:',type(ele_tag_a)) #WebElement
print('|对象属性(方法)|说明|')
print('|---|---|')
for _ in dir(ele_tag_a):
    if _[0]!='_':
        print('|',_,'||')