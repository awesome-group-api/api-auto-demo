# Author:  wuxianfeng
# Company: songqin
# File:    第3个脚本_css伪类选择器.py
# Date:    22-4-29
# Time:    下午 8:36
print()
'''
知识点:
1. p:nth-child(1)  第一个儿子一定是p注意跟xpath的区别
2. :last-child   能出现几个元素？
3. :only-child 一定有，至少是html（这一层的唯一的一个）
'''
