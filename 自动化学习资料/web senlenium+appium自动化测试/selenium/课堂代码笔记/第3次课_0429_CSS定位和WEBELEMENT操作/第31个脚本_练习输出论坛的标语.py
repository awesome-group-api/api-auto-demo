# Author:  wuxianfeng
# Company: songqin
# File:    第31个脚本_练习输出论坛的标语.py
# Date:    22-4-29
# Time:    下午 8:43
print()
'''
知识点:  对css语法的复习
1. 官方论坛下有个标语：提供最新 Discuz! 产品新闻、软件下载与技术交流
2. 输出这个文本（不能用文本本身的信息）
'''
from selenium import webdriver
from time import sleep
driver = webdriver.Chrome('/usr/local/bin/chromeDriver')
driver.get('http://106.14.1.150:8090/forum.php')
css1 = 'h5+p'
css2 = '.lk_content.z>p'
css3 = '#category_lk p'
css4 = "[class='bm_c ptm'] p"
css5 = ".lk_content>p:nth-child(2)"
for css in (css5,css4,css3,css2,css1):
    print(driver.find_element('css selector',css).text)
