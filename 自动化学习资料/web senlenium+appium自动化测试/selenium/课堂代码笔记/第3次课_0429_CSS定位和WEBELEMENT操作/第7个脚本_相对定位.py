# Author:  wuxianfeng
# Company: songqin
# File:    第7个脚本_相对定位.py
# Date:    22-4-29
# Time:    下午 9:47
from selenium.webdriver.common.by import By
from selenium.webdriver.support.relative_locator import locate_with

print()
'''
知识点:locate_with
1. locate_with 传递的参数 by , using
2. 返回是RelativeBy
3. RelativeBy的初始化是怎么做
---
4. get_attribute的使用
'''
from selenium import webdriver
from time import sleep
driver = webdriver.Chrome('/usr/local/bin/chromeDriver')
driver.get('http://121.41.14.39:8088/index.html')
ele_username = driver.find_element('id','username')
ele_password = driver.find_element('id','password')
ele_code = driver.find_element('id','code')
ele_codepic = driver.find_element('tag name','img')
ele_loginbutton = driver.find_element('id','submitButton')

# 利用语法来定位
#       根据ele_password找它上面的一个input标签
# 1. 上
driver.find_element(locate_with(By.TAG_NAME,'input').above(ele_password)).send_keys('密码上')
# 2. 下
driver.find_element(locate_with(By.TAG_NAME,'input').below(ele_username)).send_keys('用户名下')
# 3. 左侧
# 有的同学同样的代码会失败？有可能会定位到勾选框，它无法send_keys
driver.find_element(locate_with(By.TAG_NAME,'input').to_left_of(ele_codepic)).send_keys('验证码图片左')
# 4. 右侧
print(driver.find_element(locate_with(By.TAG_NAME, 'img').to_right_of(ele_code)).get_attribute('src'))
# http://121.41.14.39:8088/verifyCode?time=Fri%20Apr%2029%202022%2022:04:44%20GMT+0800%20(%E4%B8%AD%E5%9B%BD%E6%A0%87%E5%87%86%E6%97%B6%E9%97%B4)

# 5. near
#  button 无法找到
#  span可以产生点击的效果
driver.find_element(locate_with(By.TAG_NAME,'span').near(ele_code)).click()

