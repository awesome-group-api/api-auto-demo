# Author:  wuxianfeng
# Company: songqin
# File:    第6个脚本_认识三种等待.py
# Date:    22-5-8
# Time:    上午 11:01
print()
'''
知识点:   官网 https://www.selenium.dev/zh-cn/documentation/webdriver/waits/
1. 强制等待 sleep
    固定的，更多用于演示，用来辅助调试，不稳定（延迟是随着环境的改变而变化）
    但是有情况是建议添加的（实战中有案例）
2. 隐式等待   implicitly_wait
    2.1 推荐大家在打开浏览器后直接设置
    2.2 implicitly_wait(最大等待时间)  单位是秒
        网页加载元素完成后就可以进行操作，你设置100s，加载10s，10s后就可以操作不要等100s
        等待网页全部加载完毕（你要操作只是用户名，但网页会加载所有，用户名可能早就加载了，造成了一定的浪费）
        一次设置全局生效，不要对每个元素进行等待
        只能针对元素的出现与否进行判断，但对比如窗口的打开，打开了几个，url的变化，元素的属性等无能为力
        
3. 显式等待  #推荐用，
    3.1 每个场景都要单独设置（可以规避的，封装）
    3.2 适配所有的需要等待的场景，甚至写自己的方法来扩展显式等待
    
----
1. 认识隐式等待的最大超时时间
'''
from selenium import webdriver
from time import sleep,ctime
driver = webdriver.Chrome('/usr/local/bin/chromeDriver')
driver.implicitly_wait(25)  #     2.1 推荐大家在打开浏览器后直接设置
driver.maximize_window()
driver.get('http://106.14.1.150:8090/forum.php')
print('当前时间是:',ctime())
try:
    driver.find_element('id','ls_usernam').send_keys('admin')
except:
    print(ctime())
finally:
    print(ctime())  #手工修改网页用户名元素的id的值为ls_usernam
    driver.quit()
