# Author:  wuxianfeng
# Company: songqin
# File:    第2个脚本_VIP系统 的登录.py
# Date:    22-5-8
# Time:    上午 9:25
from selenium.webdriver import ActionChains

print()
'''
知识点:  1. 自动化测试的时候验证码不应该过多关注（让开发屏蔽，提供万能验证码）
2. 爬虫的时候你绕不开（option、cookie等试试）
'''
from selenium import webdriver
from time import sleep
driver = webdriver.Chrome('/usr/local/bin/chromeDriver')
driver.maximize_window()
driver.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {
        "source": """Object.defineProperty(navigator, 'webdriver', {get: () => undefined})""",
    })  #让浏览器不知道你是selenium  #这个代码可能会失效
driver.get('http://vip.ytesting.com/loginController.do?login')

driver.find_element('id','userName').send_keys('wuxf')
driver.find_element('id','password').send_keys('12345678')
# 滑块拖动
## 滑块元素
ele_huakuai = driver.find_element('id','nc_1_n1z')
## 拖动条
ele_area = driver.find_element('id','nc_1__scale_text')
# 滑块经过的水平方向的距离
x_distance = ele_area.rect['width']-ele_huakuai.rect['width']
# 滑块经过的垂直方向的距离
y_distance = 0
ActionChains(driver).drag_and_drop_by_offset(ele_huakuai,x_distance,y_distance).perform()
# but_login
sleep(1)
driver.find_element('id','but_login').click()

# 第一次运行：哎呀，出错了，点击刷新再来一次(error:8

# 调试打开网址后手工输入也不行，打开了网页就错了！网站检测到了selenium！
# 思路：有没有办法让网站不知道我是selenium ， 才可以去搜索
