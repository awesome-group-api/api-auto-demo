# Author:  wuxianfeng
# Company: songqin
# File:    第1个脚本_认识鼠标操作.py
# Date:    22-5-8
# Time:    上午 9:10
from selenium.webdriver import ActionChains

print()
'''
知识点:

'''
from selenium import webdriver
from time import sleep
driver = webdriver.Chrome('/usr/local/bin/chromeDriver')
test_flag = 3
if test_flag ==3:  #拖拽的源码实现，click_and_hold 和release
    driver.get('http://sahitest.com/demo/dragDropMooTools.htm')
    ele_drag = driver.find_element('css selector','#dragger')
    ele_items = driver.find_elements('css selector','.item')
    # 把ele_drag 拖拽到ele_item中
    for ele_item in ele_items:
        # ActionChains(driver).drag_and_drop(ele_drag,ele_item).perform()
        myaction = ActionChains(driver)
        myaction.click_and_hold(ele_drag)
        myaction.release(ele_item)
        myaction.perform()
        sleep(1)

if test_flag ==2:  #拖拽
    driver.get('http://sahitest.com/demo/dragDropMooTools.htm')
    ele_drag = driver.find_element('css selector','#dragger')
    ele_items = driver.find_elements('css selector','.item')
    # 把ele_drag 拖拽到ele_item中
    for ele_item in ele_items:
        ActionChains(driver).drag_and_drop(ele_drag,ele_item).perform()
        sleep(1)
if test_flag ==1:  #三种点击操作
    driver.get('http://sahitest.com/demo/clicks.htm')
    ele_double_click = "//*[@value='dbl click me']"
    ele_click = "//*[@value='click me']"
    ele_right_click = "//*[@value='right click me']"
    # 单击
    ActionChains(driver).click(driver.find_element('xpath',ele_click)).perform()
    # 双击
    ActionChains(driver).double_click(driver.find_element('xpath',ele_double_click)).perform()
    # 右击  context_click
    ActionChains(driver).context_click(driver.find_element('xpath',ele_right_click)).perform()
















