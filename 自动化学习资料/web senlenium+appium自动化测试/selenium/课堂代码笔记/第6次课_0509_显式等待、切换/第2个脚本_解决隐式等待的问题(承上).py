# Author:  wuxianfeng
# Company: songqin
# File:    第2个脚本_解决隐式等待的问题(承上).py
# Date:    22-5-9
# Time:    下午 8:21
print()
'''
知识点: 接口(异步)学过 轮询==>显式等待的核心之一
'''
from selenium import webdriver
import time
driver = webdriver.Chrome()
driver.implicitly_wait(5)
driver.maximize_window()
driver.get('http://106.14.1.150:8090/forum.php')
time_start = time.time()  #起始时间
time_out = 30  #超时时间
time_end = time_start+time_out  #结束时间
frequency_time = 1   #频率
while  True:
    print('现在的时间是',time.ctime())
    if driver.find_element('id', 'ls_username').get_attribute('placeholder'):#有值了
        print('现在有placeholder这个属性')
        break
    time.sleep(frequency_time)  #等待的间隔
    if time.time()>time_end: #如果超过了结束时间
        print('时间到了，不等了，走吧')
        break

