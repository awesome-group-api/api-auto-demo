# Author:  wuxianfeng
# Company: songqin
# File:    第4个脚本_认识显式等待的until.py
# Date:    22-5-9
# Time:    下午 8:47
print()
'''
知识点:  until的源码
---
    def until(self, method, message=''):
        ☆第一个参数:method 方法====>EC.那些方法，但也可以自定义
        end_time = time.time() + self._timeout  #定义结束时间
        while True:  #死循环
            try:
                value = method(self._driver)  #核心代码:传入的方法，调用并给一个参数driver
                                              #跟装饰器的写法，闭包很类似
                if value:  #如果这个方法调用后有值
                    return value  #就返回这个值，就跳出
            time.sleep(self._poll)  #如果没有值，就等
            # print(f'等待了{self._poll}')
            if time.time() > end_time:  #看是否超过结束时间，
                break  #超过就跳出，不等了！
---
     
'''
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from time import sleep,ctime
driver = webdriver.Chrome()
driver.maximize_window()
driver.get('http://106.14.1.150:8090/forum.php')
#WebDriverWait(driver,5,0.5).until(方法)
# 这个方法接受一个参数是driver，
# lambda x:x.find_element('id','ls_username')===>这就是你写的参数是driver的一个方法
# lambda 参数列表:返回值
# def myfunc(driver):
#     return driver.find_element('id','ls_username')
WebDriverWait(driver,5,0.5).until(lambda x:x.find_element('id','ls_username')).send_keys('admin')

# 思考: send_keys 出不来？



