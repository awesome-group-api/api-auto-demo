# Author:  wuxianfeng
# Company: songqin
# File:    第7个脚本_认识显示等待的方法2.py
# Date:    22-5-9
# Time:    下午 9:30
print()
'''
知识点: url_to_be
1. 传入什么参数 :   url期望的
2. 返回值是什么 :    True/False
3. 这个方法的意义是什么 ： 判断浏览器当前的url是否变成指定的url
'''
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
from time import sleep,ctime
driver = webdriver.Chrome()
driver.maximize_window()
driver.get('http://106.14.1.150:8090/forum.php')
expected_url = 'http://106.14.1.150:8090/member.php?mod=registe'
driver.find_element('link text','立即注册').click()
if WebDriverWait(driver,25,0.5).until(EC.url_to_be(expected_url)):
    print(F'url变成了{expected_url}')