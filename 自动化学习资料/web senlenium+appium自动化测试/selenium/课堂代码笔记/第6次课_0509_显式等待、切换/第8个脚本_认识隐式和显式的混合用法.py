# Author:  wuxianfeng
# Company: songqin
# File:    第8个脚本_认识隐式和显式的混合用法.py
# Date:    22-5-9
# Time:    下午 9:37
print()
'''
知识点:  显式和隐式一起用的弊端
1. 官网
警告: 不要混合使用隐式和显式等待。这样做会导致不可预测的等待时间。例如，将隐式等待设置为10秒，将显式等待设置为15秒，可能会导致在20秒后发生超时。
Warning: Do not mix implicit and explicit waits. Doing so can cause unpredictable wait times. For example, setting an implicit wait of 10 seconds and an explicit wait of 15 seconds could cause a timeout to occur after 20 seconds.
'''
from selenium import webdriver
from time import sleep,ctime
driver = webdriver.Chrome()
driver.implicitly_wait(10)
driver.maximize_window()
driver.get('http://106.14.1.150:8090/forum.php')
# driver.find_element('id','ls_username').send_keys('admin')
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
ele_username_locator = 'id','ls_usernam'
print(ctime())
try:
    WebDriverWait(driver,15,1).until(EC.visibility_of_element_located(ele_username_locator)).send_keys('admin')
except:
    pass
finally:
    print(ctime())