# Author:  wuxianfeng
# Company: songqin
# File:    第4个脚本_三个is属性.py
# Date:    22-5-6
# Time:    下午 9:31
print()
'''
| is_displayed                         | 是否显示，跟hidden属性相关(是否跟其他相关暂不可知)   |
| is_enabled                           | 是否使能，跟disable属性相关(是否跟其他相关暂不可知)  |
| is_selected                          | 是否选中，跟当前选择状态有关(是否跟其他相关暂不可知) |

'''
from selenium import webdriver
from time import sleep
driver = webdriver.Chrome('/usr/local/bin/chromeDriver')
driver.get(r'D:\pythonProject\AutoTest\AutoSelenium0425\第4次课_0506_option、select、键盘、鼠标操作\is.html')
print('p元素是否显示:',driver.find_element('css selector', 'p').is_displayed())  #False  目前是hidden隐藏了
print('单选按钮是否勾选:',driver.find_element('css selector', '#choose').is_selected())  #False
driver.find_element('css selector', '#choose').click()  #点击
print('单选按钮是否勾选:',driver.find_element('css selector', '#choose').is_selected())  #True

print('按钮是否使能:',driver.find_element('css selector', 'button').is_enabled())  #False


