# Author:  wuxianfeng
# Company: songqin
# File:    第1个脚本_认识webelement操作.py
# Date:    22-5-6
# Time:    下午 8:08
print()
'''
知识点:

'''
from selenium import webdriver
from time import sleep
driver = webdriver.Chrome('/usr/local/bin/chromeDriver')
driver.maximize_window()
driver.get('http://106.14.1.150:8090/forum.php')
test_flag = 4
if  test_flag == 4: #submit  提交
    driver.find_element('css selector','#ls_username').send_keys('admin')
    ele_password = driver.find_element('css selector','#ls_password')
    ele_password.send_keys('123456')
    ele_password.submit()   #不需要点击登录亦可完成登录
    # 正常操作就是点击登录按钮
    #ele_login = driver.find_element('css selector', '.pn.vm').click()
if  test_flag == 3:  #位置相关的属性
    ele_login = driver.find_element('css selector','.pn.vm')
    print('大小是:',ele_login.size)  #字典： {'height': 23, 'width': 75}
    print('位置是:',ele_login.location) # {'x': 1378, 'y': 80}  #坐标  左上角是原点(0,0) # X往右增大，y往下增大
    print('矩形是:',ele_login.rect)  #size+location  {'height': 23, 'width': 75, 'x': 1377.8875732421875, 'y': 79.5}
if test_flag ==2: #获取不存在的属性,如果不存在是不会报错的，是空字符串
    ele_login = driver.find_element('css selector','.pn.vm')
    print(type(ele_login.get_attribute('id')))  #str
if test_flag ==1:  # 元素上面找元素
    ele_login = driver.find_element('css selector','.pn.vm')
    print(ele_login.find_element('tag name', 'em').text)





