# Author:  wuxianfeng
# Company: songqin
# File:    第9个脚本_认识鼠标操作.py
# Date:    22-5-6
# Time:    下午 10:12


print()
'''
知识点:
1. 对于鼠标悬浮方可弹出的界面，用F12好像有点难以定位
   -- 解决方法 ： 
    1.1 右键检查
    1.2 CTRL+SHIFT+C
    1.3 firefox移动到快捷导航直接可以定位元素
2. 题目
  2.1 移动鼠标到快捷导航
  2.2 点击登录
3. ActionChains的用法
    3.1 用法一
    ActionChains(driver).动作1.动作2.perform()
    perform一定要有，表示最终的执行，动作可以有1个或多个
    3.2 用法二
    myaction = ActionChains(driver)
    myaction.动作1
    myaction.动作2
    myaction.perform
4. 动作很关键，要理解传参
'''
from selenium import webdriver
from selenium.webdriver import ActionChains
from time import sleep
driver = webdriver.Chrome('/usr/local/bin/chromeDriver')
driver.maximize_window()
driver.get('http://106.14.1.150:8090/forum.php')
# 移动鼠标到快捷导航
ele_navi = driver.find_element('css selector','a#qmenu')
ActionChains(driver).move_to_element(ele_navi).perform()
sleep(2)
driver.find_element('css selector','.xi2>strong').click()



