# Author:  wuxianfeng
# Company: songqin
# File:    第7个脚本_认识option.py
# Date:    22-5-6
# Time:    下午 9:48
print()
'''
知识点:
1. option的内容非常多
2. 参考网址：https://www.cnblogs.com/yikemogutou/p/12624113.html 很多废弃了
'''
from selenium import webdriver
from time import sleep
test_flag = 3
if test_flag ==3: #免登录   #如果你要去爬取淘宝、京东等网站的用户个人信息
    # chrome://version
    # 个人资料路径:C:\Users\songqin008\AppData\Local\Google\Chrome\User Data\Default
    # 每个人都不一样的，要用自己的
    # 尾部的Default要去掉
    my_data_dir = r'C:\Users\songqin008\AppData\Local\Google\Chrome\User Data'
    myoption = webdriver.ChromeOptions('/usr/local/bin/chromeDriver')
    myoption.add_argument(f'--user-data-dir={my_data_dir}')
    driver = webdriver.Chrome('/usr/local/bin/chromeDriver',options=myoption)
    driver.get('https://processon.com/')  #selenium打开网址的时候默认是裸奔不带任何用户数据的
    #selenium.common.exceptions.InvalidArgumentException:
    # Message: invalid argument: user data directory is already in use, please specify a unique value for --user-data-dir argument, or don't use --user-data-dir

if test_flag == 2:  #伪造user-agent
    from fake_useragent import UserAgent
    ua = UserAgent()
    print(ua.chrome)
    myoption = webdriver.ChromeOptions()
    myoption.add_argument(f'user-agent={ua.chrome}')
    driver = webdriver.Chrome(options=myoption)
if test_flag ==1:  #无痕模式
    myoption = webdriver.ChromeOptions()
    myoption.add_argument('--incognito')
    driver = webdriver.Chrome(options=myoption)


