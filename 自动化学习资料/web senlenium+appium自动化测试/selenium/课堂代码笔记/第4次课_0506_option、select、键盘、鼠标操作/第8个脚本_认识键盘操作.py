# Author:  wuxianfeng
# Company: songqin
# File:    第8个脚本_认识键盘操作.py
# Date:    22-5-6
# Time:    下午 10:04
from selenium.webdriver import Keys

print()
'''
知识点:  
基于论坛的小操作
1. 输入用户名admin
2. 复制用户名（ctrl+a）
   ctrl+c
3. 在搜索框中黏贴你复制的内容 ctrl+v
4. 删除min
5. 点击搜索
----
知识点
1. 用send_keys发送组合键
2. send_keys(Keys.CONTROL,'a')
'''
from selenium import webdriver
from time import sleep
driver = webdriver.Chrome('/usr/local/bin/chromeDriver')
driver.maximize_window()
driver.get('http://106.14.1.150:8090/forum.php')
ele_username = driver.find_element('id','ls_username')
ele_username.send_keys('admin')
sleep(2)
ele_username.send_keys(Keys.CONTROL,'a')
sleep(2)
ele_username.send_keys(Keys.CONTROL,'C')
sleep(2)
ele_search = driver.find_element('id','scbar_txt')
ele_search.send_keys(u'\ue009','v')
sleep(2)
ele_search.send_keys(Keys.BACKSPACE*3)  #删除3个字符串


