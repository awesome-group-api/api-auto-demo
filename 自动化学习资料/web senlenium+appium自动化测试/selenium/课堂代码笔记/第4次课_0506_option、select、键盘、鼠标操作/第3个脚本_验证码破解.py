# Author:  wuxianfeng
# Company: songqin
# File:    第3个脚本_验证码破解.py
# Date:    22-5-6
# Time:    下午 8:36
print()
'''
知识点:  
1. 验证码分很多种（图形验证码、涉及AI、短信、语音、视频、头像）
2. 图形验证码
   -- 简单字符  √   常用ocr技术来破解，光学字符识别 
      --这种识别都有失败的可能（pytesseract/PaddleOCR）
      --涉及AI都有训练的动作，跟你的素材库有关系
   -- 算术     √
   -- 文字顺序
   -- 成语
   -- 滑块
   -- 图形方向
2. screenshot_as_png  属性加了个property的装饰，返回是bytes对象
3. retry  ===>python的重试的库
'''
from selenium import webdriver
driver = webdriver.Chrome('/usr/local/bin/chromeDriver')
driver.get('http://121.41.14.39:8088/index.html#/')
driver.find_element('id','username').send_keys('sq1')
driver.find_element('id','password').send_keys('123')
ele_pic = driver.find_element('xpath','//img')
with open('code.png','wb') as f:
    f.write(ele_pic.screenshot_as_png)
import ddddocr  # 你需要安装，也就是这个库暂不支持python3.10
ocr = ddddocr.DdddOcr()  #实例化
pic_text = ocr.classification(ele_pic.screenshot_as_png)
print(pic_text)
driver.find_element('id','code').send_keys(pic_text)
driver.find_element('id','submitButton').click()


