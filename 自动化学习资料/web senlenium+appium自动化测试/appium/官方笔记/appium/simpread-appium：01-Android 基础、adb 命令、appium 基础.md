> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b5879e220ea017a2d2d56c97b81)

> appium 官网文档

*   **appium 官网文档**
    

*   文档有不少没有及时更新的地方
    
*   [https://appium.io/docs/cn/about-appium/intro/](https://appium.io/docs/cn/about-appium/intro/)   中文版
    
*   [https://appium.io/docs/en/about-appium/intro/](https://appium.io/docs/en/about-appium/intro/)   英文版
    

> 第一天: Android 基础、adb 命令、appium 基础

Android 简介
----------

*   **发展历史**
    

1.  03 年 10 月 Andy Rubin 等人创建 Android 公司
    
2.  05 年 8 月 google 收购
    
3.  07 年 11 月谷歌对外宣布 Android 操作系统，成立 Open Handset Alliance
    

1.  Android 本义是 “机器人”，来源于科幻小说《未来夏娃》，这个 logo 由 Ascender 公司在 2010 年设计
    
2.  Android 虽然是运行在 linux kernel 上，但并非是 GNU/Linux，它为了达到商用的目的，必须要移除 GPL 约束的部分
    

5.  08 年 9 月 Android 1.0 问世
    

*   **历史版本**
    
    <table><thead><tr cid="n39" mdtype="table_row"><th>系统版本号</th><th>API</th><th>发布时间</th></tr></thead><tbody><tr cid="n43" mdtype="table_row"><td>Android 1.5：Cupcake：纸杯蛋糕</td><td>3</td><td>2009.4.30</td></tr><tr cid="n47" mdtype="table_row"><td>Android 1.6：Donut：甜甜圈</td><td>4</td><td>2009.9.15</td></tr><tr cid="n51" mdtype="table_row"><td>Android 2.0/2.0.1/2.1：Eclair：松饼</td><td>2005/6/7</td><td>2009.10.26</td></tr><tr cid="n55" mdtype="table_row"><td>Android 2.2/2.2.1：Froyo：冻酸奶</td><td>8</td><td>2010.5.20</td></tr><tr cid="n59" mdtype="table_row"><td>Android 2.3：Gingerbread：姜饼</td><td>9</td><td>2010.12.7</td></tr><tr cid="n63" mdtype="table_row"><td>Android 3.0：Honeycomb：蜂巢</td><td>11</td><td>2011.2.2</td></tr><tr cid="n67" mdtype="table_row"><td>Android 3.1：Honeycomb：蜂巢</td><td>12</td><td>2011.5.11</td></tr><tr cid="n71" mdtype="table_row"><td>Android 3.2：Honeycomb：蜂巢</td><td>13</td><td>2011.7.13</td></tr><tr cid="n75" mdtype="table_row"><td>Android 4.0：Ice Cream Sandwich：冰激凌三文治</td><td>14</td><td>2011.10.19</td></tr><tr cid="n79" mdtype="table_row"><td>Android 4.1：Jelly Bean：果冻豆</td><td>16</td><td>2012.6.28</td></tr><tr cid="n83" mdtype="table_row"><td>Android 4.2：Jelly Bean：果冻豆</td><td>17</td><td>2012.10.30</td></tr><tr cid="n87" mdtype="table_row"><td>Android 4.3：Jelly Bean：果冻豆</td><td>18</td><td>2013.7.25</td></tr><tr cid="n91" mdtype="table_row"><td>Android 4.4：KitKat：奇巧巧克力</td><td>19</td><td>2013.11.01</td></tr><tr cid="n95" mdtype="table_row"><td>Android 5.0：Lollipop：棒棒糖</td><td>21/22</td><td>2014.10.16</td></tr><tr cid="n99" mdtype="table_row"><td>Android 6.0：Marshmallow 棉花糖</td><td>23</td><td>2015.9.30</td></tr><tr cid="n103" mdtype="table_row"><td>Android 7.0：Nougat</td><td>24/25</td><td>2016.8.22</td></tr><tr cid="n107" mdtype="table_row"><td>Android 8.0：Oreo</td><td>26/27</td><td>2017.8.22</td></tr><tr cid="n111" mdtype="table_row"><td>Android 9.0：Pie</td><td>28</td><td>2018.5.9</td></tr><tr cid="n115" mdtype="table_row"><td>Android 10</td><td>29</td><td>2019.9.3</td></tr><tr cid="n119" mdtype="table_row"><td>Android 11</td><td>30</td><td>2020.9.9</td></tr><tr cid="n123" mdtype="table_row"><td>Android 12</td><td>31</td><td>2021.5.9</td></tr></tbody></table>

> 从 Android10 开始不再使用字母（Q/R/S 等，虽然有些地方还是这么引用）.Android Q 不叫 Q，正式命名为 Android 10。谷歌改变了 Android OS 的命名习惯，不会再按照基于美味零食或甜点的字母顺序命名，而是转换为版本号.
> 
> API 级别是一个用于唯一标识 API 框架版本的整数，由某个版本的 Android 平台提供

*   **四大组件**
    

*   **活动 activity**
    

*   在 Android 中 activity 是指与用户交互的界面，从你打开一个 Android 应用所见到的第一个页面就是一个 activity。但是同一个 activity 可能会对应多个（你所看到的）页面。
    
*   Activity 是与用户交互的入口点。它表示拥有界面的单个屏幕。例如，电子邮件应用可能有一个显示新电子邮件列表的 Activity、一个用于撰写电子邮件的 Activity 以及一个用于阅读电子邮件的 Activity。
    

*   服务 service Android 的服务是在后台运行的，没有界面部分，一旦启动大多会在后台进行，直到手机关机，服务 service 承担着大部分的数据处理工作
    
*   广播接收器 broadcastreceiver 主要用于接收系统或 app 发送的广播事件
    
*   内容提供者 content provider Android 平台提供了内容提供者，使一个程序的指定数据集提供给其他程序，可以理解为是一个特殊 的数据库，主要用来存取数据
    

*   **关于 APK**
    

*   AndroidPackage 的缩写，apk 其实是 zip 格式
    
*   目录结构
    

*   assets/：包含应用可以使用 AssetManager 对象检索的应用资源。
    
*   lib/：包含特定于处理器软件层的编译代码。该目录包含了每种平台的子目录，像 armeabi，armeabi-v7a， arm64-v8a，x86，x86_64，和 mips。
    
*   META-INF/：包含. SF 和 .RSA 签名文件以及 MANIFEST.MF 清单文件。
    
*   res/：该目录包含了工程中 res 目录下除 values 以外的所有内容，一般包括各种 layout 文件，drawable，但 layout 文件内容是以二进制 xml 的方式进行保存的，而所有的 drawable 图像资源都是未经压缩过的原始文件。因为 drawable 中的 jpg，png 本身就是压缩文件。
    
*   classes.dex：包含以 Dalvik / ART 虚拟机可理解的 DEX 文件格式编译的类。该文件是真正的 Java class 文件，dex 文件时 google 发明的，这种格式的作用和 Java class 文件相同。所不同的是，为了能够快速读取 class 文件并让 class 在解释时占用更少的内存，gogole 对标准的 class 文件进行了重新的格式优化，这就是 dex。想从 dex 文件看到源码，我们可以进行反编译。
    
*   resources.arsc：是所有的资源 id 的数据集合。该文件同样是二进制格式的文件，aapt 对资源进行编译时，会为每一个资源分配唯一的 id 值，程序在执行时会根据这些 id 值读取特定的资源，而 resouces.arsc 文件正式包含了所有的 id 值的数据集合。在该文件中，如果某个 id 对应的是 string，那么该文件会直接包含该值，如果 id 对应的资源是某个 layout 或者 drawable 资源，那么该文件会存入对应资源的路径。
    

*   **清单文件 AndroidManifest.xml**
    

*   该文件列出应用程序的名称，版本，访问权限和引用的库文件。该文件使用 Android 的二进制 XML 格式。
    
*   要素
    
    <table><thead><tr cid="n175" mdtype="table_row"><th>元素</th><th>说明</th></tr></thead><tbody><tr cid="n178" mdtype="table_row"><td><action></action></td><td>向 Intent 过滤器添加操作。</td></tr><tr cid="n181" mdtype="table_row"><td><activity></activity></td><td>声明 Activity 组件。</td></tr><tr cid="n184" mdtype="table_row"><td><activity-alias></activity-alias></td><td>声明 Activity 的别名。</td></tr><tr cid="n187" mdtype="table_row"><td><application></application></td><td>应用的声明。</td></tr><tr cid="n190" mdtype="table_row"><td><category></category></td><td>向 Intent 过滤器添加类别名称。</td></tr><tr cid="n193" mdtype="table_row"><td><compatible-screens></compatible-screens></td><td>指定与应用兼容的每个屏幕配置。</td></tr><tr cid="n196" mdtype="table_row"><td><data></data></td><td>向 Intent 过滤器添加数据规范。</td></tr><tr cid="n199" mdtype="table_row"><td><grant-uri-permission></grant-uri-permission></td><td>指定父级内容提供程序有权访问的应用数据的子集。</td></tr><tr cid="n202" mdtype="table_row"><td></td><td>声明支持您监控应用与系统进行交互的 <code>Instrumentation</code> 类。</td></tr><tr cid="n205" mdtype="table_row"><td><intent-filter></intent-filter></td><td>指定 Activity、服务或广播接收器可以响应的 Intent 类型。</td></tr><tr cid="n208" mdtype="table_row"><td><manifest></manifest></td><td>AndroidManifest.xml 文件的根元素。</td></tr><tr cid="n211" mdtype="table_row"><td><meta-data></meta-data></td><td>可以提供给父级组件的其他任意数据项的名称 - 值对。</td></tr><tr cid="n214" mdtype="table_row"><td><path-permission></path-permission></td><td>定义内容提供程序中特定数据子集的路径和所需权限。</td></tr><tr cid="n217" mdtype="table_row"><td><permission></permission></td><td>声明安全权限，可用于限制对此应用或其他应用的特定组件或功能的访问。</td></tr><tr cid="n220" mdtype="table_row"><td><permission-group></permission-group></td><td>为相关权限的逻辑分组声明名称。</td></tr><tr cid="n223" mdtype="table_row"><td><permission-tree></permission-tree></td><td>声明权限树的基本名称。</td></tr><tr cid="n226" mdtype="table_row"><td><provider></provider></td><td>声明内容提供程序组件。</td></tr><tr cid="n229" mdtype="table_row"><td><receiver></receiver></td><td>声明广播接收器组件。</td></tr><tr cid="n232" mdtype="table_row"><td><service></service></td><td>声明服务组件。</td></tr><tr cid="n235" mdtype="table_row"><td><supports-gl-texture></supports-gl-texture></td><td>声明应用支持的一种 GL 纹理压缩格式。</td></tr><tr cid="n238" mdtype="table_row"><td><supports-screens></supports-screens></td><td>声明应用支持的屏幕尺寸，并为大于此尺寸的屏幕启用屏幕兼容模式。</td></tr><tr cid="n241" mdtype="table_row"><td><uses-configuration></uses-configuration></td><td>指明应用要求的特定输入功能。</td></tr><tr cid="n244" mdtype="table_row"><td><uses-feature></uses-feature></td><td>声明应用使用的单个硬件或软件功能。</td></tr><tr cid="n247" mdtype="table_row"><td><uses-library></uses-library></td><td>指定应用必须链接到的共享库。</td></tr><tr cid="n250" mdtype="table_row"><td><uses-permission></uses-permission></td><td>指定为使应用正常运行，用户必须授予的系统权限。</td></tr><tr cid="n253" mdtype="table_row"><td><uses-permission-sdk-23></uses-permission-sdk-23></td><td>指明应用需要特定权限，但仅当应用在运行 Android 6.0（API 级别 23）或更高版本的设备上安装时才需要。</td></tr><tr cid="n256" mdtype="table_row"><td><uses-sdk></uses-sdk></td><td>您可以通过整数形式的 API 级别，表示应用与一个或多个版本的 Android 平台的兼容性。</td></tr></tbody></table>

### 三种 app 的类型

1.  **原生应用 native app**：指那些用 iOS、 Android 或者 Windows SDKs 编写的应用
    
2.  **移动 Web app**：用移动端浏览器访问的应用（ Appium 支持 iOS 上的 Safari 、Chrome 和 Android 上的内置浏览器
    
3.  **混合应用 hybrid app**：带有一个「webview」的包装器——用来和 Web 内容交互的原生控件。类似于 Apache Cordova] 项目，创建一个混合应用使得用 Web 技术开发然后打包进原生包装器创建一个混合应用变得容易了。
    

1.  H5 在特定的场合下可以认为等同于混合应用，只不过它特指混合 App 的前端部分。
    

#### 原生应用 native app

*   **原生** **App** **是专门为特定手机平台开发的应用程序** ，无法在其他平台运行。一个手机软件如果要同时支
    
    持苹果手机和安卓手机，就需要为它们**各写**一个原生 App。
    
*   历史上，原生 App 最早出现，跟智能手机系统一起诞生。2007 年 6 月 iPhone 诞生，2008 年 9 月安卓诞
    
    生，就同时发布了自家平台的原生 App 开发方法。
    
*   原生 App 使用与手机操作系统相同的语言。iOS 的原生 App 使用 Objective-C 语言或 Swift 语言，安卓
    
    使用 Java 语言或 Kotlin 语言。由于跟底层系统的语言和技术模型一致，所以原生 App 的性能和用户体
    
    验都很好。
    

<table><thead><tr cid="n284" mdtype="table_row"><th>优点</th><th>缺点</th></tr></thead><tbody><tr cid="n287" mdtype="table_row"><td>较好的性能和体验</td><td>成本高，每个手机平台都要建立一个独立的开发团队</td></tr><tr cid="n290" mdtype="table_row"><td>可以使用系统的所有硬件和软件 API，比如 GPS、摄像头、麦克风、加速计、通知推送等等，能充分发挥系统的潜力</td><td>原生 App 使用底层操作系统的语言，都是很重的编译型语言，开发和调试成本相对较</td></tr><tr cid="n293" mdtype="table_row"><td><br></td><td>原生 App 必须下载安装才能使用 (C/S)，只要升级版本，就必须重新下载安装。用户往往不愿意更新版本，厂商被迫不得不长期支持很久以前的旧版本。</td></tr></tbody></table>

#### web 应用 web app

*   Web App 是使用网页做的应用程序，必须在浏览器中使用。
    
*   Web App 主要使用网页技术，即 HTML、JavaScript 和 CSS。
    
*   2008 年，w3c 组织发布了 HTML 第 5 版，简称 HTML 5，该版本大大增强了网页的功能，使得网页可以当作应用程序使用，而不仅仅是展示文字和图片，这就是 Web App 的由来。
    

<table><thead><tr cid="n306" mdtype="table_row"><th>优点</th><th>缺点</th></tr></thead><tbody><tr cid="n309" mdtype="table_row"><td>不需要下载安装，打开浏览器就能使用（B/S），而且总是使用最新版本</td><td>首先，浏览器提供的 API（即 Web API）很有限（目前只有相机、GPS、 电池等少数几个），大部分系统硬件都不能通过网页访问，也无法直接读取硬盘文件。</td></tr><tr cid="n312" mdtype="table_row"><td>对于开发者来说，Web App 写起来比较快，调试容易，不需要应用商店的批准就能发布</td><td>网页通过浏览器渲染，性能不如原生 App，不适合做性能要求较高的页面</td></tr><tr cid="n315" mdtype="table_row"><td><br></td><td>Web App 需要打开浏览器才能使用，这意味着，用户必须记住如何导航到它，要么直接输入网址，要么翻找书签。这使得进入 Web App，远不如原生 App 方便。</td></tr></tbody></table>

#### 混合应用  hybrid app

*   顾名思义就是原生 App 与 Web App 的结合。它的壳是原生 App，但是里面放的是网页。
    
*   混合 App 里面隐藏了一个浏览器，用户看到的实际上是这个隐藏浏览器渲染出来的网页。
    
*   混合 App 的原生外壳称为 "容器"，内部隐藏的浏览器，通常使用系统提供的网页渲染控件（即 WebView 控件），也可以自己内置一个浏览器内核。结构上，混合 App 从上到下分成三层：HTML5 网页层、网页引擎层（本质上是一个隔离的浏览器实例）、容器层。
    

<table><thead><tr cid="n328" mdtype="table_row"><th>优点</th><th>缺点</th></tr></thead><tbody><tr cid="n331" mdtype="table_row"><td>跨平台</td><td>以性能比较欠缺</td></tr><tr cid="n334" mdtype="table_row"><td>灵活性</td><td>体验不如纯的原生 App</td></tr><tr cid="n337" mdtype="table_row"><td>开发方便</td><td><br></td></tr></tbody></table>

#### 微信小程序

*   小程序，可以看作是针对特定容器的 H5 开发。微信本身是一个容器，开放自己的接口 （JSbridge），外部开发者使用规定的语法，编写页面，容器可以动态加载这些页面
    

ADB 简介
------

> [https://adbinstaller.com/](https://adbinstaller.com/)  单文件下载位置

*   adb 是 Android Debug Bridge 的缩写，Android 调试桥接，是一个 c/s 架构的命令行工具。
    
*   可以通过 Android studio 获取或者一些模拟器（夜神、海马玩、MuMu）都会默认自带该工具
    

### adb 组成

*   **客户端**：用于发送命令。客户端在开发计算机上运行。您可以通过发出 adb 命令来从命令行终端调用客户端。`客户机输入的地方，比如cmd。`
    
*   **守护进程 (adbd)**：在设备上运行命令。守护进程（daemon）在每个设备上作为后台进程运行。`在手机上。`
    
*   **服务器**：管理客户端和守护进程之间的通信。服务器在开发机器上作为后台进程运行。`在客户机上的一个进程adb.exe或者nox_adb.exe。`
    

### adb 原理

1.  当启动 adb client 的时候，client 会首先检查 adb server 是否已经在运行，如果没有启动，则会先启动 adb server（所以 adb start-server 一般我们不用）
    
2.  server 启动后会绑定到本地的 5037 端口，随后会监听来自 adb client 发来的所有命令，adb client 都是通过 5037 端口来和 server 通讯的
    
3.  server 会和手机设备建立连接，server 会在端口号 5555~5585 之间的奇数端口里查找手机设备，当 sever 找到手机上的 adb daemon 的时候 server 会和手机端口建立连接
    
4.  server 和手机建立连接后，你就可以使用 adb 命令去操作和控制手机。server 负责管理与手机设备的连接，处理来自各个 adb client 的命令。
    
    所有 client 默认通过端口号 5037 进行与 server 通信，而 server 创建 local socket 和 remote socket，前者用于和 client 通信，后者用于和远端 adb daemon 通信。
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20211029/1635501706466067114.png)

ADB 命令
------

> *   adb 官网 [https://developer.android.google.cn/studio/command-line/adb](https://developer.android.google.cn/studio/command-line/adb)
>     
> *   [https://github.com/mzlogin/awesome-adb](https://github.com/mzlogin/awesome-adb) github 上的 awesome adb
>     

*   前提能与调试设备（模拟器）通讯
    

### 全局选项

*   -s 选项用于连接指定序列号的设备
    
    ```
    -s SERIAL  use device with given serial (overrides $ANDROID_SERIAL)
    
    ```
    
*   示例
    
    ```
    C:\Users\Tanya>adb  shell wm size  #一个设备连接的时候执行命令没问题
    Physical size: 768x1280
    
    C:\Users\Tanya>adb connect 127.0.0.1:62001  #连接设备
    connected to 127.0.0.1:62001
    
    C:\Users\Tanya>adb  shell wm size   #多个设备连接的时候执行命令有问题
    error: more than one device/emulator
    
    C:\Users\Tanya>adb devices  #从这里看多个设备连接了
    List of devices attached
    127.0.0.1:62001 device
    emulator-5554   device
    
    ```
    

*   从上面的例子可以看出来
    
*   当一个设备连接的时候我们执行 adb 命令是没问题的
    
*   但是当我们多个设备连接的时候需要用 - s 等参数来指定执行命令的设备是哪个，否则不知道你的 adb 命令发往哪个设备
    

### adb  help  查看帮助

### adb connect  连接到设备

*   示例
    
    ```
      D:\work\\share\test>adb devices
      List of devices attached
    
      D:\work\\share\test>adb connect 127.0.0.1:62001
      connected to 127.0.0.1:62001
    
      D:\work\share\test>adb devices
      List of devices attached
      127.0.0.1:62001 device
    
    ```
    

### adb  devices  查看设备

*   示例
    
    ```
    CMD>adb devices
    List of devices attached
    cf264b8f   device
    emulator-5554  device
    10.129.164.6:5555 device
    
    ```
    
*   说明
    

*   格式是：序列号（serialNumber）   设备状态（state）
    
*   state 一般有以下几种
    

*   `offline` —— 表示设备未连接成功或无响应。
    
*   `device` —— 设备已连接。注意这个状态并不能标识 Android 系统已经完全启动和可操作，在设备启动过程中设备实例就可连接到 adb，但启动完毕后系统才处于可操作状态。
    
*   `no device` —— 没有设备 / 模拟器连接
    

*   从 `emulator-5554` 这个名字可以看出它是一个 Android 模拟器，而 `10.129.164.6:5555` 这种形为 `:` 的 serialNumber 一般是无线连接的设备或 Genymotion 等第三方 Android 模拟器
    

### adb install/adb uninstall 安装与卸载

*   示例
    
    ```
    D:\work\share>adb install com.kejia.mine.apk
    com.kejia.mine.apk: 1 file pushed. 6.3 MB/s (698179 bytes in 0.105s)
            pkg: /data/local/tmp/com.kejia.mine.apk
    Success
    
    D:\work\吴贤峰的课件\\share>adb uninstall com.kejia.mine
    Success
    
    ```
    
*   安装的原理
    

*   push apk 文件到 /data/local/tmp。
    
*   调用 pm install 安装。
    
*   删除 /data/local/tmp 下的对应 apk 文件。
    

### adb push/adb pull 上传和下载

**上传文件**

*   语法
    
    ```
    adb push 本地文件 远端文件
    
    ```
    
*   示例
    
    ```
    D:\work\share\test>adb push helloword1.txt /
    adb: error: failed to copy 'helloword1.txt' to '/helloword1.txt': remote Read-on
    ly file system
    helloword1.txt: 0 files pushed. 0.0 MB/s (10 bytes in 0.005s)
    - 这是一个典型的失败，远端的文件系统是只读(remote Read-only file system)
    
    - 可以对远端的系统重新挂载解决，但这也是危险的。
    D:\work\share\test>adb shell mount -o rw,remount /
    D:\work\share\test>adb push helloword1.txt /
    helloword1.txt: 1 file pushed. 0.0 MB/s (10 bytes in 0.002s)
    
    
    
    ```
    

*   但手机上有些目录我们是可以传输文件的。
    

```
  D:\work\share\test>adb push helloword1.txt /sdcard/
  helloword1.txt: 1 file pushed. 0.0 MB/s (10 bytes in 0.004s)

  D:\work\share\test>adb push helloword1.txt /data
  helloword1.txt: 1 file pushed. 0.0 MB/s (10 bytes in 0.004s)

  D:\work\share\test>adb push helloword1.txt /data/local
  helloword1.txt: 1 file pushed. 0.0 MB/s (10 bytes in 0.002s)

  D:\work\share\test>adb push helloword1.txt /data/local/tmp
  helloword1.txt: 1 file pushed. 0.0 MB/s (10 bytes in 0.002s)

```

*   示例
    

*   可以改名
    
*   可以拷贝目录
    

```
  D:\work\test>adb push helloworld2.txt /data/0303.txt
  helloworld2.txt: 1 file pushed. 0.0 MB/s (10 bytes in 0.002s)

  D:\work\test>adb push dir /data/
  dir\: 1 file pushed. 0.0 MB/s (10 bytes in 0.004s)

```

**下载文件**

*   语法
    
    ```
    adb pull 设备文件  电脑文件
    
    ```
    
*   示例
    

*   可以用. 来表示当前路径
    
*   可以改名
    
*   但如果写 D:\ 这样的格式是不行的，可以改为.（当前目录）
    
*   也可以拷贝目录
    

*   示例
    
    ```
     D:\work\\share\test>adb pull /data/helloword1.txt .
      /data/helloword1.txt: 1 file pulled. 0.0 MB/s (10 bytes in 0.004s)
    
      D:\work\\share\test>adb pull /data/helloword1.txt hel
      loworld2.txt
      /data/helloword1.txt: 1 file pulled. 0.0 MB/s (10 bytes in 0.004s)
    
      D:\work\\share\test>adb pull /data/helloword1.txt d:\
    
      adb: error: cannot create file/directory 'd:\': No such file or directory
    
      D:\work\share\test>adb pull /data/ .
      adb: warning: skipping special file '/data/user/0/com.ss.android.ugc.aweme/files
      /socket_pipe' (mode = 0o140700)
      adb: warning: skipping special file '/data/system/ndebugsocket' (mode = 0o140700
      )
      adb: warning: skipping special file '/data/data/com.ss.android.ugc.aweme/files/s
      ocket_pipe' (mode = 0o140700)
      adb: warning: skipping special file '/data/misc/wifi/sockets/wpa_ctrl_1704-1' (m
      ode = 0o140660)
      adb: warning: skipping special file '/data/misc/wifi/sockets/wpa_ctrl_1704-2' (m
      ode = 0o140660)
      adb: error: failed to copy '/data/bugreports' to '.\data\bugreports': remote No
      such file or directory
    
    ```
    
*   有的时候我们会遇到一些文件无法访问，必须要 root 才可以，那可以先在设备上用 root 拷贝到指定的目录，然后再通过 adb pull 下载
    

### adb shell 登录到设备

*   注意两种执行命令的方法
    
    ```
    #1 . 登录后执行
    CMD>adb  shell 
    #ps
    
    #2.  一步执行
    CMD>adb  shell ps
    
    ```
    

### adb shell  am  活动管理

*   启动应用
    
    ```
    adb shell am start  INTENT
    INTENT的格式是   packagename/activityname
    
    adb shell am start  com.kejia.mine/com.kejia.mine.app.Mine  #
    adb shell am start  com.kejia.mine/.app.Mine   #简写
    
    ```
    
*   停止应用
    
    ```
    adb  shell am froce-stop com.kejia.mine
    
    ```
    

### adb  shell  pm 包管理

*   列出所有的安装包
    
    ```
    adb  shell  pm  list packages
    
    ```
    
*   列出系统包
    
    ```
    adb  shell  pm  list packages  -s
    
    ```
    
*   列出第三方包
    
    ```
    adb  shell  pm  list packages  -3
    
    ```
    
*   列出安装包的路径
    
    ```
    adb  shell  pm  path  com.kejia.mine
    
    ```
    
*   在 Android 中安装应用
    
    ```
    adb  shell pm install /path/to/xxx.apk  #注意要用绝对路径
    
    ```
    

### logcat 日志打印

Android 系统的日志分为两部分，底层的 Linux 内核日志输出到 /proc/kmsg，Android 的日志输出到 /dev/log。

`Android log` 输出量**巨大**，特别是**通信**系统的 log，因此，Android 把 log 输出到不同的缓冲区中，目前定义了四个 log 缓冲区：

<table><thead><tr cid="n551" mdtype="table_row"><th>缓冲区</th><th>含义</th></tr></thead><tbody><tr cid="n554" mdtype="table_row"><td>Radio</td><td>输出通信系统的 log</td></tr><tr cid="n557" mdtype="table_row"><td>System</td><td>输出系统组件的 log</td></tr><tr cid="n560" mdtype="table_row"><td>Event</td><td>输出 event 模块的 log</td></tr><tr cid="n563" mdtype="table_row"><td>Main</td><td>所有 java 层的 log 以及不属于上面 3 层的 log</td></tr></tbody></table>

缓冲区主要给系统组件使用，一般的应用不需要关心，应用的 log 都输出到 main 缓冲区中。默认 log 输出（不指定缓冲区的情况下）是输出 System 和 Main 缓冲区的 log。

*   logcat 命令可以带 shell 也可以不带
    
    ```
    adb logcat
    adb shell logcat都是可以的
    
    ```
    

#### 按日志级别过滤

Android 的日志分为如下七个优先级（priority）：

*   V —— Verbose（最低，输出得最多）
    
*   D —— Debug 调试
    
*   I —— Info 提示
    
*   W —— Warning 告警
    
*   E —— Error 错误
    
*   F —— Fatal 致命
    
*   S —— Silent（最高，啥也不输出）
    

按某级别过滤日志则会将该级别及以上的日志输出，示例：

```
adb logcat *:W  #查看w级别及以上的级别的日志

```

#### 指定格式输出

*   语法
    
    ```
    adb logcat -v
    
    ```
    
*   其中 format 可以取值为
    

*   brief   :
    

*   默认格式：/():
    

*   process
    

*   格式：()
    

*   tag
    

*   格式：/:
    

*   thread
    

*   格式:
    

*   raw
    

*   格式:
    

*   time
    

*   格式： /():
    

*   threadtime
    

*   格式： :
    

*   long
    

*   [:/]
    

*   示例
    
    ```
    03-09 10:30:17.575 W/PackageManager( 1741): Unknown permission com.sec.android.a
    pp.twlauncher.settings.READ_SETTINGS in package com.wandoujia.phoenix2
    
    时间  03-09 10:30:17.575
    日志级别：W
    标签（程序）:PackageManager
    进程号：PID1741
    内容正文：Unknown permission com.sec.android.a
    pp.twlauncher.settings.READ_SETTINGS in package com.wandoujia.phoenix2
    
    ```
    

#### 清空日志

```
adb logcat -c

```

#### 组合

*   **查看应用的日志用该方法**
    
*   示例
    
    ```
    adb logcat -v long ActivityManager:I *:S  #显示ActivityManager应用Info级别以上的信息，其他都不输出，显示信息格式是long
    
    
    adb logcat ActivityManager:I MyApp:D *:S  #显示 tag ActivityManager 的 Info 以上级别日志，输出 tag MyApp 的 Debug 以上级别日志，及其它 tag 的 Silent 级别日志（即屏蔽其它 tag 日志）
    
    ```
    

#### 查看帮助

```
D:\work\吴贤峰的课件\\share>adb logcat -h
logcat: invalid option -- h
Unrecognized Option
Usage: logcat [options] [filterspecs]
options include:
  -s              Set default filter to silent.
                  Like specifying filterspec '*:s'
  -f  Log to file. Default to stdout
  -r []   Rotate log every kbytes. (16 if unspecified). Requires -f
  -n     Sets max number of rotated logs to, default 4
  -v    Sets the log print format, whereis one of:

                  brief process tag thread raw time threadtime long

  -c              clear (flush) the entire log and exit
  -d              dump the log and then exit (don't block)
  -t     print only the most recentlines (implies -d)
  -t ''     print most recent lines since specified time (implies -d)
  -T     print only the most recentlines (does not imply -d)
  -T ''     print most recent lines since specified time (not imply -d)
                  count is pure numerical, time is 'MM-DD hh:mm:ss.mmm'
  -g              get the size of the log's ring buffer and exit
  -b    Request alternate ring buffer, 'main', 'system', 'radio',
                  'events', 'crash' or 'all'. Multiple -b parameters are
                  allowed and results are interleaved. The default is
                  -b main -b system -b crash.
  -B              output the log in binary.
  -S              output statistics.
  -G      set size of log ring buffer, may suffix with K or M.
  -p              print prune white and ~black list. Service is specified as
                  UID, UID/PID or /PID. Weighed for quicker pruning if prefix
                  with ~, otherwise weighed for longevity if unadorned. All
                  other pruning activity is oldest first. Special case ~!
                  represents an automatic quicker pruning for the noisiest
                  UID as determined by the current statistics.
  -P '...' set prune white and ~black list, using same format as
                  printed above. Must be quoted.

filterspecs are a series of
  [:priority]

whereis a log component tag (or * for all) and priority is:
  V    Verbose
  D    Debug
  I    Info
  W    Warn
  E    Error
  F    Fatal
  S    Silent (supress all output)

'*' means '*:d' andby itself means:v

If not specified on the commandline, filterspec is set from ANDROID_LOG_TAGS.
If no filterspec is found, filter defaults to '*:I'

If not specified with -v, format is set from ANDROID_PRINTF_LOG
or defaults to "brief"


```

### adb shell dumpsys 查看系统信息

<table><thead><tr cid="n672" mdtype="table_row"><th>命令</th><th>说明</th></tr></thead><tbody><tr cid="n675" mdtype="table_row"><td>dumpsys &nbsp; &nbsp;-l</td><td>查看系统组件</td></tr><tr cid="n678" mdtype="table_row"><td>dumpsys &nbsp; &nbsp; meminfo</td><td>查看系统内存信息</td></tr><tr cid="n681" mdtype="table_row"><td>dumpsys &nbsp; &nbsp; cpuinfo</td><td>查看系统 cpu 信息</td></tr><tr cid="n684" mdtype="table_row"><td>dumpsys &nbsp; &nbsp; activity</td><td>查看系统 activity 信息</td></tr><tr cid="n687" mdtype="table_row"><td>dumpsys activity activities|grep -i &nbsp;com.kejia.mine.app.mine</td><td><br></td></tr><tr cid="n690" mdtype="table_row"><td>dumpsys activity &nbsp;top</td><td><br></td></tr><tr cid="n693" mdtype="table_row"><td>dumpsys &nbsp; &nbsp;gfxinfo</td><td>获取 gpu 绘制信息</td></tr></tbody></table>

*   实用技能：
    

*   dumpsys activity activities|grep -i  com.kejia.mine.app.mine
    
*   dumpsys activity  top
    

### input 模拟按键和输入

*   语法
    
    ```
    root@shamu:/ # input
    Usage: input [<source>] <command> [<arg>...]
    
    The sources are:
          mouse
          keyboard
          joystick
          touchnavigation
          touchpad
          trackball
          stylus
          dpad
          touchscreen
          gamepad
    
    The commands and default sources are:
          text <string> (Default: touchscreen)
          keyevent [--longpress] <key code number or name> ... (Default: keyboard)
          tap <x> <y> (Default: touchscreen)
          swipe <x1> <y1> <x2> <y2> [duration(ms)] (Default: touchscreen)
          press (Default: trackball)
          roll <dx> <dy> (Default: trackball)
    
    ```
    

#### adb shell input text 输入文本

该命令主要是用于向获得焦点的 **EditText** 控件输入内容，

```
adb shell input text  "hello,world"*

```

#### adb shell input keyevent 发送按键

该命令主要是向系统发送一个按键指令，实现模拟用户在键盘上的按键动作:

```
adb shell input keyevent 26
adb shell input keyevent "KEYCODE_POWER"

```

关于键值宏的定义在 KeyEvent.java 文件中有定义，一般都会用默认值，这里也包括黑屏手势的宏定义。

**常见的按键如下**

<table><thead><tr cid="n722" mdtype="table_row"><th>keycode</th><th>含义</th></tr></thead><tbody><tr cid="n725" mdtype="table_row"><td>3</td><td>HOME 键</td></tr><tr cid="n728" mdtype="table_row"><td>4</td><td>返回键</td></tr><tr cid="n731" mdtype="table_row"><td>5</td><td>打开拨号应用</td></tr><tr cid="n734" mdtype="table_row"><td>6</td><td>挂断电话</td></tr><tr><td colspan="1" rowspan="1">7</td><td colspan="1" rowspan="1">&nbsp;数字 0（依次类推，3 对应 10，9 对应 16）</td></tr><tr cid="n737" mdtype="table_row"><td>24</td><td>增加音量</td></tr><tr cid="n740" mdtype="table_row"><td>25</td><td>降低音量</td></tr><tr cid="n743" mdtype="table_row"><td>26</td><td>电源键</td></tr><tr cid="n746" mdtype="table_row"><td>27</td><td>拍照（需要在相机应用里）</td></tr><tr cid="n749" mdtype="table_row"><td>64</td><td>打开浏览器</td></tr><tr cid="n752" mdtype="table_row"><td>82</td><td>菜单键</td></tr><tr cid="n755" mdtype="table_row"><td>85</td><td>播放 / 暂停</td></tr><tr cid="n758" mdtype="table_row"><td>86</td><td>停止播放</td></tr><tr cid="n761" mdtype="table_row"><td>87</td><td>播放下一首</td></tr><tr cid="n764" mdtype="table_row"><td>88</td><td>播放上一首</td></tr><tr cid="n767" mdtype="table_row"><td>122</td><td>移动光标到行首或列表顶部</td></tr><tr cid="n770" mdtype="table_row"><td>123</td><td>移动光标到行末或列表底部</td></tr><tr cid="n773" mdtype="table_row"><td>126</td><td>恢复播放</td></tr><tr cid="n776" mdtype="table_row"><td>127</td><td>暂停播放</td></tr><tr cid="n779" mdtype="table_row"><td>164</td><td>静音</td></tr><tr cid="n782" mdtype="table_row"><td>176</td><td>打开系统设置</td></tr><tr cid="n785" mdtype="table_row"><td>187</td><td>切换应用</td></tr><tr cid="n788" mdtype="table_row"><td>207</td><td>打开联系人</td></tr><tr cid="n791" mdtype="table_row"><td>208</td><td>打开日历</td></tr><tr cid="n794" mdtype="table_row"><td>209</td><td>打开音乐</td></tr><tr cid="n797" mdtype="table_row"><td>210</td><td>打开计算器</td></tr><tr cid="n800" mdtype="table_row"><td>220</td><td>降低屏幕亮度</td></tr><tr cid="n803" mdtype="table_row"><td>221</td><td>提高屏幕亮度</td></tr><tr cid="n806" mdtype="table_row"><td>223</td><td>系统休眠</td></tr><tr cid="n809" mdtype="table_row"><td>224</td><td>点亮屏幕</td></tr><tr cid="n812" mdtype="table_row"><td>231</td><td>打开语音助手</td></tr><tr cid="n815" mdtype="table_row"><td>276</td><td>如果没有 wakelock 则让系统休眠</td></tr></tbody></table>

**完整的按键可以参考: keyevent.java 按键事件**

[https://developer.android.google.cn/reference/android/view/KeyEvent?hl=zh-cn](https://developer.android.google.cn/reference/android/view/KeyEvent?hl=zh-cn)  

[https://docs.oracle.com/javase/7/docs/api/java/awt/event/KeyEvent.html](https://docs.oracle.com/javase/7/docs/api/java/awt/event/KeyEvent.html)

#### adb shell input tap 坐标点击

该命令是用于向设备发送一个**点击操作**的指令，参数是 坐标

```
adb shell input tap 100 100

```

**坐标的获取**

1.  adb shell getevent  # 或者带上 - l 参数
    
2.  在屏幕上某一点点击下
    
    ```
    C:\Users\Tanya>adb shell getevent
    add device 1: /dev/input/event4
      name:     "Android_Input"
    could not get driver version for /dev/input/mouse0, Not a typewriter
    add device 2: /dev/input/event3
      name:     "gpio-keys"
    add device 3: /dev/input/event1
      name:     "Sleep Button"
    add device 4: /dev/input/event0
      name:     "Power Button"
    add device 5: /dev/input/event2
      name:     "Video Bus"
    could not get driver version for /dev/input/mice, Not a typewriter
    
    /dev/input/event4: 0001 014a 00000001
    /dev/input/event4: 0003 0035 00000050   #0035，x坐标，50（16进制）=>80
    /dev/input/event4: 0003 0036 000003b7   #0036，y坐标,3b7=>951
    /dev/input/event4: 0000 0002 00000000
    /dev/input/event4: 0000 0000 00000000
    /dev/input/event4: 0000 0002 00000000
    /dev/input/event4: 0000 0000 00000000
    /dev/input/event4: 0001 014a 00000000
    /dev/input/event4: 0003 0035 00000000
    /dev/input/event4: 0003 0036 00000000
    /dev/input/event4: 0000 0002 00000000
    /dev/input/event4: 0000 0000 00000000
    
    #getevent -l参数更加直观
    C:\Users\Tanya>adb shell getevent -l
    add device 1: /dev/input/event4
      name:     "Android_Input"
    could not get driver version for /dev/input/mouse0, Not a typewriter
    add device 2: /dev/input/event3
      name:     "gpio-keys"
    add device 3: /dev/input/event1
      name:     "Sleep Button"
    add device 4: /dev/input/event0
      name:     "Power Button"
    add device 5: /dev/input/event2
      name:     "Video Bus"
    could not get driver version for /dev/input/mice, Not a typewriter
    /dev/input/event4: EV_KEY       BTN_TOUCH            DOWN
    /dev/input/event4: EV_ABS       ABS_MT_POSITION_X    00000145
    /dev/input/event4: EV_ABS       ABS_MT_POSITION_Y    000004da
    /dev/input/event4: EV_SYN       SYN_MT_REPORT        00000000
    /dev/input/event4: EV_SYN       SYN_REPORT           00000000
    /dev/input/event4: EV_SYN       SYN_MT_REPORT        00000000
    /dev/input/event4: EV_SYN       SYN_REPORT           00000000
    /dev/input/event4: EV_KEY       BTN_TOUCH            UP
    /dev/input/event4: EV_ABS       ABS_MT_POSITION_X    00000000
    /dev/input/event4: EV_ABS       ABS_MT_POSITION_Y    00000000
    /dev/input/event4: EV_SYN       SYN_MT_REPORT        00000000
    /dev/input/event4: EV_SYN       SYN_REPORT           00000000
    
    ```
    
3.  这样就获取到了你要点击位置的 x 和 y 坐标
    

#### adb shell input swipe [duration(ms)] 滑动

向设备发送一个滑动指令，并且可以选择设置滑动时长。

**滑动操作**

```
adb shell input swipe 100 100 200 200  300 //从 100 100 经历300毫秒滑动到 200 200

```

**长按操作**

```
adb shell input swipe 100 100 100 100  1000 //在 100 100 位置长按 1000毫秒

```

### Monkey 命令

*   官方：[https://developer.android.google.cn/studio/test/monkey](https://developer.android.google.cn/studio/test/monkey)
    
*   Monkey 是一个在模拟器或设备上运行的程序（命令行工具），可生成伪随机用户事件（例如点击、轻触或手势）流以及很多系统级事件。您可以使用 Monkey 以随机且可重复的方式对正在开发的应用进行压力测试。
    
*   Monkey 程序是 Android 系统自带的，用 Java 语言编写。
    
    ```
    root@generic:/system/bin # ll /system/framework/monkey.*
    -rw-r--r-- root     root        48970 2013-10-28 23:58 monkey.jar
    -rw-r--r-- root     root       120392 2013-10-28 23:58 monkey.odex
    root@generic:/system/bin # ll /system/bin/monkey
    -rwxr-xr-x root     shell         217 2013-10-28 23:35 monkey
    
    ```
    
    ```
    root@shamu:/system/bin # cat monkey
    # Script to start "monkey" on the device, which has a very rudimentary
    # shell.
    #
    base=/system
    export CLASSPATH=$base/framework/monkey.jar
    trap "" HUP
    exec app_process $base/bin com.android.commands.monkey.Monkey $*
    
    ```
    
*   Monkey 在运行时会生成事件并将其发送到系统。它还会监视被测系统并查找三种特殊情况：
    

*   如果您已将 Monkey 限制为在一个或多个特定软件包中运行，它会监视转到任何其他软件包的尝试并阻止它们。
    
*   如果应用崩溃或收到任何未处理的异常，Monkey 会停止并报告错误。
    
*   如果应用生成 “应用无响应” 错误，Monkey 会停止并报告错误。
    

#### 架构

![](http://vip.ytesting.com/plug-in/ueditor/themes/default/images/spacer.gif)

#### 参数说明

*   概览
    
    ```
    C:\Users\Tanya>adb shell monkey
    usage: monkey [-p ALLOWED_PACKAGE [-p ALLOWED_PACKAGE] ...]
                  [-c MAIN_CATEGORY [-c MAIN_CATEGORY] ...]
                  [--ignore-crashes] [--ignore-timeouts]
                  [--ignore-security-exceptions]
                  [--monitor-native-crashes] [--ignore-native-crashes]
                  [--kill-process-after-error] [--hprof]
                  [--pct-touch PERCENT] [--pct-motion PERCENT]
                  [--pct-trackball PERCENT] [--pct-syskeys PERCENT]
                  [--pct-nav PERCENT] [--pct-majornav PERCENT]
                  [--pct-appswitch PERCENT] [--pct-flip PERCENT]
                  [--pct-anyevent PERCENT] [--pct-pinchzoom PERCENT]
                  [--pkg-blacklist-file PACKAGE_BLACKLIST_FILE]
                  [--pkg-whitelist-file PACKAGE_WHITELIST_FILE]
                  [--wait-dbg] [--dbg-no-events]
                  [--setup scriptfile] [-f scriptfile [-f scriptfile] ...]
                  [--port port]
                  [-s SEED] [-v [-v] ...]
                  [--throttle MILLISEC] [--randomize-throttle]
                  [--profile-wait MILLISEC]
                  [--device-sleep-time MILLISEC]
                  [--randomize-script]
                  [--script-log]
                  [--bugreport]
                  [--periodic-bugreport]
                  COUNT
    
    ```
    

##### 基本配置选项

<table><thead><tr cid="n878" mdtype="table_row"><th>选项</th><th>含义</th></tr></thead><tbody><tr cid="n881" mdtype="table_row"><td>-p ALLOWED_PACKAGE [-p ALLOWED_PACKAGE]</td><td>如果您通过这种方式指定<strong>一个或多个软件包</strong>，Monkey 将仅允许系统访问这些软件包内的 Activity。如果应用需要访问其他软件包中的 Activity（例如选择联系人），您还需要指定这些软件包。 如果您未指定任何软件包，Monkey 将允许系统启动所有软件包中的 Activity。要指定多个软件包，请多次使用 -p 选项 - 每个软件包对应一个 -p 选项。</td></tr><tr cid="n884" mdtype="table_row"><td>-c MAIN_CATEGORY [-c MAIN_CATEGORY]</td><td>如果您通过这种方式指定一个或多个类别，Monkey 将仅允许系统访问其中一个指定类别中所列的 Activity。 如果您没有指定任何类别，Monkey 会选择 Intent.CATEGORY_LAUNCHER 或 Intent.CATEGORY_MONKEY 类别所列的 Activity。要指定多个类别，请多次使用 -c 选项 - 每个类别对应一个 -c 选项。</td></tr><tr cid="n887" mdtype="table_row"><td>-v [-v -v]</td><td>命令行上的每个 -v 都会增加详细程度级别。 级别 0（默认值）只提供启动通知、测试完成和最终结果。 级别 1 提供有关测试在运行时（例如发送到您的 Activity 的各个事件）的更多详细信息。 级别 2 提供更详细的设置信息，例如已选择或未选择用于测试的 Activity。</td></tr><tr cid="n890" mdtype="table_row"><td>-s<seed></seed></td><td>伪随机数生成器的种子值。如果您使用相同的种子值重新运行 Monkey，它将会生成相同的事件序列。</td></tr><tr cid="n893" mdtype="table_row"><td>--throttle<milliseconds></milliseconds></td><td>在事件之间插入固定的延迟时间。您可以使用此选项减慢 Monkey 速度。 如果未指定，则不延迟，系统会尽快地生成事件。</td></tr><tr cid="n896" mdtype="table_row"><td>COUNT</td><td>次数，所有事件的总和。</td></tr><tr cid="n899" mdtype="table_row"><td>-f scriptfile [-f scriptfile]</td><td>将 Monkey 的参数放在脚本文件中</td></tr></tbody></table>

##### 事件类型

<table><thead><tr cid="n905" mdtype="table_row"><th>选项</th><th>代号</th><th>含义</th></tr></thead><tbody><tr cid="n909" mdtype="table_row"><td>--pct-touch<percent></percent></td><td>0</td><td>调整<strong>轻触</strong>事件所占百分比。 （轻触事件是指屏幕上的单个位置上的按下 / 释放事件。）</td></tr><tr cid="n913" mdtype="table_row"><td>--pct-motion<percent></percent></td><td>1</td><td>调整<strong>动作</strong>事件所占百分比。 （动作事件包括屏幕上某个位置的按下事件，一系列伪随机动作和一个释放事件。）</td></tr><tr cid="n917" mdtype="table_row"><td>--pct-pinchzoom<percent></percent></td><td>2</td><td>调整<strong>缩放</strong>事件所占百分比。</td></tr><tr cid="n921" mdtype="table_row"><td>--pct-trackball<percent></percent></td><td>3</td><td>调整轨迹球事件所占百分比。 （轨迹球事件包括一个或多个随机动作，有时后跟点击。）</td></tr><tr cid="n925" mdtype="table_row"><td>--pct-rotation<percent></percent></td><td>4</td><td>调整屏幕<strong>旋转</strong>事件所占百分比。</td></tr><tr cid="n929" mdtype="table_row"><td>--pct-nav<percent></percent></td><td>5</td><td>调整 “基本” 导航事件所占百分比。 （导航事件包括向上 / 向下 / 向左 / 向右，作为方向输入设备的输入。）</td></tr><tr cid="n933" mdtype="table_row"><td>--pct-majornav<percent></percent></td><td>6</td><td>调整 “主要” 导航事件所占百分比。 （这些导航事件通常会导致界面中的操作，例如 5 方向键的中间按钮、返回键或菜单键。）</td></tr><tr cid="n937" mdtype="table_row"><td>--pct-syskeys<percent></percent></td><td>7</td><td>调整<strong> “系统” 按键</strong>事件所占百分比。 （这些按键通常预留供系统使用，例如 “主屏幕”、“返回”、“发起通话”、“结束通话” 或“音量控件”。）</td></tr><tr cid="n941" mdtype="table_row"><td>--pct-appswitch<percent></percent></td><td>8</td><td>调整 <strong>Activity 启动次数</strong>所占百分比。Monkey 会以随机间隔发起 startActivity() 调用，以最大限度地覆盖软件包中的所有 Activity。</td></tr><tr cid="n945" mdtype="table_row"><td>--pct-flip<percent></percent></td><td>9</td><td>调整<strong>键盘唤出</strong>隐藏时间百分比。</td></tr><tr cid="n949" mdtype="table_row"><td>--pct-anyevent<percent></percent></td><td>10</td><td>调整其他类型事件所占百分比。这包括所有其他类型的事件，例如按键、设备上的其他不太常用的按钮等等。</td></tr><tr cid="n953" mdtype="table_row"><td><br></td><td><br></td><td><br></td></tr></tbody></table>

##### 调试选项

<table><thead><tr cid="n960" mdtype="table_row"><th>选项</th><th>含义</th></tr></thead><tbody><tr cid="n963" mdtype="table_row"><td>--dbg-no-events</td><td>指定后，Monkey 将初始启动到测试 Activity，但不会生成任何其他事件。 为了获得最佳结果，请结合使用 -v、一个或多个软件包约束条件以及非零限制，以使 Monkey 运行 30 秒或更长时间。这提供了一个环境，您可以在其中监控应用调用的软件包转换操作。</td></tr><tr cid="n966" mdtype="table_row"><td>--hprof</td><td>如果设置此选项，则会在 Monkey 事件序列之前和之后立即生成分析报告。这将在 data/misc 下生成大型（约为 5Mb）文件，因此请谨慎使用。</td></tr><tr cid="n969" mdtype="table_row"><td>--ignore-crashes</td><td>通常，当应用崩溃或遇到任何类型的未处理异常时，Monkey 将会停止。如果您指定此选项，Monkey 会继续向系统发送事件，直到计数完成为止</td></tr><tr cid="n972" mdtype="table_row"><td>--ignore-timeouts</td><td>通常情况下，如果应用遇到任何类型的超时错误（例如 “应用无响应” 对话框），Monkey 将会停止。如果您指定此选项，Monkey 会继续向系统发送事件，直到计数完成为止。</td></tr><tr cid="n975" mdtype="table_row"><td>--ignore-security-exceptions</td><td>通常情况下，如果应用遇到任何类型的权限错误（例如，如果它尝试启动需要特定权限的 Activity），Monkey 将会停止。如果您指定此选项，Monkey 会继续向系统发送事件，直到计数完成为止。</td></tr><tr cid="n978" mdtype="table_row"><td>--kill-process-after-error</td><td>通常情况下，当 Monkey 因出错而停止运行时，出现故障的应用将保持运行状态。设置此选项后，它将会指示系统停止发生错误的进程。 注意，在正常（成功）完成情况下，已启动的进程不会停止，并且设备仅会处于最终事件之后的最后状态。</td></tr><tr cid="n981" mdtype="table_row"><td>--monitor-native-crashes</td><td>监视并报告 Android 系统原生代码中发生的崩溃。如果设置了 --kill-process-after-error，系统将会停止。</td></tr><tr cid="n984" mdtype="table_row"><td>--wait-dbg</td><td>阻止 Monkey 执行，直到为其连接了调试程序。</td></tr><tr cid="n987" mdtype="table_row"><td><br></td><td><br></td></tr></tbody></table>

#### 示例

```
adb shell monkey -p com.kejia.mine -v  -v 100
adb shell monkey -p com.kejia.mine -v -v -v 100 > C:\monkey.log
adb shell monkey -p com.kejia.mine  -s XXXX --pct-rotation 80 -v -v -v 100
adb shell monkey -p com.kejia.mine  –p com.tencent.mobileqq -v -v 100
adb shell monkey -p com.shjt.map 100 
adb shell monkey –p com.kejia.mine –-throttle 100 –-pct-touch 50 –-pct-motion 50 –v –v 1000 >c:\monkey.txt


```

*   注意 s 值也可以在执行的时候指定
    
*   PERCENT 可以不为 100
    

### 其他在自动化可能用到的命令

*   adb shell getprop 获取配置
    
*   adb  screencap  xxx.png  # 注意文件读写要有权限，可以用 root 用户在 / data 下写入，再 adb root 获取 root 权限后用 adb  pull 下载
    
*   adb  shell  ps
    
*   adb  shell  top  -m  2
    
*   adb shell ime list -s  # 列出输入法
    
*   adb shell wm
    
    ```
    generic_x86_arm:/ $ wm size      #分辨率  宽*高
    Physical size: 1080x2220
    generic_x86_arm:/ $ wm density   #DPI 每英寸的像素数，越高越细腻
    Physical density: 440
    
    还有一个指标是屏幕尺寸，如5英寸（对角线长度）
    上面3个指标是可以计算的
    1080*1080+2220*2220开根号除以屏幕尺寸就是DPI的值
    
    ```
    

Appium 介绍
---------

> [https://appium.io/docs/cn/about-appium/intro/](https://appium.io/docs/cn/about-appium/intro/)

1.  全能:  iOS 手机、 Android 手机和 Windows 桌面平台上的原生、移动 Web 和混合应用
    
2.  通用: 支持 Win/Linux/Mac, 支持 Java/Python/Ruby/Js/PHP 等各种语言
    

1.  iOS 平台的支持
    
    ```
    ★版本：9.0 及以上版本
    ★设备：iPhone 模拟器，iPad 模拟器，以及 iPhone 和 iPad 的真机
    ★是否支持 Native 应用：支持。如在模拟器执行，需要 debug 版本的 .app 包，在真机上运行则需要已签名的 .ipa 包。底层的框架是由苹果的 XCUITest (或 UIAutomation 支持更旧的版本) 所提供支持
    ★是否支持移动端浏览器：支持。我们通过移动端的 Safari 进行自动化测试。对于真机，ios-webkit-remote-debugger 工具是必须的。可惜的是对于 Safari 的 native 部分的自动化目前还不支持。更多介绍请查看 mobile web doc(English)。
    ★是否支持 Hybrid 应用：支持。如使用真机，ios-webkit-remote-debugger 工具也是必须的。更多介绍请查看 hybrid doc。
    ★是否支持在一个 session 中多个应用的自动化：不支持
    ★是否支持多设备同时执行自动化：不支持
    ★是否支持第三方应用的自动化：仅支持在模拟器上仅有的第三方应用（设置，地图，等等...）。若在 iOS 10 及以上的版本，你同样可以在 home 界面做自动化。
    ★是否支持自定义的、非标准的 UI 控件的自动化：只支持小部分。你需要在控件设置可识别的信息，从而对一些元素进行一些基础的自动化操作。
    
    ```
    
2.  Android 平台支持
    
    ```
    ★版本：4.3 及以上版本
      4.3 以及更高的版本是通过 Appium 自己的 UiAutomator 库实现。这是默认的自动化后台。
    ★设备：Android 模拟器以及 Android 真机
    ★是否支持 Native 应用：支持
    ★是否支持移动端浏览器：支持（除了使用 Selendroid 后台的时候）。Appium 绑定了一个   
      Chromedriver 服务，使用这个代理服务进行自动化测试。在 4.2 和 4.3 版本，只能在官方的 Chrome   浏览器或者 Chromium 执行自动化测试。在 4.4 及更高版本，可以在内置的 “浏览器” 应用上进行自动化   了。更多介绍请查看 mobile web doc(English)。
    ★是否支持 Hybrid 应用：支持。更多介绍请查阅 hybrid doc。
    ★默认的 Appium 自动化后台：支持 4.4 以及更高版本
    ★Selendroid 自动化后台：支持 2.3 以及更高版本
    ★是否支持多个 app 在同一个 session 中自动化：支持（除了使用 Selendroid 后台的时候）
    ★是否支持多个设备同时进行自动化：支持，即使 Appium 在开始的时候，使用不同的端口号作为服务器参      数，--port, --bootstrap-port (或者 --selendroid-port) 还有 --chromedriver-port. 更   多介绍请查看 server args doc。
    ★支持第三方引用：支持（除了使用 Selendroid 后台的时候）
    ★支持自定义的、非标准的 UI 控件的自动化：不支持
    
    ```
    
3.  开源: 免费
    

Appium 设计理念
-----------

1.  你不应该为了自动化而重新编译你的应用或以任何方式修改它。
    
    ```
    为了实现理念#1，我们使用了系统自带的自动化框架。这样，我们不需要把 Appium 特定的或者第三方的代码编译进你的应用，这意味着你测试使用的应用与最终发布的应用并无二致。我们使用以下系统自带的自动化框架：
        iOS 9.3 及以上: 苹果的 XCUITest
        iOS 9.3 及以下: 苹果的 UIAutomation
        Android 4.3+: 谷歌的 UiAutomator / UiAutomator2
         　UiAutomator是Android自动化测试框架
         　UiAutomator2是UiAutomator的升级版本
        Android 2.3+: 谷歌的 Instrumentation. (通过绑定独立的项目—— Selendroid 提供对 Instrumentation 的支持)
        Windows: 微软的 WinAppDriver
    
    ```
    
2.  你不应该被限制在特定的语言或框架上来编写运行测试。
    
    ```
    为了实现理念#2，我们把这些系统本身提供的框架包装进一套 API —— WebDriver API 中。WebDriver（也叫「Selenium WebDriver」）规定了一个客户端-服务器协议（称为 JSON Wire Protocol），按照这种客户端-服务器架构，可以使用任何语言编写的客户端向服务器发送适当的 HTTP 请求。已经有为 各个流行编程语言编写的客户端 。这也意味着你可以自由使用任何你想用的的测试运行器和测试框架；客户端程序库不过是一个简单的 HTTP 客户端，可以以任何你喜欢的方式混入你的代码。换句话说，Appium & WebDriver 客户端在技术上而言不是「测试框架」，而是「自动化程序库」。你可以以任何你喜欢的方式管理你的测试环境！
    
    ```
    
3.  移动端自动化框架不应该在自动化接口方面重造轮子。
    
    ```
    实现理念#3：WebDriver 已经成为 Web 浏览器自动化事实上的标准，并且是一个 W3C 工作草案。何必在移动端做完全不同的尝试？我们通过附加额外的 API 方法 扩展协议，这些方法对移动自动化非常有用
    
    ```
    
4.  移动端自动化框架应该开源，在精神、实践以及名义上都该如此
    
    ```
    https://github.com/appium
    
    ```
    

Appium 概念
---------

**客户端 / 服务器架构** Appium 的核心一个是暴露 REST API 的 WEB 服务器。它接受来自客户端的连接，监听命令并在移动设备上执行，答复 HTTP 响应来描述执行结果。实际上客户端 / 服务器架构给予了我们许多可能性：我们可以使用任何有 http 客户端 API 的语言编写我们的测试代码，不过选一个 [Appium 客户端程序库](http://appium.io/downloads) 用起来更为容易。我们可以把服务器放在另一台机器上，而不是执行测试的机器。我们可以编写测试代码，并依靠类似 [Sauce Labs](https://saucelabs.com/products/mobile-app-testing) 的云服务接收和解释命令。

**会话（Session）** 自动化始终在一个会话的上下文中执行，这些客户端程序库以各自的方式发起与服务器的会话，但最终都会发给服务器一个 `POST /session` 请求，请求中包含一个被称作「预期能力（Desired Capabilities）」的 JSON 对象。这时服务器就会开启这个自动化会话，并返回一个用于发送后续命令的会话 ID。

**预期能力（Desired Capabilities）** 预期能力（Desired Capabilities）是一些发送给 Appium 服务器的键值对集合（比如 map 或 hash），它告诉服务器我们想要启动什么类型的自动化会话。也有许多能力（Capabilities）可以修改服务器在自动化过程中行为。例如，我们可以将 `platformName` 能力设置为 `iOS`，以告诉 Appium 我们想要 iOS 会话，而不是 Android 或者 Windows 会话。或者我们也可以设置 `safariAllowPopups` 能力为 `true` ，确保我们在 Safari 自动化会话期间可以使用 JavaScript 打开新窗口。有关 Appium 能力的完整列表，请参阅 [能力文档](https://github.com/appium/appium/blob/master/docs/cn/writing-running-appium/caps.md) 。

**Appium 服务器** Appium 是一个用 Node.js 写的服务器。可以从[源码](https://github.com/appium/appium/blob/master/docs/cn/contributing-to-appium/appium-from-source.md)构建安装或者从 [NPM](https://www.npmjs.com/package/appium) 直接安装：

```
$ npm install -g appium
$ appium

```

Appium 的 `beta` 版本可以通过 NPM 使用 `npm install -g appium@beta` 指令进行安装。它是开发版本，所以可能存在破坏性的变更。在安装新版本请卸载 `appium@beta` （`npm uninstall -g appium@beta`）以获得一组干净的依赖。

**Appium 客户端** 有一些客户端程序库（分别在 Java、Ruby、Python、PHP、JavaScript 和 C# 中实现），它们支持 Appium 对 WebDriver 协议的扩展。你需要用这些客户端程序库代替常规的 WebDriver 客户端。你可以在[这里](https://github.com/appium/appium/blob/master/docs/cn/about-appium/appium-clients.md)浏览所有程序库的列表。

**Appium Desktop** 这有一个 Appium 服务器的图形界面封装可以下载，它适用于任何平台。它打包了 Appium 服务器运行需要的所有东西，所以你不需要为 Node 而烦恼。它们还提供一个 Inspector 使你可以查看应用程序的层级结构。这在写测试时可以派上用场。

Appium 实现原理
-----------

**（1）Android 平台**

①由 Client 发起请求，经过中间服务套件，驱动 App 执行相关的操作。 Client 是测试人员开发的 WebDriver 测试脚本，也就是你我写的代码。

②中间服务套件则是 Appium 解析服务，Appium 在服务端启用 4723 端口，通过该端口实现 Client 与 Appium Server 通信。Appium Server 把请求转发给中间件 Bootstrap.jar。Bootstrap.jar 安装在手机上，监听 4724 端口并接收 Appium 命令，最终通过调用 UIAutomator 命令来实现测试过程。

③Bootstrap 将执行的结果返回给 Appium Server。Appium Server 再将结果返回给 Client。

**（2）iOS 平台**

①由 Client 发起请求，经过中间服务套件，驱动 App 执行相关的操作。 Client 是测试人员开发的 Webdriver 测试脚本。

②中间服务套件则是 Appium 解析服务，Appium 在服务端启用 4723 端口，通过该端口实现 Client 与 Appium Server 通信。Appium Server 调用 instruments.js 启动一个 Socket Server，同时分出一个⼦进程运⾏ instruments.app，将 bootstrap.js（一个 UIAutomation 脚本）注入到设备从而与外界进行交互。

③Bootstrap.js 将执行的结果返回给 Appium Server，Appium Server 再将结果返回给 Client。

Android 与 iOS 区别在于 Appium 将请求转发到 bootstrap.js 或者 bootstrap.jar. 然后由 bootstrap 驱动 UIAutomation 或 UIAutomator 去设备上完成具体的动作。

**架构图**

![](http://vip.ytesting.com//upload/ueditor/upload/image/20211029/1635502138411058304.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20211029/1635502156093082998.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20211029/1635502201102008426.png)

![](http://vip.ytesting.com//upload/ueditor/upload/image/20211029/1635502228512095787.png)

> [XCUITest](https://developer.apple.com/reference/xctest) ：xcode ui test，是 apple 推出的在 ios9.3 之后的 UI 测试标准框架（xcode 不低于 7）
> 
> ios UiAutomation： 是 XCode 自带的 UI 自动化测试工具，支持录制回放功能，支持 javascript 编辑脚本，能够在真机和模拟器上面执行自动化测试。
> 
> Android uiautomator：[https://developer.android.google.cn/training/testing/ui-automator?hl=zh-cn](https://developer.android.google.cn/training/testing/ui-automator?hl=zh-cn)，UI Automator 是一个界面测试框架，适用于整个系统上以及多个已安装应用间的跨应用功能界面测试。UI Automator 测试框架提供了一组 API，用于构建在用户应用和系统应用上执行交互的界面测试。通过 UI Automator API，您可以执行在测试设备中打开 “设置” 菜单或应用启动器等操作。UI Automator 测试框架非常适合编写黑盒式自动化测试。
> 
> UI Automator 测试框架的主要功能包括：
> 
> *   用于检查布局层次结构的查看器。
>     
> *   用于检索状态信息并在目标设备上执行操作的 API。
>     
> *   支持跨应用界面测试的 API。
>