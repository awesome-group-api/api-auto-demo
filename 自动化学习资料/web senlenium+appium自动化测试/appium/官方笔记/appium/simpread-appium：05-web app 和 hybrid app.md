> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b5879e220ea017a2d3c5cea7ba1)

> appium 支持 Android 的 Chrome 和 iOS 的 safari。

Web App
-------

### 概述

*   appium 支持 Android 的 Chrome 和 iOS 的 safari。
    
*   如果是真机，手机上你未必安装 chrome，那要去下载 chrome 的安装包（老师提供了一些）
    
    ```
    https://apkpure.com/google-chrome-fast-secure/com.android.chrome  #需要翻墙，下载的是xapk包
    
    ```
    
    国内的话也有，但需要有 google 账号和绑定对应的设备
    
    ![](http://vip.ytesting.com//upload/ueditor/upload/image/20211102/1635817534899071786.png)
    
    ![](http://vip.ytesting.com//upload/ueditor/upload/image/20211102/1635817559080077629.png)
    
*   但不幸的是，每次 Chromedriver 升级，支持的 Chrome 的最小版本都会升级，这就导致了老设备上经常无法在绑定的版本上自动化。在 Appium 的服务端就会有这样的错误日志：
    
    ```
    An unknown server-side error occurred while processing the command.
    Original error: unknown error: Chrome version must be >= 55.0.2883.0
    
    ```
    
*   这种情况下你需要指定版本
    
    ![](http://vip.ytesting.com//upload/ueditor/upload/image/20211102/1635817582512069628.png)
    
*   网上有版本和驱动的对应关系表格
    
    > [https://github.com/appium/appium/blob/master/docs/cn/writing-running-appium/web/chromedriver.md](https://github.com/appium/appium/blob/master/docs/cn/writing-running-appium/web/chromedriver.md)
    

> [https://appium.io/docs/cn/writing-running-appium/web/chromedriver/index.html](https://appium.io/docs/cn/writing-running-appium/web/chromedriver/index.html)

 这里截取部分

<table><thead><tr cid="n2968" mdtype="table_row"><th>Version</th><th>Minimum Chrome Version</th></tr></thead><tbody><tr cid="n2971" mdtype="table_row"><td>2.29</td><td>57.0.2987.0</td></tr><tr cid="n2974" mdtype="table_row"><td>2.28</td><td>55.0.2883.0</td></tr><tr cid="n2977" mdtype="table_row"><td>2.27</td><td>54.0.2840.0</td></tr><tr cid="n2980" mdtype="table_row"><td>2.26</td><td>53.0.2785.0</td></tr><tr cid="n2983" mdtype="table_row"><td>2.25</td><td>53.0.2785.0</td></tr><tr cid="n2986" mdtype="table_row"><td>2.24</td><td>52.0.2743.0</td></tr><tr cid="n2989" mdtype="table_row"><td>2.23</td><td>51.0.2704.0</td></tr><tr cid="n2992" mdtype="table_row"><td>2.22</td><td>49.0.2623.0</td></tr><tr cid="n2995" mdtype="table_row"><td>2.21</td><td>46.0.2490.0</td></tr><tr cid="n2998" mdtype="table_row"><td>2.20</td><td>43.0.2357.0</td></tr><tr cid="n3001" mdtype="table_row"><td>2.19</td><td>43.0.2357.0</td></tr><tr cid="n3004" mdtype="table_row"><td>2.18</td><td>43.0.2357.0</td></tr><tr cid="n3007" mdtype="table_row"><td>2.17</td><td>42.0.2311.0</td></tr><tr cid="n3010" mdtype="table_row"><td>2.16</td><td>42.0.2311.0</td></tr><tr cid="n3013" mdtype="table_row"><td>2.15</td><td>40.0.2214.0</td></tr><tr cid="n3016" mdtype="table_row"><td>2.14</td><td>39.0.2171.0</td></tr><tr cid="n3019" mdtype="table_row"><td>2.13</td><td>38.0.2125.0</td></tr><tr cid="n3022" mdtype="table_row"><td>2.12</td><td>36.0.1985.0</td></tr><tr cid="n3025" mdtype="table_row"><td>2.11</td><td>36.0.1985.0</td></tr><tr cid="n3028" mdtype="table_row"><td>2.10</td><td>33.0.1751.0</td></tr><tr cid="n3031" mdtype="table_row"><td>2.9</td><td>31.0.1650.59</td></tr><tr cid="n3034" mdtype="table_row"><td>2.8</td><td>30.0.1573.2</td></tr><tr cid="n3037" mdtype="table_row"><td>2.7</td><td>30.0.1573.2</td></tr><tr cid="n3040" mdtype="table_row"><td>2.6</td><td><br></td></tr><tr cid="n3043" mdtype="table_row"><td>2.5</td><td><br></td></tr><tr cid="n3046" mdtype="table_row"><td>2.4</td><td>29.0.1545.0</td></tr><tr cid="n3049" mdtype="table_row"><td>2.3</td><td>28.0.1500.0</td></tr><tr cid="n3052" mdtype="table_row"><td>2.2</td><td>27.0.1453.0</td></tr><tr cid="n3055" mdtype="table_row"><td>2.1</td><td>27.0.1453.0</td></tr><tr cid="n3058" mdtype="table_row"><td>2.0</td><td>27.0.1453.0</td></tr></tbody></table>

*   在你的代码中增加能力值即可（原理跟 python-selenium 的驱动是类似的）
    
    ```
    des_cap['chromedriverExecutable']=r'D:\app_chromedriver\chromedriver.exe'  #指定chromedriver
    des_cap['showChromedriverLog'] = True   #打印底层细节
    
    ```
    

* * *

### 操作

*   首先在设备中打开浏览器，并打开你要操作的 web 界面
    
*   在本地浏览器中输入
    
    ```
    chrome://inspect/#devices
    
    ```
    
    ![](http://vip.ytesting.com//upload/ueditor/upload/image/20211102/1635817611474076102.png)
    

*   正常情况下是可以看到的，但如果开发没有打开 webview 的调试开关是不行的
    
    ```
    WebView.setWebContentsDebuggingEnabled(true); #需要注意的是每个webview组件实例需要单独设置
    
    ```
    

*   点击 inspect，此处实测不翻墙无法访问，而翻墙后是可以的（不知道后续会不会调整）
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20211102/1635817630859072945.png)

*   这是一种方式，但也可以用浏览器的开发者工具，直接用手机模式来打开网站也是类似的，不同点是两个浏览器不太一样。要学会如何查看浏览器的版本。
    
*   这样你定位元素的时候跟 selenium 是完全一致的，要注意的是 PC 和 PHONE 上的同一个网站，其元素信息往往是不一样的（以百度为例）
    

Hybrid App
----------

### 关于 webview

*   WebView 是一种控件，它基于 webkit 引擎，具备渲染 Web 页面的功能。
    
*   基于 Webview 的混合开发，就是在 Anddroid os(安卓)/I os(苹果) 原生 APP 里，通过 WebView 控件嵌入 Web 页面
    
*   优点
    

*   一次开发，多系统适配，节省了人力成本和时间成本
    
*   Web 可以线上即时更新，不用下载安装补丁包
    

*   缺点
    

*   性能不足（渲染），但逐步赶上了。应付简单的一些操作是足够的（如浏览），但如果是游戏等性能需求强劲的，则还欠缺的很（一般都是原生的实现）。
    

### 进入 webview 的操作流程

> [https://appium.io/docs/cn/writing-running-appium/web/hybrid/](https://appium.io/docs/cn/writing-running-appium/web/hybrid/)

*   启动应用
    
*   获取当前页的上下文（contexts）
    

*   一般当前是原生界面
    
*   uiautomator 解析 webview 中的内容并映射为原生控件
    
*   getPageSource 为 DOM 结构可发现 webview 组件和控件
    

*   切换到指定的 webview
    

*   getPageSource 是 HTML
    
*   就是 web 页面
    
*   进行 HTML 操作（selenium api 完全可用，如你可以使用 css 来定位）
    

*   切换回原生框架（native app）
    
*   进行原生界面操作（用 id、class、xpath、accessibility_id、android_uiautomator 等定位）
    

### 上下文 context

<table><thead><tr cid="n3149" mdtype="table_row"><th>方法</th><th>说明</th></tr></thead><tbody><tr cid="n3152" mdtype="table_row"><td>current_context</td><td>当前的上下文</td></tr><tr cid="n3155" mdtype="table_row"><td>context</td><td>同上</td></tr><tr cid="n3158" mdtype="table_row"><td>contexts</td><td>所有的上下文，1 个原生的或多个 web view contexts</td></tr></tbody></table>

*   原生：NATIVE_APP
    
*   chrome：CHROMIUM
    
*   iOS - WEBVIEW_<id>
    
*   Android - WEBVIEW_<package name>
    

*   示例：以海文老师提供的 app 为例
    
    ```
    from appium import webdriver
    from time import sleep
    des_cap = {}
    des_cap['platformName']='android'
    des_cap['platformVersion']='11'
    # des_cap['browserName']='chrome'  #用浏览器吊起来不行的
    des_cap['appPackage']='com.example.haiwen.myhybirdapp'
    des_cap['appActivity']='.MainActivity'
    
    driver = webdriver.Remote(command_executor='http://127.0.0.1:4723/wd/hub',
                              desired_capabilities=des_cap)
    driver.implicitly_wait(10)
    print(driver.contexts)
    print(driver.current_context)
    input_url = driver.find_element_by_id('com.example.haiwen.myhybirdapp:id/editText')
    input_url.send_keys('https://m.douban.com/home_guide')
    driver.find_element_by_id('com.example.haiwen.myhybirdapp:id/button').click()
    print(driver.current_context)
    print(driver.contexts)
    driver.switch_to.context(driver.contexts[-1])
    driver.find_element_by_css_selector('.search-input').send_keys('长津湖\n')
    #selenium.common.exceptions.WebDriverException: Message: An unknown server-side error occurred while processing the command. Original error: invalid argument: cannot parse capability: goog:chromeOptions
    #from invalid argument: unrecognized chrome option: androidDeviceSerial
    
    score = driver.find_element_by_css_selector('.search-module:nth-child(2)  .rating>span:last-child')
    print('长津湖当前评分',score.text)
    #再切换回去操作
    driver.switch_to.context('NATIVE_APP')
    driver.find_element_by_id('com.example.haiwen.myhybirdapp:id/editText').clear()
    driver.find_element_by_id('com.example.haiwen.myhybirdapp:id/editText').send_keys('https://m.baidu.com')
    
    ```
    

*   输入 url（豆瓣）
    
*   输入电影名，得到其评分
    
*   返回到原生界面，再输入新的内容
    

区分 H5 和 NATIVE 的几种方法
--------------------

> 转自：[https://www.cnblogs.com/Durant0420/p/14986462.html](https://www.cnblogs.com/Durant0420/p/14986462.html)

### 1、看断网情况

　　把手机的网络断掉。然后点开页面。然后可以正常显示的东西就是原生写的。

　　显示 404 或则错误页面的是 html 页面。

### 2、看布局边界

　　可以打开开发者选项中的显示布局边界，页面元素很多的情况下布局是一整块的是 h5 的，布局密密麻麻的是原生控件。页面有布局的是原生的否则为 h5 页面。（仅针对安卓手机试用）如下图所示

![](http://vip.ytesting.com//upload/ueditor/upload/image/20211102/1635817754649036782.png)

### 3、看复制文章的提示，需要你通过对比才能得出结果

　　比如是文章资讯页面可以长按页面试试，如果出现文字选择、粘贴功能的是 H5 页面，否则是 native 原生的页面。

　　有些原生 APP 开放了复制粘贴功能或者关闭了。而 H5 的 css 屏蔽了复制选择功能等等情况。需要通过对目标测试 APP 进行对比才可知。　　这个在支付宝 APP、蚂蚁聚宝都是可以判断的

### 4、看加载方式

　　如果在打开新页面导航栏下面有一条加载的线的话，这个页面就是 H5 页面，如果没有就是原生的。 微信里面打开我们的 H5 页面常见的有个绿色的 加载线条。如下图红框里面所示

![](http://vip.ytesting.com//upload/ueditor/upload/image/20211102/1635817773342088619.png)

### 5、看 app 顶部 导航栏是否会有关闭的操作

　　如果 APP 顶部导航栏当中出现了关闭按钮或者有关闭的图标，那么当前的页面肯定的 H5，原生的不会出现（除非设计开发者故意弄的）

　　美团的、大众点评的 APp、微信 APP 当加载 h5 过多的时候，左上角会出现关闭 2 字。

### 6、判断页面 下拉刷新的时候（前提是要有下拉刷新的功能）

　　如果界面没有明显刷新现象的是原生的，如果有明显刷新现象（比如闪一下）的是 H5 页面（ios 和 android）。

　　比如淘宝的众筹页面。

### 7、下拉页面的时候显示网址提供方的一定是 H5。如下图所示：

![](http://vip.ytesting.com//upload/ueditor/upload/image/20211102/1635817783835024411.png)