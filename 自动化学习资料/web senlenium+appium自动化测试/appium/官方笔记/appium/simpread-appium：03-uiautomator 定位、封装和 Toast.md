> 本文由 [简悦 SimpRead](http://ksria.com/simpread/) 转码， 原文地址 [vip.ytesting.com](http://vip.ytesting.com/newsItemController.do?goContent&id=2c9f9b5879e220ea017a2d36ea8d7b90)

> 官网介绍： https://developer.android.google.cn/training/testing/ui-automator?hl=zh-cn#java

### 元素定位高级方法

*   官网介绍： [https://developer.android.google.cn/training/testing/ui-automator?hl=zh-cn#java](https://developer.android.google.cn/training/testing/ui-automator?hl=zh-cn#java)
    
*   UiSelector 官网：[https://developer.android.google.cn/reference/android/support/test/uiautomator/UiSelector](https://developer.android.google.cn/reference/android/support/test/uiautomator/UiSelector)
    
*   前面提到的 id、class、accessibility 底层都是通过 uiautomator 来定位的。
    

#### find_element_by_android_uiautomator  定位

*   **在 appium inspector 中单引号不行，双引号可以**
    

**一、常规用法**

<table><thead><tr cid="n2232" mdtype="table_row"><th>方法</th><th>说明</th></tr></thead><tbody><tr cid="n2235" mdtype="table_row"><td>new UiSelector().text("通讯录")</td><td>text 属性 的值</td></tr><tr cid="n2238" mdtype="table_row"><td>new UiSelector().textContains("通讯录")</td><td>text 属性 包含的值</td></tr><tr cid="n2241" mdtype="table_row"><td>new UiSelector().textStartsWith("通讯录")</td><td>text 属性 开头的值</td></tr><tr cid="n2244" mdtype="table_row"><td>new UiSelector().textMatches('通. 录 $')</td><td>正则匹配 text 属性的值</td></tr><tr cid="n2247" mdtype="table_row"><td>new UiSelector().resourceId</td><td>resourceId 的值</td></tr><tr cid="n2250" mdtype="table_row"><td>new UiSelector().resourceIdMatches</td><td><br></td></tr><tr cid="n2253" mdtype="table_row"><td>new UiSelector().className</td><td>class 的值</td></tr><tr cid="n2256" mdtype="table_row"><td>new UiSelector().classNameMatches</td><td><br></td></tr><tr cid="n2259" mdtype="table_row"><td>new UiSelector().description</td><td>content-desc 的值</td></tr><tr cid="n2262" mdtype="table_row"><td>new UiSelector().descriptionContains</td><td><br></td></tr><tr cid="n2265" mdtype="table_row"><td>new UiSelector().descriptionStartsWith</td><td><br></td></tr><tr cid="n2268" mdtype="table_row"><td>new UiSelector().descriptionMatches</td><td><br></td></tr></tbody></table>

*   注意：new UiSelector(). 这部分是可以不写的
    

* * *

**二、高级用法**

**childSelector**  找子孙节点

*   语法
    

```
  父节点定位器.childSelector(待定位的子孙节点)

```

*   类似于实现了 xpath 或 css 中的同类功能，根据父节点找到某个子孙节点
    
*   示例
    
    ```
    #企业微信头部
    resourceId("com.tencent.wework:id/ixc")  #1
     #apidemo的示例
        resourceId("android:id/text1")  #可以定位到
        resourceId('android:id/text1')  #无法定位到
    #企业微信头部中消息区域（部分）
    resourceId("com.tencent.wework:id/ixc").childSelector(resourceId("com.tencent.wework:id/iw3"))  #2
    #企业微信头部消息区域（部分的部分） #childSelector是子代节点（不一定是儿子，可能是孙子）
    resourceId("com.tencent.wework:id/ixc").childSelector(resourceId("com.tencent.wework:id/iw5"))   #3
    
    
    ```
    
    ![](http://vip.ytesting.com//upload/ueditor/upload/image/20211031/1635672904945015340.png)
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20211031/1635672913876043409.png)

**fromParent**  找同一个祖宗的节点

*   语法
    
    ```
    兄节点定位器.fromParent(待定位的弟节点定位器)
    兄节点定位器.fromParent(待定位的弟的子孙节点定位器)
    
    ```
    
*   第一种类似于实现了 css 的 div+p 的功能
    
*   示例
    
    ```
    resourceId("com.tencent.wework:id/bs4").fromParent (resourceId("com.tencent.wework:id/h04"))   #找到的是fromParent 括号里面的节点
    resourceId("com.tencent.wework:id/h04").fromParent (resourceId("com.tencent.wework:id/bs4"))   #兄弟的位置前后可以调换
    resourceId("com.tencent.wework:id/bs4").fromParent (resourceId("com.tencent.wework:id/bry"))   #兄弟和兄弟3，只要是同一个祖宗极客 
    resourceId("com.tencent.wework:id/bs4").fromParent(resourceId("com.tencent.wework:id/f02"))    #前者是儿子，后者是孙子#不一定是同辈分 ， 这样你会觉得所有的节点都有一个共同的祖先，但实际不是的
    resourceId("com.tencent.wework:id/bs4").fromParent(resourceId("com.tencent.wework:id/f43"))  #比如这样得不到结果，因为他们各自网上追溯的时候，找到祖宗的同辈分的不是一个人 #很绕，除非你已经没有办法定位了，否则不建议用
    
    ```
    

![](http://vip.ytesting.com//upload/ueditor/upload/image/20211031/1635672932961002158.png)

**index** 某个节点下的第几个

*   示例
    
    ```
    index(0).resourceId("com.tencent.wework:id/f02")   #f02这个节点是某个节点的第一个子节点
    index(1).resourceId("com.tencent.wework:id/h04")  #H04这个节点是某个节点的第二个子节点，所有有未读消息的头像（但注意最后一个总计也在里面）
    
    ```
    

**instance**  找到的多个节点中的第一个

*   语法
    
    ```
    instancen(索引号).所有找到的节点
    
    ```
    
*   示例
    
    ```
    instance(0).resourceId("com.tencent.wework:id/bry")      #resourceId("com.tencent.wework:id/bry")    这个表达式会找到多个，取第一个
    
    ```
    

**组合定位**

*   类似于逻辑运算符 and
    
*   示例
    
    ```
    textContains("免费").resourceId("com.tencent.wework:id/i9_")
    
    ```
    

* * *

*   以企业微信为例：基础用法
    
    ```
    driver.find_element_by_android_uiautomator('new UiSelector().text("通讯录")').click()
    sleep(1)
    driver.find_element_by_android_uiautomator('new UiSelector().text("消息")').click()
    sleep(1)
    driver.find_element_by_android_uiautomator('new UiSelector().textContains("工作台")').click()
    sleep(1)
    driver.find_element_by_android_uiautomator('new UiSelector().textStartsWith("通讯")').click()
    sleep(1)
    #点击待办
    driver.find_element_by_android_uiautomator('new UiSelector().resourceId("com.tencent.wework:id/itp")').click()
    sleep(1)
    #点击返回
    #注意有多个匹配的class值，但这是第一个
    driver.find_element_by_android_uiautomator('new UiSelector().className("android.widget.TextView")').click()
    sleep(1)  #content-desc
    
    # driver.find_element_by_android_uiautomator('new UiSelector().description("该界面没有")').click()
    
    ```
    
*   点击所有未读消息
    
    ```
    from appium import webdriver
    from time import sleep
    des_cap = {}
    des_cap['platformName']='android'
    des_cap['platformVersion']='11'
    
    
    driver = webdriver.Remote(command_executor='http://127.0.0.1:4723/wd/hub',
                              desired_capabilities=des_cap)
    driver.activate_app('com.tencent.wework')   #高级操作的一种方法
    driver.implicitly_wait(20)
    sleep(1)
    print('点击消息')
    driver.find_element_by_android_uiautomator('text("消息")').click()
    
    eles = driver.find_elements_by_android_uiautomator('index(1).resourceId("com.tencent.wework:id/h04")')
    print(len(eles))
    for ele in eles[:-1][::-1]:  #注意最后一个节点是所有消息的统计，不能顺着点击，要逆向点
        print('点击未读消息')
        ele.click()
        sleep(1)
        print('点击返回')
        driver.find_element_by_android_uiautomator('resourceId("com.tencent.wework:id/ivv")').click()
    
    
    
    ```
    

### 封装

*   跟 selenium 是一样的
    
*   不同在于多了一个类，封装了一些特有的属性
    

1.  对于跟 selenium 类似的方法
    
    ```
    from selenium.webdriver.common.by import By
    class By:
        ID = "id"
        XPATH = "xpath"
        LINK_TEXT = "link text"
        PARTIAL_LINK_TEXT = "partial link text"
        NAME = "name"
        TAG_NAME = "tag name"
        CLASS_NAME = "class name"
        CSS_SELECTOR = "css selector"
    
    ```
    
2.  但 appium 特有的方法则不能用 by 来替代，要用 MobileBy 来替代，同时注意位置的不同
    
    ```
    from appium.webdriver.common.mobileby import MobileBy
    
    class MobileBy(By):  #但由于继承了By，所以在appium中可以统一用该方法来替换
        IOS_PREDICATE = '-ios predicate string'
        IOS_UIAUTOMATION = '-ios uiautomation'
        IOS_CLASS_CHAIN = '-ios class chain'
        ANDROID_UIAUTOMATOR = '-android uiautomator'
        ANDROID_VIEWTAG = '-android viewtag'
        ANDROID_DATA_MATCHER = '-android datamatcher'
        ANDROID_VIEW_MATCHER = '-android viewmatcher'
        WINDOWS_UI_AUTOMATION = '-windows uiautomation'
        ACCESSIBILITY_ID = 'accessibility id'
        IMAGE = '-image'
        CUSTOM = '-custom'
    
    ```
    

官方简介

> [https://appium.io/docs/en/commands/element/find-elements/index.html#selector-strategies](https://appium.io/docs/en/commands/element/find-elements/index.html#selector-strategies)

<table><thead><tr cid="n2368" mdtype="table_row"><th>Strategy</th><th>Description</th></tr></thead><tbody><tr cid="n2371" mdtype="table_row"><td>Accessibility ID</td><td>Read a unique identifier for a UI element. For XCUITest it is the element's <code>accessibility-id</code> attribute. For Android it is the element's <code>content-desc</code> attribute.</td></tr><tr cid="n2374" mdtype="table_row"><td>Class name</td><td>For IOS it is the full name of the XCUI element and begins with XCUIElementType. For Android it is the full name of the UIAutomator2 class (e.g.: android.widget.TextView)</td></tr><tr cid="n2377" mdtype="table_row"><td>ID</td><td>Native element identifier. <code>resource-id</code> for android; <code>name</code> for iOS.</td></tr><tr cid="n2380" mdtype="table_row"><td>Name</td><td>Name of element</td></tr><tr cid="n2383" mdtype="table_row"><td>XPath</td><td>Search the app XML source using xpath (not recommended, has performance issues)</td></tr><tr cid="n2386" mdtype="table_row"><td>Image</td><td>Locate an element by matching it with a base 64 encoded image file</td></tr><tr cid="n2389" mdtype="table_row"><td>Android UiAutomator (UiAutomator2 only)</td><td>Use the UI Automator API, in particular the <a spellcheck="false" href="https://developer.android.com/reference/android/support/test/uiautomator/UiSelector.html">UiSelector</a> class to locate elements. In Appium you send the Java code, as a string, to the server, which executes it in the application’s environment, returning the element or elements.</td></tr><tr cid="n2392" mdtype="table_row"><td>Android View Tag (Espresso only)</td><td>Locate an element by its <a spellcheck="false" href="https://developer.android.com/reference/android/support/test/espresso/matcher/ViewMatchers.html#withTagValue(org.hamcrest.Matcher)">view tag</a></td></tr><tr cid="n2395" mdtype="table_row"><td>Android Data Matcher (Espresso only)</td><td>Locate an element using <a spellcheck="false" href="https://developer.android.com/reference/android/support/test/espresso/Espresso#ondata">Espresso DataMatcher</a></td></tr><tr cid="n2398" mdtype="table_row"><td>IOS UIAutomation</td><td>When automating an iOS application, Apple’s <a spellcheck="false" href="https://appium.io/docs/en/drivers/ios-uiautomation/index.html">Instruments</a> framework can be used to find elements</td></tr></tbody></table>

> 关于 Espresso 　[https://developer.android.google.cn/training/testing/espresso?hl=zh-cn](https://developer.android.google.cn/training/testing/espresso?hl=zh-cn)

### Toast

> [https://www.cnblogs.com/qinghan123/p/14012689.html](https://www.cnblogs.com/qinghan123/p/14012689.html)

*   Android 中的 Toast 是一种简易的消息提示框，常见如协议未勾选、用户名密码错误、验证码提示、再按一次退出等（也可以用非 Toast 来实现），一般显示 3s 左右的时间就消失（这个时间是可以控制的）。它属于系统的一种提示（有点像 alert），而不是应用上的，所以使用定位元素工具定位是获取不到 Toast 元素的，但不妨碍它可以被定位到。
    
*   定位 Toast 元素需要借助 UiAutomator2 ，automationName：uiautomator2；由于它的设计方式，所以在 PageSource 是查找不到的。
    
*   在定位 Toast 元素时只能使用 xpath 定位方式。
    
*   使用 xpath 定位有两种方法
    

*   一种是借助 Toast 的 className：android.widget.Toast
    
    ```
    driver.find_element_by_xpath("//*[@class='android.widget.Toast']")
    
    ```
    
*   一种是借助文本内容。所以定位写法有两种形式：
    
    ```
    driver.find_element_by_xpath("//*[@contains(@text,'Toast消息或其部分内容')]")
    #注意不要用text()方法，跟selenium冲突了，不一样的
    
    ```
    

*   注意点
    

*   在我们等待元素可见的时候，不要用 visibility_of_element_located，因为它对 Toast 的可见处理并不支持，会直接报错命令无法执行。也就是等待的时候，要用元素存在的条件。不能用元素可见的条件。
    
*   无法匹配到 Toast（等待的时候 Toast 消失了，可能 Toast 太快了，那你可以让开发多等一会）。但是 Toast 的获取目前好像只能这么做。
    
*   如果 Appium Server 版本低于 1.6.3+，代码中必须指定 automationName 为 UIAutomator2（这是以前）
    
    ```
    desired_caps["automationName"]="UiAutomator2"
    
    ```
    

*   示例
    
    ```
    #打开有道云笔记，不输入手机号点击手机号登录，会提示“请阅读并同意《服务条款》和《隐私政策》”要捕捉这个提示信息（用于做测试。
    
    from appium import webdriver
    from selenium.webdriver.support.wait import WebDriverWait
    from selenium.webdriver.support import expected_conditions as EC
    from time import sleep
    des_cap = {}
    des_cap['platformName']='android'
    des_cap['platformVersion']='11'
    des_cap['appPackage']='com.youdao.note'
    des_cap['appActivity']='.splash.SplashActivity'
    driver = webdriver.Remote(command_executor='http://127.0.0.1:4723/wd/hub',
                              desired_capabilities=des_cap)
    driver.implicitly_wait(5)
    driver.find_element_by_android_uiautomator('text("同意")').click()
    driver.find_element_by_android_uiautomator('text("Allow")').click()
    driver.find_element_by_android_uiautomator('text("Allow")').click()
    driver.find_element_by_android_uiautomator('text("跳过")').click()
    driver.find_element_by_android_uiautomator('text("我的")').click()
    driver.find_element_by_android_uiautomator('text("未登录")').click()
    driver.find_element_by_android_uiautomator('text("手机号登录")').click()
    # toast = driver.find_element_by_xpath("//*[contains(text(),'服务条款')]")
    toast_loc1 = ('xpath',"//*[contains(@text,'请阅读并同意')]")  #此处不能用text()
    toast_loc2 = ('xpath',"//*[@class='android.widget.Toast']") #这个元素不好获取
    
    try:
        ele_toast = WebDriverWait(driver,3,0.5).until(EC.presence_of_element_located(toast_loc2))
        # ele_toast = driver.find_element(*toast_loc1)
        print(ele_toast.text)
    except:
        print('没找到')
    
    ```