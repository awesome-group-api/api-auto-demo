# 220316_面向对象高级
# 私有属性与私有方法
# 私有属性与私有方法不能从外部被调用,也不能被子类继承
# 在属性或方法的前面加上__,就是私有属性或私有方法
# 如果前后都有__,不是私有属性或私有方法

# class Class100:
#     __str1 = 'abc'  # 私有属性
#     str2 = 'def'
#
#     def __method1(self):  # 私有方法
#         print('这是私有方法')
#
#     def method2(self):
#         print(self.__str1)
#         self.__method1()
# cls10 = Class100()
# # print(cls10.__str1)
# # cls10.__method1()
# cls10.method2()

# @property
# class Class6:
#     def __init__(self):
#         self.a = 100
#     @property  #装饰器,声明下面的方法是一个属性,而不是方法
#     def b(self):
#         return self.a
# cls6 = Class6()
# print(cls6.b)


#多态
#不同的类中有同名的方法,调用方法时,根据对象的不同,实现的操作也不同,称之为多态
#比如,调用狗的叫的方法为狗叫,调用猫的叫的方法为猫叫
# class Dog:
#     def say(self):
#         print('汪汪汪')
#
# class Cat:
#     def say(self):
#         print('喵喵喵')
#
# dog = Dog()
# cat = Cat()
#
# def animal_say(animal):
#     animal.say()
# animal_say(cat)

# class Restaurant:
#     pass
#
# class Yuxiangrousi(Restaurant):
#     def menu(self):
#         print('鱼香肉丝')
#
# class Gongbaojiding(Restaurant):
#     def menu(self):
#         print('宫爆鸡丁')
#
# class Qingjiaotudousi(Restaurant):
#     def menu(self):
#         print('青椒土豆丝')
#
# customer1 = Yuxiangrousi()
# customer2 = Gongbaojiding()
# customer3 = Qingjiaotudousi()

# def waiter(obj):
#     obj.menu()
# waiter(customer3)

#第7次课思考题
# 写一个猜数字游戏,需求如下:
# 随机生成一个0-100之间的数字,让用户猜,如果用户猜对了,提示:回答正确,游戏结束.
# 如果猜错了给出对应的提示(您输入的值过大,您输入的值过小),最多允许猜7次.

# from random import randint
# answer = randint(0,100)
# for i in range(7):
#     input1 = int(input('请输入一个数字:  '))
#     if input1 == answer:
#         print('回答正确')
#         break
#     elif input1 > answer:
#         print('数字过大')
#     elif input1 < answer:
#         print('数字过小')

# 写一个三角形的类
class Triangle:
    def __init__(self, a, b, c):
        self.a = a
        self.b = b
        self.c = c

    def perimeter(self):
        if self.a + self.b <= self.c or self.a + self.c <= self.b or self.b + self.c <= self.a:
            return '无法构成三角形,忽略周长'
        else:
            return self.a + self.b + self.c

    def area(self):
        if self.a + self.b <= self.c or self.a + self.c <= self.b or self.b + self.c <= self.a:
            return '无法构成三角形,忽略周长'
        else:
            p = (self.a + self.b + self.c) / 2
            return (p * (p - self.a) * (p - self.b) * (p - self.c)) ** 0.5

tr = Triangle(3,4,5)
print(tr.perimeter())
print(tr.area())