# 220121_pycharm使用技巧
# 新建文件时,自动生成代码
# settings→editor→file and code templates,选择python script
# ${NAME}  文件名
# ${DATE}  日期

# 自动补齐
# if __name__ == '__main__':  #先输入main,然后按tab键
#     pass

# 自动补齐自定义的段落
# settings→editor→live templates,在右侧点击+号,添加自定义的内容
# 完成之后,在下方勾选python

# 修改注释的颜色
# settings→editor→color scheme→python

# 取消语法检查
# settings→editor→inspections,选择python,取消勾选PEP 8的两个选项

# 真实环境与虚拟环境

# 分屏
# settings→keymap,查询split关键字,找到分屏的图标,设置快捷键

# 设置编码
# settings→editor→file encodings
# 也可以在文件的第一行加上#encoding=utf-8

# 列表的排序
# list1=[68,36,72,10,100,-50,109,27]
# list1.sort(reverse=True)  #排序,没有返回值,reverse=True倒序排序
# print(list1)
# list1_new=sorted(list1,reverse=True)  #排序,返回排序后的列表,reverse=True倒序排序
# print(list1_new)
# list1_new = list1[::-1]  #只翻转,不排序
# print(list1_new)

# 冒泡算法
# 列表中的数字,两两比较,如果前面的数比后面的大,则两数互换,每一轮确定一个最大的数,通过若干轮比较,实现所有数的排序

# 原始数据[4,3,2,1]  n个数,第一轮比较n-1次
# 3421  #第一次比较,将4和3的位置互换
# 3241  #第二次比较,将4和2的位置互换
# 3214  #第三次比较,将4和1的位置互换
# 第二轮比较,原始数据[3,2,1,4]  比较n-2次
# 2314  第一次比较,将3和2的位置互换
# 2134  第二次比较,将3和1的位置互换
# 第三轮比较,原始数据[2,1,3,4]  比较n-3次
# 1234 第一次比较,将2和1的位置互换
# n个数,最多比较n-1轮
# list1 = [68, 36, 72, 10, 100, -50, 109, 27]
# for i in range(len(list1) - 1):  # 控制比较多少轮
#     for j in range(len(list1) - 1 - i):  # 控制比较多少次
#         if list1[j] > list1[j + 1]:
#             list1[j], list1[j + 1] = list1[j + 1], list1[j]
# print(list1)

# for i in range(len(list1) - 1):  # 控制比较多少轮
#     for j in range(len(list1) - 1 - i):  # 控制比较多少次
#         if list1[j] > list1[j + 1]:
#             print(f'第{j}位和第{j + 1}位的顺序不对')
#             print(f'变化之前--------------------->{list1}')
#             list1[j], list1[j + 1] = list1[j + 1], list1[j]
#             print(f'变化之后--------------------->{list1}')
#     print(f'第{i + 1}轮比较结束')
# print(list1)
