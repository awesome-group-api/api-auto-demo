# 220307_文件的读写
# filepath = 'd:/note1.txt'
# file1 = open(filepath,'r',encoding='utf-8')  #打开一个文件,参数1,文件路径  参数2,r/w/a  参数2不写时缺省值为r
# print(file1.read())  #返回文件内容,以str形式返回
# file1.close()  #关闭文件  使用open()时,必须close()关闭文件,否则会一直占用内存

# with open(filepath) as file1:
#     print(file1.read())

# filepath2 = './note2.txt'
# with open(filepath2,encoding='utf-8') as file2:
#     print(file2.read())
    # print(file2.readline())  #读取一行内容,返回值是str型
    # print(file2.readlines())  #读取文件全部内容,返回值是列表,每一行是一个元素
    # print(file2.read().splitlines())  #读取文件全部内容,返回值是列表,每一行是一个元素,没有\n

#文件的读写
#w+  可以同时读写,如果文件不存在,则新建,写入时,清空之前的内容写入
#r+  可以同时读写,如果文件不存在,则报错,写入时,覆盖写入
#a+  可以同时读写,如果文件不存在,则新建文件,写入时是追加写入

#文件一开始为abcde,w+写入qq,文件内容保存的是qq
#文件一开始为abcde,r+写入qq,文件内容保存的是qqcde
#文件一开始为abcde,a+写入qq,文件内容保存的是abcdeqq

# with open('./1.txt','w+',encoding='utf-8') as file1:
#     file1.write('till they tell the true')
#     file1.seek(0)  #光标回到文件首位
#     print(file1.read())

#某项目性能测试，要求快速生成多个账号，格式为sq001~sq999,密码123456，账号和密码之间用逗号隔开
with open('./账号220307.txt','w+') as file1:
    for i in range(1,1000):
        if i <999:
            file1.write(f'sq{i:03},123456\n')
        else:
            file1.write(f'sq{i:03},123456')

#课堂小结
#open(),with open()
#read(),readline(),readlines(),read().splitlines()
#w+,r+,a+
#seek()
#使用seek()时的汉字占用字节数,gbk2字节,utf-8,3字节