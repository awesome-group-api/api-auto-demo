# 220307_字典与json
# 字典是以键值对形式出现的存储对象
# 字典是可变对象
# 字典的键可以存放不可变对象
# 字典的值可以存放任意类型的对象
# 字典的键是唯一的
# 字典是无序的

# dict1 = {'A': 'apple'}
# print(dict1['A'])
# print(dict1['B'])  #找不到键，报错
# dict1['A']='ace'  #字典里有对应的键,修改
# dict1['B']='book'  #字典里没有对应的键,新增
# print(dict1)
# print('A' in dict1)  #判断键是否在字典里
# print('apple' in dict1)  #判断某个对象是否在字典里,根据键判断,而不是值
# del dict1['A']  #删除字典的键值对
# print(dict1)

# 清空字典
# dict1.clear()  #删除字典中的键值对,内存中的地址不变
# dict1 = {}  #重新定义一个空字典,内存中的地址有改变

# dict2={'A':'apple','B':'book','C':'cake'}
# 遍历字典中的键
# for key in dict2.keys():
#     print(key)

# 遍历字典中的值
# for value in dict2.values():
#     print(value)

# for key,value in dict2.items():
#     print(key,value)

# 字典是无序的
# dict3 = {'A': 'apple', 'B': 'book'}
# dict6 = {'B': 'book', 'A': 'apple'}
# print(dict3 == dict6)

#集合,相当于只有键的字典
# set1 = {'A','B','C','D','E','A'}
# print(set1)

# json
str1 = '''{
"aac003": "张三",
"aac030": "13574553997",
"crm003": "1",
"crm004": "1"
}'''

#有一个注册页面的接口请求,传了一个json,其中的手机号唯一,传json时,要求修改为随机手机号
import json
from random import randint
dict1 = json.loads(str1)  #将json转为字典
dict1['aac030']=f'138{randint(10000000,99999999)}'
str1 = json.dumps(dict1,ensure_ascii=False)  #将字典转为json,如果有中文乱码,加一个参数ensure_ascii=False
print(type(str1))

#课堂小结
#字典的概念与特性
#字典的键的唯一性
#字典是无序的
#字典的新增,需改,删除
#遍历字典
#json.loads(),json.dumps()