# 220311_读取yaml文件
#安装yaml模块 pip install PyYaml
import yaml

# with open('./1.yaml') as file1:
#     text = yaml.load(file1,Loader=yaml.FullLoader)
#     print(text)

#读取多种格式的yaml文件
# with open('./1.yaml',encoding='utf-8') as file1:
#     text = yaml.load_all(file1,Loader=yaml.FullLoader)
#     for one in text:
#         print(one)

#统计10000以内有多少个含有9的数
count = 0
for i in range(10001):
    if '9' in str(i):
        count += 1
print(count)