# 220318_异常与日志
# 异常就是程序运行时出现了错误
# try except可以抓取异常,语句中,至少有一个except,也可以有多个,也可以有一个else语句,一个finally语句
# try:
#     input1 = int(input('请输入一个数字:  '))
#     print(1/input1)
# except ZeroDivisionError:  #0作为分母的异常
#     print('0不能作为分母')
# except ValueError:  #非数字异常
#     print('您输入的不是数字')
# except:  #不指定异常类型,则捕获任何异常
#     print('程序出现异常')
# else:  #程序未出现异常,则执行else中的语句
#     print('程序未出现异常')
# finally:  #无论程序是否出现异常,都会执行
#     print('程序运行完毕')

# 常见的异常
# NameError　　未定义的变量
# print(a)

# IndexError  下标越界
# list1 = [100]
# print(list1[1])

# FileNotFoundError  找不到文件异常
# with open('./cdefg.txt') as file1:
#     print(file1.read())

# 所有的异常,都是Exception的子类,或者子类的子类
# print(NameError.__base__)
# print(IndexError.__base__)
# print(LookupError.__base__)
# print(FileNotFoundError.__base__)
# print(OSError.__base__)
# print(Exception.__base__)
# print(BaseException.__base__)

#raise 手动抛出异常
# try:
#     raise IOError
# except IOError:
#     print('出现了IO异常')

from loguru import logger
# 日志的级别 debug<info<warning<error<critical


logfile = './log1.log'
# logger.remove(handler_id=None)  #不在控制台打印

# rotation='200KB'  文件达到200KB之后生成新文件  compression='zip'  将达到200KB的文件压缩为.zip格式
logger.add(logfile,encoding='utf-8',rotation='200KB',compression='zip')
# logger.debug('松勤')
# logger.info('松勤')
# logger.warning('松勤')
# logger.error('松勤')
# logger.critical('松勤')


# for i in range(10000):
#     logger.warning('test')

def fun1(a,b):
    return a/b
try:
    print(fun1(100,0))
except:
    logger.exception('报错')