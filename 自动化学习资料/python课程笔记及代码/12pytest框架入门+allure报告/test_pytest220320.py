# test_pytest220320
# pytest命名规范
# 测试文件必须以test_开头（或者以_test结尾）,pytest所在的路径不要使用中文或特殊符号
# 测试类必须以Test开头，并且不能有 __init__ 方法
# 测试方法必须以test_开头
# 断言必须使用 assert

# settings→tools→pyton integrated tools,找到default test runner,选unittests
# unittests模式,测试通过为".",测试不通过为F
import pytest
import os
import allure


# class Test1:
#     def test_c01(self):
#         assert 1 == 1
#     def test_c02(self):
#         assert 2 == 3

# list1 = [[1, 1], [2, 3], [3, 6], [7, 8], [9, 10]]


# class Test2:
#     @pytest.mark.parametrize('result,real_result', list1)  # 数据驱动
#     def test_c100(self, result, real_result):
#         assert result == real_result


# 1、下载allure.zip
# 2、解压allure.zip到一个文件目录中,allure需要有jdk环境
# 3、将allure报告安装目录\bin所在的路径添加环境变量path中
# 4、pip install  allure-pytest


@allure.epic('层级1')
@allure.feature('层级2')
@allure.story('层级3')
@allure.title('层级4')
class Test6:
    def test_c06(self):
        assert 1 == 2


path = '../report/report'
if __name__ == '__main__':
    # pytest.main([__file__, '-s', '--alluredir', '../report'])
    # os.system('allure generate ../report -o ../report/report --clean')

    pytest.main([__file__,'-s','--alluredir',f'{path}','--clean-alluredir'])
    os.system(f'allure serve {path}')
