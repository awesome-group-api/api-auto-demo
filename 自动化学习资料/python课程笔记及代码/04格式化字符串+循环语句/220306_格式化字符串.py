# 220306_格式化字符串
# a=10
# b=8
# print(str(a)+'+'+str(b)+'='+str(a+b))  #10+8=18
# print('%s+%s=%s'%(a,b,a+b))  #格式化字符串

#方案一
#%s字符串,%d整数,%f浮点数
# info1 = '我是%s,你是%s,他是%s,今年是%d年.'%('德华','学友','黎明',2022)
# print(info1)

#前面用%d,后面用字符串,报错
# info1 = '今年是%d年.'%('德华')
# print(info1)

#前面用%s,后面用数字,不报错
# info1 = '他是%s'%(2022)
# print(info1)

#前面的空位比后面的值多,报错
# info1 = '我是%s,你是%s,他是%s,今年是%d年.'%('德华','学友','黎明')
# print(info1)

#前面的空位比后面的值少,报错
# info1 = '我是%s,你是%s,他是%s'%('德华','学友','黎明','富城')
# print(info1)

#补齐到指定位数,默认右对齐
# info1 = '他是%10s,今年是%10d年.'%('黎明',2022)
# print(info1)

#左对齐
# info1 = '他是%-10s,今年是%-10d年.'%('黎明',2022)
# print(info1)

#补0
# info1 = '他是%10s,今年是%010d年.'%('黎明',2022)
# print(info1)

#%f浮点型,默认保留6位小数
# number1 = '您输入的数字是%f'%(3.6)
# print(number1)

#保留两位小数
# number1 = '您输入的数字是%.2f'%(3.6)
# print(number1)

#补齐到10位,保留两位小数
# number1 = '您输入的数字是%10.2f'%(3.6)
# print(number1)

#方案二
# str1 = 'My name is {},your name is {},age is {}.'.format('Clark','Ralf',21)
# print(str1)

#前面的空位比后面多,报错
# str1 = 'My name is {},your name is {},age is {}.'.format('Clark','Ralf')
# print(str1)

#前面的空位比后面少,不报错
# str1 = 'My name is {},your name is {}.'.format('Clark','Ralf',21)
# print(str1)

#大括号里不写下标称之为顺序取值法,写下标称之为下标取值法
# str1 = 'My name is {2},your name is {2},age is {2}.'.format('Clark','Ralf',21)
# print(str1)

#顺序取值法与下标取值法不可以混用
# str1 = 'My name is {},your name is {},age is {2}.'.format('Clark','Ralf',21)
# print(str1)

#补齐,字符串默认左对齐,数字默认右对齐
# str1 = 'My name is {:10},your name is {:10},age is {:10}.'.format('Clark','Ralf',21)
# print(str1)

#对齐方式,<左对齐,>右对齐,^居中对齐
# str1 = 'My name is {:>10},your name is {:^10},age is {:<10}.'.format('Clark','Ralf',21)
# print(str1)

#补0
# str1 = 'My name is {:>010},your name is {:^010},age is {:>010}.'.format('Clark','Ralf',21)
# print(str1)

#python3.6以后的版本中,可以使用更加简便的写法
name1 = 'Clark'
name2 = 'Ralf'
age = 21
print(f'My name is {name1},your name is {name2},age is {age}.')

#课堂小结
#方案一,%s,%d,%f,补齐,对齐方式,前后数量一致性
#方案二,补齐,对齐方式,顺序取值法,下标取值法
#f''的用法