# 220321_python复习1
str1 = 'myjerbgethyjumg'
# print(str1[-7:-4])  #thy
# print(str1[-5:-8:-1])  #yht
# print(str1[::-1])  #翻转

list1 = [36, 89, 72, 100, 99, 21, 64]
# list1.sort()  #排序,sort()方法没有返回值
# print(list1)

# list1.sort(reverse=True)  #reverse=True翻转列表
# print(list1)

# list1_new = sorted(list1)  #sorted()函数,返回排序后的列表
# print(list1_new)

# list1_new = sorted(list1,reverse=True)  #reverse=True翻转列表
# print(list1_new)

# 写一个函数,判断字符串是否是回文
# str2 = '上海自来水来自海上'
#
#
# def fun1(str):
#     if str == str[::-1]:
#         return True
#     else:
#         return False
#
#
# print(fun1(str2))

# 列表的增删改
# append,insert,extend
list2 = [100, 200, 300]
# list2.insert(-1,600)
# print(list2)

# pop,remove,del
# print(list2.pop())

# 写一个函数,可以打印斐波那契数列的前n位
# 1,1,2,3,5,8,13,21,34,55,89......
# def fun2(n):
#     list1= []
#     for i in range(n):
#         if i<2:
#             list1.append(1)
#         else:
#             list1.append(list1[-2]+list1[-1])
#     return list1
# print(fun2(20))

# 元组,是不可变对象,可以使用下标和切片
# 元组中只有一个元素时,加一个逗号
# 元组中有子列表时,子列表中的值可以修改

# 浅拷贝与深拷贝
# 有两个接口,一个接口A,里面的值是[100,200,300,[400,500]],一个接口B,要求接口B的值与接口A一致
# 领导要求修改接口A的第0位的值,改成900,但是不可以修改接口B的值
# 领导要求修改接口A的子列表的第0位的值,改成750,不可以修改接口B的值

listA = [100, 200, 300, [400, 500]]
# listB = listA  #赋值,相当于起了一个别名,没有生成新的对象
# listA[0] = 900
# print(listA)
# print(listB)

import copy


# listB = copy.copy(listA)  #浅拷贝,生成了新的对象,子列表仍然是同一个对象
# listA[0] = 900
# listA[3][0] = 750
# print(listA)
# print(listB)

# listB = copy.deepcopy(listA)  #深拷贝,列表与子列表都是新的对象
# listA[0] = 900
# listA[3][0] = 750
# print(listA)
# print(listB)

# 函数
# 写一个函数,用户输入一个参数n,返回从1加到n的和
# def sumdata(n):
#     sum = 0
#     for i in range(1,n+1):
#         sum += i
#     return sum
# print(sumdata(100))

# 写一个函数,不使用循环,求某数的阶乘
# 5! = 5*4*3*2*1 = 5 * 4!
# 4! = 4*3*2*1 = 4 * 3!
# 3! = 3*2*1 = 3 * 2!
# 2! = 2*1 = 2 * 1!
# 1! = 1
# n! = n*(n-1)!
def fun9(n):
    if n == 1:
        return 1
    else:
        return n * fun9(n - 1)


print(fun9(6))


def fun1():
    print('Hello')
    return True

# 程序执行时,只要它能判断布尔表达式结果为假,那么后面的式子就不再执行,直接返回False
# print(1>2 and 5>4 and 6>5 and 10>9 and 100>98 and 99>96 and fun1())

# 程序执行时,只要它能判断布尔表达式结果为真,那么后面的式子就不再执行,直接返回True
# print(2>1 or 1>5 or 3>9 or 10>100 or fun1())


# 循环
# for i in range(1,11):
#     if i==5:
#         # break  #终止循环
#         # continue  #跳出当次循环
#         pass  #占位符,防止语法错误
#     print(i)
# else:  #当循环中没有出现break,则循环结束时,运行一次else中的语句
#     print('循环运行完毕')


#文件的读写
#w+  可以同时读写,如果文件不存在,则新建,写入时,清空之前的内容写入
#r+  可以同时读写,如果文件不存在,则报错,写入时,覆盖写入
#a+  可以同时读写,如果文件不存在,则新建文件,写入时是追加写入

#文件一开始为abcde,w+写入qq,文件内容保存的是qq
#文件一开始为abcde,r+写入qq,文件内容保存的是qqcde
#文件一开始为abcde,a+写入qq,文件内容保存的是abcdeqq