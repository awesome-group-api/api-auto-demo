# 220304_对象的方法
# str1='cdefgcdefg'
# print(str1.find('e',5)) #返回某个或某些字符在字符串中的位置,默认从头开始查找,也可以从指定位置开始查找,找不到值时,返回-1
# print(str1.index('e',5))  #用法与find类似,当找不到值时抛异常

# strip  #去掉字符串前后的空格,返回去掉空格之后的字符串
str2 = '     青  山  横  北  郭  ,  白  水  绕  东  城     '
# str2 = str2.strip()
# print(str2)

# 替换字符串中的某个或某些字符,返回替换后的字符串,返回值是str型
# str2 = str2.replace(' ','')
# print(str2)

# str3 = '<p>中国男足勇夺世界杯冠军</p>'
# str3 = str3.replace('<p>','').replace('</p>','')
# print(str3)

# startswith  检查字符串是否以某个或某些字符开头,返回值是布尔型
# 根据身份证号判断是否是南京的身份证
# id_card = '320104199810070863'
# if id_card.startswith('3201'):
#     print('南京的身份证')

# endswith  检查字符串是否以某个或某些字符结尾,返回值是布尔型
# 判断身份证的最后一位是否是X
# if id_card.endswith('X'):
#     print('最后一位是X')

# isalpha,检查字符串中是否是纯字母,isdigit,检查字符串中是否是纯数字

# split  切割字符串,它有个一个参数,以参数作为分隔符,返回值是列表

# str6 ='tnhwrt'
# str6_new=str6.split('t')
# print(str6_new)  #如果切割符位于首位或末尾,会产生空值

# 上节课思考题
# 写一个号段筛选程序,需求如下:
# 用户从控制台输入一个手机号,判断出运营商(移动(130-150),联通(151-170),电信(171-190))
# 如果用户输入的位数不对,提示用户位数有误,如果用户输入非数字,提示有非法字符

mobile = input('请输入一个手机号:  ')
if not mobile.isdigit():  # 判断用户输入的是否不是纯数字
    print('您输入的不是数字')
else:
    if len(mobile) != 11:  #判断用户输入的长度是否不是11位
        print('位数不正确')
    else:
        number = int(mobile[0:3])  #取得手机号的前三位,并转为int型
        if 130 <= number <= 150:
            print('移动')
        elif 151 <= number <= 170:
            print('联通')
        elif 171 <= number <= 190:
            print('电信')
        else:
            print('您输入的手机号不属于任何运营商')