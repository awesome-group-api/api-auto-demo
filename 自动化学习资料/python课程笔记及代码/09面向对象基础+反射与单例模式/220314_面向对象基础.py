# 220314_面向对象基础
# 类是抽象的模板,实例是根据模板创建出来的具体的对象,比如人类就是一个类,彭于晏是人类的一个实例
# 新建一个长方形的类

# self表示实例本身,这个参数是默认的,不需要传值.用户在实例化一个长方形时,需要传长和宽两个值,之后初始化方法将其转为长方形实例的属性.
# 初始化方法里的length转换为self.length之后,类当中的所有含有self参数的方法都可以使用这个属性

class Rectangle:
    def __init__(self, length, width):  # 初始化方法
        self.length = length  # 将用户传的length转为实例自身的属性
        self.width = width  # 将用户传的的width转为实例自身的属性

    def perimiter(self):  # 周长的方法
        return (self.length + self.width) * 2

    def area(self):  # 面积的方法
        return self.length * self.width


#
# rec = Rectangle(10,8)  #实例化
# print(rec.__dict__)  #打印实例的属性
# print(rec.perimiter())
# print(rec.area())


# 在做程序开发中，我们常常会遇到这样的需求：需要执行对象里的某个方法，或需要调用对象中的某个变量，但是由于种种原因我们无法确定这个方法或变量是否存在，这时我们需要用一个特殊的方法或机制要访问和操作这个未知的方法或变量，这种机制就称之为反射。

# hasattr
# print(hasattr(str,'replace'))  #在对象里找有没有某个属性或方法,返回值是布尔型
# print(hasattr(list,'append'))

# getattr
# print(getattr(str,'replace'))  #在对象里找有没有某个属性或方法,返回值是属性或方法本身
# print(getattr(str,'replace1',12345))  #也可以写两个参数,找不到属性或方法时,返回第二个参数

# setattr
# class Class1:
#     a=1
#     def __init__(self):
#         self.b = 200
# setattr(Class1,'a',100)  #修改类属性
# print(Class1.a)
# cls1 = Class1()
# setattr(cls1,'b',99)  #修改实例属性
# print(cls1.b)

# 单例模式
# 一般来说,一个类可以生成任意个实例,单例模式只生成一个实例
# class Single:
#     def __init__(self):
#         pass
#     def __new__(cls, *args, **kwargs):  #构造方法
#         if not hasattr(cls,'obj'):  #判断类当中有没有实例,如果没有则新建
#             cls.obj = object.__new__(cls)  #新建一个类的实例
#         return cls.obj
#
# s1 = Single()
# s2 = Single()
# print(s1 == s2)

class Restaurant:
    def yuxiangrousi(self):
        return '鱼香肉丝'

    def gongbaojiding(self):
        return '宫爆鸡丁'

    def qingjiaotudousi(self):
        return '青椒土豆丝'

    def fanqiejidan(self):
        return '番茄鸡蛋'

    def kaishuibaicai(self):
        return '开水白菜'

customer = Restaurant()
while True:
    menu = input('请点菜:  ')
    if hasattr(Restaurant,menu):
        print('好的,请厨师开始做菜')
        break
    else:
        print('没有这道菜')