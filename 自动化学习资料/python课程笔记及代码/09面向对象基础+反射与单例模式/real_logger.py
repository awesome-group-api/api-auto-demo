# real_logger
from loguru import logger
from time import strftime

class MyLog():  #新建一个类
    __call_flag=True  #变量设置为真

    # 单例模式
    def __new__(cls, *args, **kwargs):  # 构造方法
        if not hasattr(cls, 'obj'):  # 判断类当中有没有实例,如果没有则新建
            cls.obj = object.__new__(cls)  # 生成实例对象
        return cls.obj
    def get_log(self):
        if self.__call_flag:  #如果变量为真
            __curdate=strftime('%Y%m%d-%H%M%S')
            logger.remove(handler_id=None)  #不在控制台显示
            logger.add(f'文件名{__curdate}.log',rotation='200KB',compression='zip',encoding='utf-8')  #对logger进行设置
            self.__call_flag=False  #变量设置为假
        return logger



if __name__ == '__main__':
    log100 = MyLog().get_log()  #实例化log100
    log100.error('张三')
    from time import sleep
    sleep(2)
    log101 = MyLog().get_log()  #实例化log101,如果是单例模式,log100和log101的内容会写在一个文件里,如果不是单例模式则写在两个文件里
    log101.error('李四')