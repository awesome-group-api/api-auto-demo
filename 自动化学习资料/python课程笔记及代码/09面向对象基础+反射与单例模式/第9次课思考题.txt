写一个三角形的类,包括初始化方法,计算周长的方法,计算面积的方法(可以用海伦公式)




































class Sanjiaoxing:
    def __init__(self,a,b,c):
        self.a=a
        self.b=b
        self.c=c
    def zhouchang(self):
        if self.a+self.b<=self.c or self.a+self.c<=self.b or self.b+self.c<=self.a:
            return '无法构成三角形,忽略周长'
        else:
            return self.a+self.b+self.c
    def mianji(self):
        if self.a + self.b <= self.c or self.a + self.c <= self.b or self.b + self.c <= self.a:
            return '无法构成三角形,忽略面积'
        else:
            p=(self.a+self.b+self.c)/2
            return (p*(p-self.a)*(p-self.b)*(p-self.c))**0.5
sjx=Sanjiaoxing(3,4,5)
print(sjx.zhouchang())
print(sjx.mianji())